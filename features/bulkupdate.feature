Feature: bulkupdate 

@bulkupdate
Scenario Outline: Ability to assign field services by bulk update
	Given a user is logged into SalesForce as a Scheduler 
	When the scheduler assigns "<count>" field services to a consultant
	Then the consultant should be book for the "<count>" assigned services
	
	Examples:
		|count| 
		|1|
		|2|
		|5|					

@bulkupdate		
Scenario Outline: Ability to deliver field services by bulk update
	Given a user is logged into SalesForce as a Scheduler
	When the scheduler delivers "<count>" field services for the consultant
	Then the "<count>" field services should be marked as delivered
	
	Examples:
	|count|
	|1|
	|2|
	|5|	
	
@bulkupdate	
Scenario Outline: Ability to assign and delivery field services by bulk update
	Given a user is logged into SalesForce as a Scheduler
	When the scheduler assigns "<count3>" field services to a consultant
	And the scheduler delivers "<count2>" field services for the consultant bulk update
	Then the "<count2>" field services should be marked as delivered
	And the consultant should be book for the "<count1>" assigned service bulk update 
	
	Examples:
	|count1|count2|count3|
	|1|1|2|
	|1|5|6|
	|5|1|6|
	|5|5|10|		
	
@bulkupdate	
Scenario: System presents over booked prompt when a consultant is already booked for the same day from bulk update
	Given a user is logged into SalesForce as a Scheduler
	And a consultant is already booked for a specific date
	When the scheduler schedules the same consultant for another booking for the same date by bulk update
	Then the scheduler should be prompted that the consultant is over booked

@bulkupdate
Scenario: Scheduler is able to over book a consultant
	Given a user is logged into SalesForce as a Scheduler
	And a consultant is already booked for a specific date 
	When the scheduler schedules the same consultant for another booking for the same date by bulk update
	Then the scheduler should be able to bypass the prompt 
	And the schedule should be able to book the service to the consultant

@bulkupdate	
Scenario: Ability to assign multiple field services from different contracts by bulk update
	Given a user is logged into SalesForce as a Scheduler
	When the scheduler schedules fields services from different contracts to consultants
	Then the consultant should be booked for the assigned services 

@bulkupdate
Scenario: Ability to view field service error in bulk update
	Given a user is logged into SalesForce as a Scheduler
	When the scheduler schedules multiple field services without all requirements
	Then the individual error should be viewable by the user

@bulkupdate	
Scenario Outline: User should not be able to update fields in some statuses
	Given a user is logged into SalesForce as a Scheduler
	When the user navigates to a service that has a "<status>" status
	Then the user should not be able to edit the "Status" field on the bulk update page
	And the user should not be able to edit the "Service Date" field on the bulk update page
	And the user should not be able to edit the "Consultant" field on the bulk update page
	And the user should not be able to edit the "Provider Group" field on the bulk update page
	
	Examples:
		|status|
		|Delivered|
		|Cancelled|
		|Cancelled/Delivered|	

@bulkupdate		
Scenario: Service Date to field service from bulk update
	Given a user is logged into SalesForce as a Scheduler
	When the service date is entered for a field service in bulk update
	Then the start dates should be 9 AM
	And the end date should be 5 PM 		
	