Feature: fieldservicecontract

@fieldservicecontract	
Scenario: Create contract with invalid Install to UCN
	Given the user is logged into Sales Force
	When a new contract is created with "an invalid Install to UCN"
	Then the contract should be created with the "invalid Install to UCN" that is not mapped 
	And the field service should be created with the invalid Install to UCN that is not mapped	 	
	
@fieldservicecontract
Scenario: Update contract that was created with invalid Install to UCN
	Given the user is logged into Sales Force
	And a contract was created with an invalid Install to UCN
	And field services were created with an invalid Install to UCN 
	When contract is updated 
	Then the contract should contain the updated values with the invalid Install to UCN that is not mapped
	And the field service should contain the updated values with the invalid Install to UCN that is not mapped
	
@fieldservicecontract
Scenario: Create contract with invalid Product Number
	Given the user is logged into Sales Force
	When a new contract is created with "an invalid Product Number" 
	Then the contract should be created with the "invalid Product Number" that is not mapped 
	And the field services should be created with the invalid Product Number that is not mapped
	And the contract should be flagged as pending
	And the field services should be flagged as pending
	
@fieldservicecontract
Scenario: Update contract that was created with invalid Product Number
	Given the user is logged into Sales Force
	And a contract was created with an invalid Product Number 
	When contract is updated
	Then the contract should contain the updated values with the invalid Product Number that is not mapped
	And the field services should contain the updated values with the invalid Product Number that is not mapped
	And the contract should be flagged as pending 
	And the field services should be flagged as pending

@fieldservicecontract		
Scenario: Create a Product of contract that was created with an invalid Product Number
	Given the user is logged into Sales Force
	And a contract already exists with an invalid Product Number 
	When a Product is created with the previously invalid Product Number
	Then Product Number in the contract should be mapped to the created Product Number
	And Product Number in the field services should be mapped to the created Product Number
	And the contract should no longer be flagged as pending
	And the field services should no longer be flagged as pending
		
@fieldservicecontract
Scenario: Create a client of contract that was created with an invalid Install to UCN
	Given the user is logged into Sales Force
	And a contract already exists with an invalid Install to UCN 
	When a client is created with the previously invalid Install to UCN
	Then the Install to UCN in the contract should be mapped to the created client
	And the Install to UCN in the field services should be mapped to the created client
	
@fieldservicecontract
Scenario: Create a contract with blank start-end dates
	Given the user is logged into Sales Force
	When a new contract is created with "blank start-end dates"
	Then the contract should be created without dates
	And the field services should be created without dates
	And the contract should be flagged as pending
	And the field services should be flagged as pending
	
@fieldservicecontract
Scenario: Update dates for a contract that was created with blank start and end dates
	Given the user is logged into Sales Force
	And a contract already exists with blank start-end dates
	When the contract is updated with valid start-end dates
	Then start-end dates in the contract should be populated
	And start-end dates in the field services should be populated
	And the contract should no longer be flagged as pending
	And the field services should no longer be flagged as pending	

@fieldservicecontract	
Scenario: Create contract with a Product Number which is not a Service Product
	Given the user is logged into Sales Force
	When a new contract is created with "an Product Number which is not a Service Product"
	Then the contract should be created with the "Product Number which is not a Service Product" that is not mapped
	And the field services should be created with the Product Number which is not a Service Product that is not mapped
	And the contract should be flagged as pending
	And the field services should be flagged as pending

@fieldservicecontract	
Scenario: Update contract that was created with a Product Number which is not a Service Product
	Given the user is logged into Sales Force
	And a contract was created with a Product Number which is not a Service Product
	When contract is updated
	Then the contract should contain the updated values with the Product Number which is not a Service Product that is not mapped
	And the field services should contain the updated values with the Product Number which is not a Service Product that is not mapped
	And the contract should be flagged as pending
	And the field services should be flagged as pending		

@fieldservicecontract	
Scenario: Create contract with a inactive Product Number
	Given the user is logged into Sales Force
	When a new contract is created with "an inactive Product Number"
	Then the contract should be created with the inactive Product Number 
	And the field services should be created with the inactive Product Number
	And the contract should be flagged as pending
	And the field services should be flagged as pending

@fieldservicecontract
Scenario: Update contract that was created with a inactive Product Number
	Given the user is logged into Sales Force
	And a contract was created with a inactive Product Number  
	When contract is updated 
	Then the contract should contain the updated values with the inactive Product Number
	And the field services should contain the updated values with the inactive Product Number 
	And the contract should be flagged as pending
	And the field services should be flagged as pending

@fieldservicecontract
Scenario: Reactivate the Product Number for the contract that was created with a inactive Product Number
	Given the user is logged into Sales Force
	And a contract already exists with a inactive Product Number
	When the inactive Product Number is reactivated 
	Then the contract should no longer be flagged as pending
	And the field services should no longer be flagged as pending	
	
@fieldservicecontract	
Scenario Outline: Updates to key fields in contract to trigger change in field services
	Given the user is logged into Sales Force
	And a contract already exists 
	When the "<field>" in the contract is updated
	Then the "<field>" in the field service should be changed
	
	Examples:
	|field|
	|Line Quantity|
	|Contract Dates|
	|Install to UCN|		

@fieldservicecontract	
Scenario: Update to new Install to UCN after a contract is created
	Given the user is logged into Sales Force
	And a contract already exists
	When the Install to UCN is updated 
	Then Install to UCN in the contract should be changed

@fieldservicecontract	
Scenario Outline: Field Service Updates from contract termination
	Given the user is logged into Sales Force
	And a contract already exists with "<qty>" line quantity
	And user clicks on view all Work Orders 
	And the contract contains "<d1>" delivered status field services
	And update the line quantity to "1" 
	And update the line quantity to "3"
	And user clicks on view all Work Orders	
	And the contract contains "<a1>" assigned status field services 
	When the contract is "terminated"
	And user clicks on view all Work Orders
	Then the contract should contain "<c2>" cancelled status field services
	And the contract should contain "<cd2>" cancelled/delivered status field Delivered
	
	Examples:
		|qty|o1 |a1 |d1 |c1 |cd1|c2 |cd2|
		|3  |1  |1  |2  |1  |1  |3  |2  |

@fieldservicecontract		
Scenario Outline: Field Service Updates from contract expires
	Given the user is logged into Sales Force
	And a contract already exists with "<qty>" line quantity
	And user clicks on view all Work Orders 
	And the contract contains "<d1>" delivered status field services
	And update the line quantity to "1" 
	And update the line quantity to "3"
	And user clicks on view all Work Orders
	And the contract contains "<a1>" assigned status field services
	When the contract is "expired"
	And user clicks on view all Work Orders
	Then the contract should contain "<d2>" delivered status field services
	And the contract should contain "<c2>" cancelled status field services  
	And the contract should contain "<cd2>" cancelled/delivered status field
	
	Examples:
		|qty|o1 |a1 |d1 |c1 |cd1|d2 |c2 |cd2|
		|3  |1  |1  |2  |1  |1  |1  |3  |1  |	
		
@fieldservicecontract		
Scenario Outline: Update Line Item Quantity - Decrease 
	Given the user is logged into Sales Force
	And a contract already exists with "<qty>" line quantity
	And user clicks on view all Work Orders 
	And the contract contains "<d1>" delivered status field services
	And update the line quantity to "1" 
	And update the line quantity to "3"
	And user clicks on view all Work Orders	
	And the contract contains "<a1>" assigned status field services   
	When the Line Item Quantity is updated to "<update>"
	And user clicks on view all Work Orders 
	Then the contract should contain "<o2>" open status field services 
	And the contract should contain "<a2>" assigned status field services 
	And the contract should contain "<d2>" delivered status field services 
	And the contract should contain "<c2>" cancelled status field services 
	And the contract should contain "<cd2>" cancelled/delivered status field services
	 
	Examples: 
		|qty|o1|a1|d1|c1|cd1|update|o2|a2|d2|c2|cd2|
		|3|1|1|2|1|1|2|0|1|1|2|1|

@fieldservicecontract		
Scenario Outline: Update Line Item Quantity - Increase
	Given the user is logged into Sales Force
	And a contract already exists with "<qty>" line quantity
	And user clicks on view all Work Orders 
	And the contract contains "<d1>" delivered status field services
	And update the line quantity to "1" 
	And update the line quantity to "3"
	And user clicks on view all Work Orders	
	And the contract contains "<a1>" assigned status field services   
	When the Line Item Quantity is updated to "<update>"
	And user clicks on view all Work Orders
	Then the contract should contain "<o2>" open status field services
	And the contract should contain "<a2>" assigned status field services
	And the contract should contain "<d2>" delivered status field services
	And the contract should contain "<c2>" cancelled status field services
	And the contract should contain "<cd2>" cancelled/delivered status field services
	
	Examples:
	|qty|o1|a1|d1|c1|cd1|update|o2|a2|d2|c2|cd2|
	|3|1|1|2|1|1|5|3|1|1|1|1|

@fieldservicecontract
Scenario Outline: User should not be able to create accounts in SFDC
	Given the user is logged into SalesForce as a Ed Group user
	When the attempts to create a "<type>"
	Then they should be "<type>" unsuccessful 
	 
	Examples:
		|type|
		|Account|
		|Account Address|
		|Account Relationship|

@fieldservicecontract		
Scenario Outline: when a service is delivered the Revenue Message field should populate with todays date
	Given the user is logged into Sales Force
	When a contract is created 
	And one of the services is assigned
	And the same service is delivered with the service date that is "<date>"
	Then the Revenue Message field should be populated with todays date
	
	Examples:
	|date |
	|todays date|
	|a past date|		