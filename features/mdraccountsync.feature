Feature: mdraccountsync 

@MDRMonthlyAccountSync
Scenario: MDR Monthly Account file processing
	Given the MDR Monthly Account file is processed 
	Then the data should process successfully
	And a account from the file should be present in SalesForce
	
@MDRMonthlyAccountSync	
Scenario: MDR Monthly Account file processing to rewrite error file
	Given a MDR Monthly Account error file exist with failed error entries 
	When a new MDR Monthly Account file is processed
	And it contains an error
	Then the new error should be the only error in the error file	

@MDRMonthlyAccountSync
Scenario: MDR Monthly Account file with entry that already exists in Salesforce
	Given the file contains an entry that already exists in Salesforce 
	When the Monthly Account file is processed
	Then the identical entry should not create a duplicate in Salesforce				

@MDRMonthlyAccountSync	
Scenario: MDR Monthly Account file re-processing when there was a failure - Success
	Given a MDR Monthly Account error is logged in the separate file
	When the Daily error Account file is processed
	And it is successful 
	Then the entry should be available in SalesForce	

@MDRMonthlyAccountSync	
Scenario: MDR Monthly Account file processed field validation
	Given a MDR Monthly Account File contains all the fields 
	When the MDR Monthly Account file is processed with all the fields 
	Then the data should process successfully
	And the values for all the fields should be present in SalesForce			
	
@MDRMonthlyAccountSync	
Scenario: MDR Monthly Account file processing - batches
	Given a MDR Monthly Account file is received that contains more records than the batch size 
	When the MDR Monthly Role file is processed 
	Then the data from the first batch should process successfully
	And the data that is outside the first batch of records should not be processed	