Feature: otctosfdc 

@otctosfdc
Scenario: Create a contract from OTC
	Then a contract is created from OTC 
	Then a contract should be created in SFDC

@otctosfdc
Scenario: Create contract with all values from OTC
	Given a contracted is created with all values
	Then the contract should be created with each of the values
	
@otctosfdc
Scenario: Create contract with a Product Number which is not a Service Product
	Given a new contract is created with an Product Number which is not a Service Product
	When the user is logged into Sales Force 
	Then the contract should be created with product number which is not service product from otc
	And the field services should be created with product number which is not service product from otc 
	And the contract should be flagged as pending
	And the field services should be flagged as pending

@otctosfdc	
Scenario: Create contract with a inactive Product Number
	Given a new contract is created with an inactive Product Number 
	When the user is logged into Sales Force
	Then the contract should be created with the inactive Product Number from otc
	And the field services should be created with the inactive Product Number from otc
	And the contract should be flagged as pending
	And the field services should be flagged as pending

@otctosfdc	
Scenario Outline: Field Service Updates from contract termination
	Given the user is logged into Sales Force
	And a contract already exists with "<qty>" line quantity
	And user clicks on view all Work Orders 
	And the contract contains "<d1>" delivered status field services
	And update the line quantity to "1" 
	And update the line quantity to "3"
	And user clicks on view all Work Orders
	And the contract contains "<a1>" assigned status field services
	When the contract is "terminated" from OTC
	And user clicks on view all Work Orders 
	Then the contract should contain "<c2>" cancelled status field services
	And the contract should contain "<cd2>" cancelled/delivered status field Delivered
	
	Examples:
	|qty|o1 |a1 |d1 |c1 |cd1|c2 |cd2|
	|3  |1  |1  |2  |1  |1  |1  |3  |1 |

@otctosfdc	
Scenario Outline: Field Service Updates from contract expires
	Given the user is logged into Sales Force
	And a contract already exists with "<qty>" line quantity
	And user clicks on view all Work Orders 
	And the contract contains "<d1>" delivered status field services
	And update the line quantity to "1" 
	And update the line quantity to "3"
	And user clicks on view all Work Orders
	And the contract contains "<a1>" assigned status field services
	When the contract is "expired" from OTC
	And user clicks on view all Work Orders
	Then the contract should contain "<d2>" delivered status field services
	And the contract should contain "<c2>" cancelled status field services  
	And the contract should contain "<cd2>" cancelled/delivered status field
	
	Examples:
	|qty|o1 |a1 |d1 |c1 |cd1|d2 |c2 |cd2|
	|3  |1  |1  |2  |1  |1  |1  |3  |1  |	

@otctosfdc
Scenario Outline: Update Line Item Quantity - Decrease
	Given the user is logged into Sales Force
	And a contract already exists with "<qty>" line quantity
	And user clicks on view all Work Orders 
	And the contract contains "<d1>" delivered status field services
	And update the line quantity to "1" 
	And update the line quantity to "3"
	And user clicks on view all Work Orders
	And the contract contains "<a1>" assigned status field services
	When the Line Item Quantity is updated to "<update>" from OTC
	And user clicks on view all Work Orders  
	Then the contract should contain "<o2>" open status field services 
	And the contract should contain "<a2>" assigned status field services 
	And the contract should contain "<d2>" delivered status field services 
	And the contract should contain "<c2>" cancelled status field services 
	And the contract should contain "<cd2>" cancelled/delivered status field services
	
	Examples:
	|o1 |a1 |d1 |c1 |cd1 |update |o2 |a2 |d2 |c2 |cd2 |
	|3 |1 |1 |2 |1 |1 |2 |0 |1 |1 |2 |1 |

@otctosfdc
Scenario Outline: Update Line Item Quantity - Increase
	Given the user is logged into Sales Force
	And a contract already exists with "<qty>" line quantity
	And user clicks on view all Work Orders 
	And the contract contains "<d1>" delivered status field services
	And update the line quantity to "1" 
	And update the line quantity to "3"
	And user clicks on view all Work Orders
	And the contract contains "<a1>" assigned status field services
	When the Line Item Quantity is updated to "<update>" from OTC
	And user clicks on view all Work Orders  
	Then the contract should contain "<o2>" open status field services
	And the contract should contain "<a2>" assigned status field services
	And the contract should contain "<d2>" delivered status field services
	And the contract should contain "<c2>" cancelled status field services
	And the contract should contain "<cd2>" cancelled/delivered status field services
	
	Examples:
	|o1 |a1 |d1 |c1 |cd1 |update |o2 |a2 |d2 |c2 |cd2 |
	|3 |1 |1 |2 |1 |1 |5 |3 |1 |1 |1 |1 |	