Feature: cdbaccountintegration

@AccountCreation		
Scenario: New account details sent from CDB to SFDC
	Given a new account details are sent from CDB
	When the new account details are received by SFDC
	Then the new account should be created in SFDC

@AccountCreation
Scenario: when a new account is created from CDB without parent account
	Given a new account is sent from CDB "without" a Parent Account 
	When the new account details are received by SFDC
	Then the new account should be created in SFDC
	And no Account Relationship is created

@AccountCreation	
Scenario: when a new account is created fromt CDB with a parent account
	Given a new account is sent from CDB "with" a Parent Account 
	When the new account details are received by SFDC
	Then the new parent account should be created in SFDC
	And a Account Relationship is created with the account populated in the Related Child Institution field
	And the Parent Account is populated in the Parent Institution field
	And the creation date is in the Start Date
	And the End date is left blank 
	
@AccountCreation	
Scenario: For new accounts from CDB, the address should populate in Account Address with Address type flag
	Given a new account is sent from CDB with all addresses 
	When the new account details are received by SFDC
	Then the new account should be created in SFDC with all address
	And the addresses are populated with Address type details
	
@AccountCreation	
Scenario: For new accounts from CDB, there should be at least one address, the Mailing Address
	Given a new account is sent from CDB with only one address
	When the new account details are received by SFDC
	Then the new account should be created in SFDC
	And the one address populated should be the Mailing Address	

@AccountCreation	
Scenario: The mailing address received for a new account from CDB should populate other mailing fields
	Given a new account details are sent from CDB
	When the new account details are received by SFDC
	Then the new account should be created in SFDC
	And the mailing address should also populate the mailing address in the top bar
	And the mailing county should be populated based on the mailing county code		

@AccountModification	
Scenario Outline: Specific users should be able to update non CDB fields
	Given the user is logged in to Sales Force as a "<user>" user
	When the user attempts to edit the "<field name>" field
	Then they should be able to make the update "<field name>" field
	
	Examples:
	|user|field name|
	|toni|Tier|
	|toni|Gold Customer|
	|CSR|Gold Customer|

@AccountModification		
Scenario: Ed group user should not be able to update account fields
	Given the user is logged into SalesForce as a Ed Group user
	And the user is on a page with account fields
	When the user attempts to edit any account field
	Then they should not be able to make the update

@AccountModification	
Scenario: Ed group user should not be able to update account relationship fields
	Given the user is logged into SalesForce as a Ed Group user
	And the user is on a page with account fields
	When the user attempts to edit any account relationship field
	Then they should not be able to edit account relationship field

@AccountModification	
Scenario: Ed group user should not be able to update account address fields
	Given the user is logged into SalesForce as a Ed Group user
	And the user is on a page with account fields
	When the user attempts to edit any account address field
	Then they should not be able to edit account address field 
	
@AccountModification
Scenario: Parent account field updated - new Parent
	Given an account exist with Parent Account field populated in the Account Relationship 
	When the Parent Account is modified 
	Then a new Account Relationship should be created with the new Parent Account
	And the account in the Related Child institution field	
	
@AccountModification
Scenario Outline: Enterprise status updated to closed should prevent non-CDB field changes by specific users
	Given that an account exists in SalesForce
	And the Enterprise Status on an account is updated to Closed
	When the user is logged in to Sales Force as a "<user>" user
	Then the end user in SalesForce should not be able to make an update to the "<field name>"
	
	Examples:
	|user|field name|
	|toni|Tier|
	|toni|Gold Customer|
	|CSR|Gold Customer|
	
@AccountModification		
Scenario Outline: Enterprise status updated to closed should prevent any related data changes by end users
	Given that an account exists with "<data>" in SalesForce
	When update enterprise status on account as closed 
#	Then end user in SalesForce should not be able to make an add to the "<data>" 
	Then end user in SalesForce should not be able to make an update to the "<data>"
	
	Examples:
	|data |
	|Opportunity|
	|Tasks|
	|Events|
	|Note|
	|Contact|															

@AccountMerge	
Scenario: Account Merge - Status Terminated
	Given two valid accounts exist 
	When the Merged to UCN is populated in one of the accounts
	Then status of that account should be updated to Terminated	

@AccountMerge	
Scenario: Account Merge - Link to redirect to merged account
	Given two valid accounts exist
	When the Merged to UCN is populated in one of the accounts
	Then there should be a link to the merged account from the source account				

@AccountMerge	
Scenario: Account Merge - End date on Account Relationship
	Given valid accounts exist with a Account listed in the Related Child Institution field
	And an valid account exists to receive a merge
	When the Merged to UCN is populated parent accounts
	Then the end date in the account relationship should be change to the current date

@AccountMerge	
Scenario Outline: Account Merge - Opportunities are moved
	Given two valid accounts exist
	And valid accounts exist with "<source>" number of Opportunities 
	And an valid account exists to receive a merge has "<target>" number of Opportunities
	When merge source account with target account 
	Then the Opportunities should be moved to the receiving account
	And the receiving account should have "<final>" number of Opportunities
	
	Examples:
		|source |target |final |
		|1 |0 |1 |
		|3 |0 |3 |
		|1 |1 |2 |
		|3 |1 |4 |
		|1 |3 |4 |
		|3 |3 |6 |
		
@AccountMerge
Scenario Outline: Account Merge - Tasks are moved
	Given two valid accounts exist
	And valid accounts exist with "<source>" number of Tasks
	And an valid account exists to receive a merge has "<target>" number of Tasks
	When merge source account with target account
	Then the Tasks should be moved to the receiving account 
	And the receiving account should have "<final>" number of Tasks
	
	Examples:
		|source |target |final |
		|1 |0 |1 |
		|3 |0 |3 |
		|1 |1 |2 |
		|3 |1 |4 |
		|1 |3 |4 |
		|3 |3 |6 |

@AccountMerge		
Scenario Outline: Account Merge - Contacts are moved
	Given two valid accounts exist
	And valid accounts exist with "<source>" number of Contacts
	And an valid account exists to receive a merge has "<target>" number of Contacts
	When merge source account with target account
	Then the Contacts should be moved to the receiving account 
	And the receiving account should have "<final>" number of Contacts
	
	Examples:
		|source |target |final |
		|1 |0 |1 |
		|3 |0 |3 |
		|1 |1 |2 |
		|3 |1 |4 |
		|1 |3 |4 |
		|3 |3 |6 |

@AccountMerge		
Scenario Outline: Account Merge - Events are moved
	Given two valid accounts exist
	And valid accounts exist with "<source>" number of Events 
	And an valid account exists to receive a merge has "<target>" number of Events
	When merge source account with target account
	Then the Events should be moved to the receiving account
	And the receiving account should  have "<final>" number of Events
	
	Examples:
		|source |target |final |
		|1 |0 |1 |
		|3 |0 |3 |
		|1 |1 |2 |
		|3 |1 |4 |
		|1 |3 |4 |
		|3 |3 |6 |								