Feature: datatos3 

@datatos3
Scenario Outline: SFDC Data to S3
	Given BI Data is in SFDC 
	When the SFDC "<type>" Data is loaded for BI
	Then the "<type>" files should saved in the "<S3 location>" 
	
	Examples:
	|type|S3 location|
	|account|project/sdc-salesforce-data-integration/account_details/staging/|
	|district|project/sdc-salesforce-data-integration/district_structure/staging/|
	|user|project/sdc-salesforce-data-integration/user_structure/staging/|
	|territory|project/sdc-salesforce-data-integration/territory_structure/staging/|

@datatos3
Scenario: SFDC Data to S3 file format
	Given BI Data is in SFDC
	When the SFDC Data is set to S3 by JustTranform
	Then the files saved should be CSV files

@datatos3
Scenario Outline: SFDC Data to S3 file name format
	Given BI Data is in SFDC
	When the SFDC "<type>" Data is set to S3 by JustTranform
	Then the "<type>" files should have a "<format>" naming convention
	
	Examples:
	|type|format|
	|Account|ACCOUNT-MMDDYYYYHHMMSS.csv|
	|District|DISTRICT-MMDDYYYYHHMMSS.csv|
	|User|USER-MMDDYYYYHHMMSS.csv|
	|Territory|TERRITORY-MMDDYYYYHHMMSS.csv|

@datatos3
Scenario Outline: SFDC Data file field validation
	Given Download latest "<type>" file from amazon server
	Then the "<type>" files should contain all fields for that type  
	
	Examples:
	|type|
	|Account data|
	|District data|
	|User data|
	|Territory data|

@datatos3	
Scenario Outline: SFDC Account Data validation
	Given Download latest "Account data" file from amazon server 
	When the "<id>" record is searched in Salesforce
	Then the values of the "<id>" record should match the "Account" record returned in the search results
	
	Examples:
		|id|
		|first|
		|10th|
		|50th|
		|last|	
		
@datatos3
Scenario Outline: SFDC User Data validation
	Given Download latest "User data" file from amazon server
	When the "<id>" user record is searched in Salesforce 
	Then the values of the "<id>" record should match the "User" record returned in the search results
	Examples:
		|id|
		|first|
		|10th|
		|50th|
		|last|			

@datatos3		
Scenario Outline: SFDC District Data validation
	Given Download latest "District data" file from amazon server
	When the "<id>" district record is searched in Salesforce
	Then the values of the "<id>" record should match the "District" record returned in the search results
	
	Examples:
		|id|
		|first|
		|10th|
		|50th|
		|last|

@datatos3		
Scenario Outline: SFDC Territory Data validation
	Given the user is logged into Sales Force
	When the "<id>" territory record is searched in Salesforce 
	Then the values of the "<id>" record should match the "Territory" record returned in the search results
	Examples:
	|id|
	|first|
	|100th|
	|500th|
	|last|				