package com.scholastic.salesforce.automation.beans;

public class OTCXMLBean {

	String ContractLinePrice;
	String StartDate;
	String EndDate;
	String CorpIDAndContractNumber;
	String OTCLineQuantity;
	String OTCAction;
	String InstallToUCN;
	String ISBN;
	String Title;
	String SalesOrderNumber;
	String SalesOrderLineNumber;
	String LineStatus;
	String ItemNumber;

	public String getItemNumber() {
		return ItemNumber;
	}

	public void setItemNumber(String itemNumber) {
		ItemNumber = itemNumber;
	}

	public String getLineStatus() {
		return LineStatus;
	}

	public void setLineStatus(String lineStatus) {
		LineStatus = lineStatus;
	}

	public String getSalesOrderNumber() {
		return SalesOrderNumber;
	}

	public void setSalesOrderNumber(String salesOrderNumber) {
		SalesOrderNumber = salesOrderNumber;
	}

	public String getSalesOrderLineNumber() {
		return SalesOrderLineNumber;
	}

	public void setSalesOrderLineNumber(String salesOrderLineNumber) {
		SalesOrderLineNumber = salesOrderLineNumber;
	}

	public String getInstallToUCN() {
		return InstallToUCN;
	}

	public void setInstallToUCN(String installToUCN) {
		InstallToUCN = installToUCN;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public void setContractLinePrice(String contractLinePrice) {
		ContractLinePrice = contractLinePrice;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate;
	}

	public void setCorpIDAndContractNumber(String corpIDAndContractNumber) {
		CorpIDAndContractNumber = corpIDAndContractNumber;
	}

	public void setOTCLineQuantity(String oTCLineQuantity) {
		OTCLineQuantity = oTCLineQuantity;
	}

	public void setOTCAction(String oTCAction) {
		OTCAction = oTCAction;
	}

	public String getContractLinePrice() {
		return ContractLinePrice;
	}

	public String getStartDate() {
		return StartDate;
	}

	public String getEndDate() {
		return EndDate;
	}

	public String getCorpIDAndContractNumber() {
		return CorpIDAndContractNumber;
	}

	public String getOTCLineQuantity() {
		return OTCLineQuantity;
	}

	public String getOTCAction() {
		return OTCAction;
	}

	@Override
	public String toString() {
		return "OTCXMLBean [ContractLinePrice=" + ContractLinePrice + ", StartDate=" + StartDate + ", EndDate="
				+ EndDate + ", CorpIDAndContractNumber=" + CorpIDAndContractNumber + ", OTCLineQuantity="
				+ OTCLineQuantity + ", OTCAction=" + OTCAction + "]";
	}

}
