package com.scholastic.salesforce.automation.beans;

import com.scholastic.salesforce.automation.support.ConstantUtils;
import com.scholastic.salesforce.automation.support.RandomDataUtil;

public class InstituteBean {
	private String InstituteName;
	private String PostalCode;
	private String ZipCode;
	private String AddressOne;
	private String PhoneNumber;
	private String CustomerGroup;
	private String CustomerType;
	private String Enrollment;

	public String getInstituteName() {
		return InstituteName;
	}

	public void setInstituteName(String instituteName) {
		InstituteName = instituteName;
	}

	public String getPostalCode() {
		return PostalCode;
	}

	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}

	public String getZipCode() {
		return ZipCode;
	}

	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}

	public String getAddressOne() {
		return AddressOne;
	}

	public void setAddressOne(String addressOne) {
		AddressOne = addressOne;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getCustomerGroup() {
		return CustomerGroup;
	}

	public void setCustomerGroup(String customerGroup) {
		CustomerGroup = customerGroup;
	}

	public String getCustomerType() {
		return CustomerType;
	}

	public void setCustomerType(String customerType) {
		CustomerType = customerType;
	}

	public String getEnrollment() {
		return Enrollment;
	}

	public void setEnrollment(String enrollment) {
		Enrollment = enrollment;
	}

	public static InstituteBean getInstituteDataBean() {
		InstituteBean insBean = new InstituteBean();
		String InstituteName = RandomDataUtil.getRandomInstituteName();
		insBean.setInstituteName(InstituteName);
		insBean.setAddressOne(ConstantUtils.INSTITUTE_ADDRESS_ONE);
		insBean.setPostalCode(ConstantUtils.INSTITUTE_PIN);
		insBean.setZipCode(ConstantUtils.INSTITUTE_ZIP4);
		insBean.setPhoneNumber(ConstantUtils.INSTITUTE_PHONE);
		insBean.setEnrollment(ConstantUtils.INSTITUTE_ENROLLMENT);
		return insBean;
	}
}
