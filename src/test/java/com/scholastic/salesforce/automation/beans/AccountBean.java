package com.scholastic.salesforce.automation.beans;

public class AccountBean {

	// index 2 in Account file
	private String accountName;
	private String phone;
	private String customerType;
	private String organizationType;
	private String institutionStatus;
	private String numberOfSchool;
	private String africanAmericanPer;
	private String asianAmericanPer;
	private String hispanicAmericanPer;
	private String nativeAmericanPer;
	private String mailingStreet;
	private String mailingCity;
	private String mailingCounty;
	private String mailingState;
	private String mailingZip;
	private String mailingCountry;
	private String fax;

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(String organizationType) {
		this.organizationType = organizationType;
	}

	public String getInstitutionStatus() {
		return institutionStatus;
	}

	public void setInstitutionStatus(String institutionStatus) {
		this.institutionStatus = institutionStatus;
	}

	public String getNumberOfSchool() {
		return numberOfSchool;
	}

	public void setNumberOfSchool(String numberOfSchool) {
		this.numberOfSchool = numberOfSchool;
	}

	public String getAfricanAmericanPer() {
		return africanAmericanPer;
	}

	public void setAfricanAmericanPer(String africanAmericanPer) {
		this.africanAmericanPer = africanAmericanPer;
	}

	public String getAsianAmericanPer() {
		return asianAmericanPer;
	}

	public void setAsianAmericanPer(String asianAmericanPer) {
		this.asianAmericanPer = asianAmericanPer;
	}

	public String getHispanicAmericanPer() {
		return hispanicAmericanPer;
	}

	public void setHispanicAmericanPer(String hispanicAmericanPer) {
		this.hispanicAmericanPer = hispanicAmericanPer;
	}

	public String getNativeAmericanPer() {
		return nativeAmericanPer;
	}

	public void setNativeAmericanPer(String nativeAmericanPer) {
		this.nativeAmericanPer = nativeAmericanPer;
	}

	public String getMailingStreet() {
		return mailingStreet;
	}

	public void setMailingStreet(String mailingStreet) {
		this.mailingStreet = mailingStreet;
	}

	public String getMailingCity() {
		return mailingCity;
	}

	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}

	public String getMailingCounty() {
		return mailingCounty;
	}

	public void setMailingCounty(String mailingCounty) {
		this.mailingCounty = mailingCounty;
	}

	public String getMailingState() {
		return mailingState;
	}

	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}

	public String getMailingZip() {
		return mailingZip;
	}

	public void setMailingZip(String mailingZip) {
		this.mailingZip = mailingZip;
	}

	public String getMailingCountry() {
		return mailingCountry;
	}

	public void setMailingCountry(String mailingCountry) {
		this.mailingCountry = mailingCountry;
	}

	@Override
	public String toString() {
		return "AccountBean [accountName=" + accountName + ", phone=" + phone + ", customerType=" + customerType
				+ ", organizationType=" + organizationType + ", institutionStatus=" + institutionStatus
				+ ", numberOfSchool=" + numberOfSchool + ", africanAmericanPer=" + africanAmericanPer
				+ ", asianAmericanPer=" + asianAmericanPer + ", hispanicAmericanPer=" + hispanicAmericanPer
				+ ", nativeAmericanPer=" + nativeAmericanPer + ", mailingStreet=" + mailingStreet + ", mailingCity="
				+ mailingCity + ", mailingCounty=" + mailingCounty + ", mailingState=" + mailingState + ", mailingZip="
				+ mailingZip + ", mailingCountry=" + mailingCountry + "]";
	}
}
