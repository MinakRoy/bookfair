package com.scholastic.salesforce.automation.beans;

import org.json.JSONArray;
import org.json.JSONObject;

import com.scholastic.salesforce.automation.support.ConstantUtils;
import com.scholastic.salesforce.automation.support.RandomDataUtil;
import com.scholastic.torque.api.APIClient;
import com.scholastic.torque.api.JSONParser;
import com.scholastic.torque.common.TestBaseProvider;
import com.sun.jersey.api.client.ClientResponse;

public class FieldServiceContractBean {
	private String contractnumber;
	private String installToUCN;
	private String contractLineNumber;
	private String itemNumber;
	private String reasonCode;
	private String lineQuantity;
	private String dateTerminated;
	private String startDate;
	private String endDate;
	private String externalContractID;
	private String salesOrderNumber;
	private String salesOrderLineNumber;
	private String assignmentID;
	private String ISBN;
	private String title;

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSalesOrderNumber() {
		return salesOrderNumber;
	}

	public void setSalesOrderNumber(String salesOrderNumber) {
		this.salesOrderNumber = salesOrderNumber;
	}

	public String getSalesOrderLineNumber() {
		return salesOrderLineNumber;
	}

	public void setSalesOrderLineNumber(String salesOrderLineNumber) {
		this.salesOrderLineNumber = salesOrderLineNumber;
	}

	public String getAssignmentID() {
		return assignmentID;
	}

	public void setAssignmentID(String assignmentID) {
		this.assignmentID = assignmentID;
	}

	public String getContractnumber() {
		return contractnumber;
	}

	public void setContractnumber(String contractnumber) {
		this.contractnumber = contractnumber;
	}

	public String getInstallToUCN() {
		return installToUCN;
	}

	public void setInstallToUCN(String installToUCN) {
		this.installToUCN = installToUCN;
	}

	public String getContractLineNumber() {
		return contractLineNumber;
	}

	public void setContractLineNumber(String contractLineNumber) {
		this.contractLineNumber = contractLineNumber;
	}

	public String getItemNumber() {
		return itemNumber;
	}

	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getLineQuantity() {
		return lineQuantity;
	}

	public void setLineQuantity(String lineQuantity) {
		this.lineQuantity = lineQuantity;
	}

	public String getDateTerminated() {
		return dateTerminated;
	}

	public void setDateTerminated(String dateTerminated) {
		this.dateTerminated = dateTerminated;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getExternalContractID() {
		return externalContractID;
	}

	public void setExternalContractID(String externalContractID) {
		this.externalContractID = externalContractID;
	}

	public static FieldServiceContractBean getRandomFieldServiceContractBean() {
		FieldServiceContractBean fsBean = new FieldServiceContractBean();
		if (TestBaseProvider.getTestBase().getContext().getString("salesforce", "true").equals("true")) {
			String contractNumber = RandomDataUtil.getRandomNumber(8);
			String contractLineNumber = RandomDataUtil.getRandomNumber(4);
			String externalContractID = contractNumber + contractLineNumber;
			fsBean.setInstallToUCN(RandomDataUtil.getRandomNumber(5));
			fsBean.setItemNumber(RandomDataUtil.getRandomNumber(6));
			fsBean.setContractnumber(RandomDataUtil.getRandomNumber(8));
			fsBean.setContractLineNumber(contractLineNumber);
			fsBean.setExternalContractID(externalContractID);
			fsBean.setLineQuantity(ConstantUtils.LINE_QTY);
			fsBean.setAssignmentID(RandomDataUtil.getRandomNumber(8));
			fsBean.setISBN(RandomDataUtil.getRandomNumber(6));
			fsBean.setSalesOrderLineNumber(RandomDataUtil.getRandomNumber(8));
			fsBean.setSalesOrderNumber(RandomDataUtil.getRandomNumber(8));
		} else {
			ClientResponse response = APIClient.GET("");
			String output = response.getEntity(String.class);
			JSONArray jsonArray = JSONParser.getJsonArray(output);
			int jsonArrayLength = jsonArray.length();
			for (int index = 0; index < jsonArrayLength; index++) {
				JSONObject jsonObject = JSONParser.getJsonObject(jsonArray, index);
				System.out.println(jsonObject);
				System.out.println("Contract Number : " + JSONParser.getValue(jsonObject, ""));
				System.out.println("Contract Line Number : " + JSONParser.getValue(jsonObject, ""));
				System.out.println("External Contract Number : " + JSONParser.getValue(jsonObject, "amount"));
			}
		}
		return fsBean;
	}
}
