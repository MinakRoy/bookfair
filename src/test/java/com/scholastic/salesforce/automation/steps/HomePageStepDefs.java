package com.scholastic.salesforce.automation.steps;

import org.testng.Assert;

import com.scholastic.salesforce.automation.pages.AccountsPage;
import com.scholastic.salesforce.automation.pages.HomePage;
import com.scholastic.salesforce.automation.support.ConstantUtils;
import com.scholastic.salesforce.automation.support.OTCXMLGenerate;
import com.scholastic.salesforce.automation.support.SCHUtils;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePageStepDefs {

	HomePage homePage = new HomePage();

	@Given("^user navigate to the application$")
	public void userNavigatesToApplication() {
		homePage.launchPage();
	}

	@Given("^the user is logged into Sales Force$")
	public void userLoginInToSystem() {
		String username = TestBaseProvider.getTestBase().getString("username");
		String password = TestBaseProvider.getTestBase().getString("password");
		homePage.doLogin(username, password);
	}

	@Given("^the user is logged in to Sales Force as a \"([^\"]*)\" user$")
	public void userLoginWithSalesForce(String userType) {
		String username = null, password = null;
		if (userType.equalsIgnoreCase("toni")) {
			username = ConstantUtils.USERTYPE_TONI_USERNAME;
			password = ConstantUtils.USERTYPE_TONI_PASSWORD;
		} else if (userType.equalsIgnoreCase("csr")) {
			username = ConstantUtils.USERTYPE_CSR_USERNAME;
			password = ConstantUtils.USERTYPE_CSR_PASSWORD;
		} else {
			Assert.fail("Wrong usertype passed.");
		}
		homePage.doLogin(username, password);
	}

	@Given("^the user is logged into SalesForce as a Ed Group user$")
	public void userLoginToSFWithEdGroup() {
		String username = TestBaseProvider.getTestBase().getString("ed.group.username");
		String password = TestBaseProvider.getTestBase().getString("ed.group.password");
		homePage.doLogin(username, password);
	}

	@And("^the user is logged out from \"([^\"]*)\"$")
	public void userLoggedOutFrom(String application) {
		homePage.doLogOut(application);
	}

	/*
	 * pause of 20 minutes because from CDM to receive data to SFDC taking time
	 */
	@When("^the new account details are received by SFDC$")
	public void the_new_account_details_are_received_by_SFDC() {
		userLoginInToSystem();
		SCHUtils.waitforDataToSyncInCDB();
		homePage.navigateToMenu("Accounts");
	}

	@And("^the addresses are populated with Address type details$")
	public void the_addresses_are_populated_with_Address_type_details() {
		homePage.addressesArePopulatedWithAddressTypeDetails();
	}

	@And("^the one address populated should be the Mailing Address$")
	public void the_one_address_populated_should_be_the_Mailing_Address() {
		homePage.oneAddressPopulatedShouldMailingAddress();
	}

	@Then("^a new Account Relationship should be created with the new Parent Account$")
	public void a_new_Account_Relationship_should_be_created_with_the_new_Parent_Account() {
		homePage.newAccountRelationshipCreatedWithTheNewParentAccount();
	}

	@Then("^there should be a link to the merged account from the source account$")
	public void there_should_be_a_link_to_the_merged_account_from_the_source_account() {
		homePage.thereShouldBeLinkToMergedAccountFromTheSourceAccount();
	}

	@Then("^the end date in the account relationship should be change to the current date$")
	public void the_Account_listed_in_the_Related_Child_Institution_field_is_change_to_the_current_date() {
		homePage.accountListedInRelatedChildInstitutionFieldChangeToCurrentDate();
	}

	@Then("^the end user in SalesForce should not be able to make an update to the \"([^\"]*)\"$")
	public void the_end_user_in_SalesForce_should_not_be_able_to_make_an_update_to_the(String arg1) {
		homePage.userShouldNotbeAbleToMakeUpdateIn(arg1);
	}

	@And("^valid accounts exist with \"([^\"]*)\" number of Opportunities$")
	public void valid_accounts_exist_with_number_of_Opportunities(String arg1) {
		homePage.numberOfOpportunitiesExist(arg1);
	}

	@And("^an valid account exists to receive a merge has \"([^\"]*)\" number of Opportunities$")
	public void an_valid_account_exists_to_receive_a_merge_has_number_of_Opportunities(String arg1) {
		homePage.validAccountExistsToReceiveAMergeNumberOfOpportunities(arg1);
	}

	@And("^valid accounts exist with \"([^\"]*)\" number of Tasks$")
	public void valid_accounts_exist_with_number_of_Tasks(String arg1) {
		userLoginInToSystem();
		homePage.navigateToMenu("Accounts");
		homePage.numberOfTasksExists(arg1);
	}

	@And("^an valid account exists to receive a merge has \"([^\"]*)\" number of Tasks$")
	public void an_valid_account_exists_to_receive_a_merge_has_number_of_Tasks(String arg1) {
		homePage.validAccountExistsToReceiveMergeNumberofTasks(arg1);
	}

	@And("^valid accounts exist with \"([^\"]*)\" number of Contacts$")
	public void valid_accounts_exist_with_number_of_Contacts(String arg1) {
		userLoginInToSystem();
		homePage.navigateToMenu("Accounts");
		homePage.numberOfContactsExists(arg1);
	}

	@And("^an valid account exists to receive a merge has \"([^\"]*)\" number of Contacts$")
	public void an_valid_account_exists_to_receive_a_merge_has_number_of_Contacts(String arg1) {
		homePage.validAccountExistsToReceiveMergeNumberofContacts(arg1);
	}

	@And("^valid accounts exist with \"([^\"]*)\" number of Events$")
	public void valid_accounts_exist_with_number_of_Events(String arg1) {
		userLoginInToSystem();
		homePage.navigateToMenu("Accounts");
		homePage.numberOfEventsExists(arg1);
	}

	@And("^an valid account exists to receive a merge has \"([^\"]*)\" number of Events$")
	public void an_valid_account_exists_to_receive_a_merge_has_number_of_Events(String arg1) {
		homePage.validAccountExistsToReceiveMergeNumberofEvents(arg1);
	}

	@Given("^that an account exists with \"([^\"]*)\" in SalesForce$")
	public void that_an_account_exists_with_in_SalesForce(String arg1) {
		homePage.anAccountExistWithInSalesForce(arg1);
	}

	@Then("^end user in SalesForce should not be able to make an update to the \"([^\"]*)\"$")
	public void endUserInSalesforceShouldNotbeABletoMakeanUpdateTo(String arg1) {
		userLoginToSFWithEdGroup();
		homePage.endUserInSalesforceShouldNotbeABletoMakeanUpdateTo(arg1);
	}

	@Then("^the data should process successfully$")
	public void the_data_should_process_successfully() {
		userLoginInToSystem();
		homePage.dataShouldProcessSuccessfully();
	}

	@And("^a account from the file should be present in SalesForce$")
	public void a_account_from_the_file_should_be_present_in_SalesForce() {
		homePage.accountFromTheFileisUpdatedInAccount();
	}

	@Then("^the identical entry should not create a duplicate in Salesforce$")
	public void the_identical_entry_should_not_create_a_duplicate_in_Salesforce() {
		userLoginInToSystem();
		homePage.the_identical_entry_should_not_create_a_duplicate_in_Salesforce();
	}

	@Then("^the data from the first batch should process successfully$")
	public void the_data_from_the_first_batch_should_process_successfully() {
		userLoginInToSystem();
		homePage.the_data_from_the_first_batch_should_process_successfully();
	}

	@And("the data that is outside the first batch of records should not be processed")
	public void the_data_that_is_outside_the_first_batch_of_records_should_not_be_processed() {
		// userLoginInToSystem();
		homePage.the_data_that_is_outside_the_first_batch_of_records_should_not_be_processed();
	}

	public void settingPreConditionForBatchProcessing() {
		userLoginInToSystem();
		AccountsPage accountPage = new AccountsPage();
		accountPage.settingPreConditionForBatchProcessing();
	}

	@When("^it is successful$")
	public void it_is_successful() {
		userLoginInToSystem();
		AccountsPage accountPage = new AccountsPage();
		accountPage.itIsSuccessful();
	}

	@Then("^the entry should be available in SalesForce$")
	public void the_entry_should_be_available_in_SalesForce() {
		AccountsPage accountPage = new AccountsPage();
		accountPage.entryShouldBeAvailableInSFDC();
	}

	@When("^the values for all the fields should be present in SalesForce$")
	public void the_values_for_all_the_fields_should_be_present_in_SalesForce() {
		AccountsPage accountPage = new AccountsPage();
		accountPage.valuesForAllTheFieldsShouldBePresentInSalesForce();
	}

	@Given("^a user is logged into SalesForce as a Scheduler$")
	public void a_user_is_logged_into_SalesForce_as_a_Scheduler() {
		String username = TestBaseProvider.getTestBase().getString("username");
		String password = TestBaseProvider.getTestBase().getString("password");
		homePage.doLogin(username, password);
	}

	@When("^the \"([^\"]*)\" user record is searched in Salesforce$")
	public void the_user_record_is_Searched_in_Salesforce(String recordNumber) {
		userLoginInToSystem();
		homePage.the_user_record_is_Searched_in_Salesforce(recordNumber);
	}

	@Given("^a contracted is created with all values$")
	public void a_contracted_is_created_with_all_values() {
		OTCXMLGenerate OTCXML = new OTCXMLGenerate();
		OTCXML.generateOTCXMLWithAllValues();
		OTCXML.placeGeneratedOTCXMLIntoActiveMQPath();
	}

	@Then("^the contract should be created with each of the values$")
	public void the_contract_should_be_created_with_each_of_the_values() {
		userLoginInToSystem();
		SCHUtils.waitforContractToSyncInSFDCFromOTC();
		homePage.the_contract_should_be_created_with_each_of_the_values();
	}

	@Given("^a new contract is created with an Product Number which is not a Service Product$")
	public void a_new_contract_is_created_with_an_Product_Number_which_is_not_a_Service_Product() {
		OTCXMLGenerate OTCXML = new OTCXMLGenerate();
		OTCXML.generateOTCXMLWithNotServiceProductNumber();
		OTCXML.placeInvalidProductNumberOTCXMLIntoActiveMQPath();
	}

	@Given("^a new contract is created with an inactive Product Number$")
	public void a_new_contract_is_created_with_an_inactive_Product_Number() {
		OTCXMLGenerate OTCXML = new OTCXMLGenerate();
		OTCXML.generateOTCXMLInActiveProductNumber();
		OTCXML.placeInactiveProductNumberOTCXMLIntoActiveMQPath();
	}

	@Given("^a contract is created from OTC$")
	public void a_contract_is_created_from_OTC() {
		a_contracted_is_created_with_all_values();
	}

	@Then("^a contract should be created in SFDC$")
	public void a_contract_should_be_created_in_SFDC() {
		the_contract_should_be_created_with_each_of_the_values();
	}

}
