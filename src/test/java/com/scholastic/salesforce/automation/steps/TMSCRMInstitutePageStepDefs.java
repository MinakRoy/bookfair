package com.scholastic.salesforce.automation.steps;

import com.scholastic.salesforce.automation.pages.TMSCRMInstitutePage;
import com.scholastic.salesforce.automation.support.SCHUtils;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TMSCRMInstitutePageStepDefs {
	TMSCRMInstitutePage tmsCRMInstitutePage = new TMSCRMInstitutePage();

	@When("^the Merged to UCN is populated in one of the accounts$")
	public void the_Merged_to_UCN_is_populated_in_one_of_the_accounts() {
		tmsCRMInstitutePage.mergeToUCNInOneAccounts();
	}

	@When("the Merged to UCN is populated parent accounts")
	public void the_Merged_to_UCN_is_populated_parent_accounts() {
		tmsCRMInstitutePage.mergeToUCNToParentAccount();
	}

	@Then("^status of that account should be updated to Terminated$")
	public void status_of_that_account_should_be_updated_to_Terminated() {
		SCHUtils.waitforDataToSyncInCDB();
		tmsCRMInstitutePage.verifyAccountStatusUpdatedToTerminated();
	}

	@When("^the Enterprise Status on an account is updated to Closed$")
	public void the_Enterprise_Status_on_an_account_is_updated_to_Closed() {
		tmsCRMInstitutePage.enterpriseStatusUpdatedToClosed();
	}
}
