package com.scholastic.salesforce.automation.steps;

import com.scholastic.salesforce.automation.pages.FieldServiceContractPage;
import com.scholastic.salesforce.automation.support.SCHUtils;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FieldServiceContractStepDefs {

	FieldServiceContractPage fieldServiceContractPage = new FieldServiceContractPage();

	@When("^a new contract is created with \"([^\"]*)\"$")
	public void createContractWithInvalidUCN(String param) {
		fieldServiceContractPage.createContractWith(param);
	}

	@Then("^the contract should be created with the \"([^\"]*)\" that is not mapped$")
	public void contractCreatedWithInvalidUCN(String param) {
		fieldServiceContractPage.contractCreatedWith(param);
	}

	@And("^the field service should be created with the invalid Install to UCN that is not mapped$")
	public void fieldServiceShouldBeCreatedWithTheInvalidInstallToUCN() {
		fieldServiceContractPage.fieldServiceShouldBeCreatedWithTheInvalidInstallToUCN();
	}

	@And("^a contract was created with an invalid Install to UCN$")
	public void contractWasCreatedWithInvalidInstallToUCN() {
		fieldServiceContractPage.contractWasCreatedWithInvalidInstallToUCN();
	}

	@And("^field services were created with an invalid Install to UCN$")
	public void fieldServiceWereCreatedWithInvalidInstallToUCN() {
		fieldServiceContractPage.fieldServiceWereCreatedWithInvalidInstallToUCN();
	}

	@When("^contract is updated$")
	public void contractIsUpdated() {
		fieldServiceContractPage.contractIsUpdated();
	}

	@And("update the line quantity to \"([^\"]*)\"")
	public void updateTheLineQuantity(String value) {
		fieldServiceContractPage.updateTheLineQuantity(value);
	}

	@Then("^the contract should contain the updated values with the invalid Install to UCN that is not mapped$")
	public void contractShouldContainTheUpdatedValuesWithInvalidInstallToUCN() {
		fieldServiceContractPage.contractShouldContainTheUpdatedValuesWithInvalidInstallToUCN();
	}

	@And("^the field service should contain the updated values with the invalid Install to UCN that is not mapped$")
	public void fieldServiceShouldContainTheUpdatedValues() {
		fieldServiceContractPage.fieldServiceShouldContainTheUpdatedValues();
	}

	@And("^the field services should be created with the invalid Product Number that is not mapped$")
	public void fieldServicesShouldBeCreatedWithTheInvalidProductNumberThatIsNotMapped() {
		fieldServiceContractPage.fieldServicesShouldBeCreatedWithTheInvalidProductNumberThatIsNotMapped();
	}

	@And("^the contract should be flagged as pending$")
	public void contractShouldBeFlaggedAsPending() {
		fieldServiceContractPage.contractShouldBeFlaggedAsPending();
	}

	@And("^the field services should be flagged as pending$")
	public void fieldServiceShouldBeFlaggedAsPending() {
		fieldServiceContractPage.fieldServiceShouldBeFlaggedAsPending();
	}

	@And("^a contract was created with an invalid Product Number$")
	public void contractWasCreatedWithAnInvalidProductNumber() {
		fieldServiceContractPage.contractWasCreatedWithAnInvalildProductNumber();
	}

	@Then("^the contract should contain the updated values with the invalid Product Number that is not mapped$")
	public void contractShouldContainTheUpdatedValueWithInvalidProductNumberThatIsNotMapped() {
		fieldServiceContractPage.contractShouldContainUpdatedValueWithInvalidProductNumberThatIsNotMapped();
	}

	@And("^the field services should contain the updated values with the invalid Product Number that is not mapped$")
	public void fieldServiceShouldContainTheUpdatedValuesWithTheInvalidProductNumberThatIsNotMapped() {
		fieldServiceContractPage.fieldServiceShouldContainTheUpdatedValuesWithTheInvalidProductNumberThatIsNotMapped();
	}

	@Then("^Product Number in the contract should be mapped to the created Product Number$")
	public void product_Number_in_the_contract_should_be_mapped_to_the_created_Product_Number() {
		fieldServiceContractPage.productNumberInTheContractShouldBeMappedToTheCreatedProductNumber();
	}

	@And("^Product Number in the field services should be mapped to the created Product Number$")
	public void product_Number_in_the_field_services_should_be_mapped_to_the_created_Product_Number() {
		fieldServiceContractPage.productnumberInTheFieldServiceShouldBeMappedToCreatedProductNumber();
	}

	@And("^the contract should no longer be flagged as pending$")
	public void the_contract_should_no_longer_be_flagged_as_pending() {
	}

	@And("^the field services should no longer be flagged as pending$")
	public void the_field_services_should_no_longer_be_flagged_as_pending() {
	}

	@And("a contract already exists with an invalid Product Number")
	public void contractAlreadyExistedWithAnInvalidProductNumber() {
		fieldServiceContractPage.contractAlreadyExistedWithAnInvalidProductNumber();
	}

	@And("^a contract already exists with an invalid Install to UCN$")
	public void contractAlreadyExistedWithAnInvalidInstallToUCN() {
		fieldServiceContractPage.contractAlreadyExistedWithAnInvalidInstallToUCN();
	}

	@Then("^the Install to UCN in the contract should be mapped to the created client$")
	public void the_Install_to_UCN_in_the_contract_should_be_mapped_to_the_created_client() {
		fieldServiceContractPage.theInstallToUCNIntheContractShouldBeMappedToCreatedClient();
	}

	@And("^the Install to UCN in the field services should be mapped to the created client$")
	public void the_Install_to_UCN_in_the_field_services_should_be_mapped_to_the_created_client() {
		fieldServiceContractPage.installToUCNInFieldServicesShouldBeMappedToTheCreatedClient();
	}

	@Then("^the contract should be created with the Product Number which is not a Service Product that is not mapped$")
	public void the_contract_should_be_created_with_the_Product_Number_which_is_not_a_Service_Product_that_is_not_mapped() {
		fieldServiceContractPage.contractCreatedWith("Product Number which is not a Service Product");
	}

	@And("^the field services should be created with the Product Number which is not a Service Product that is not mapped$")
	public void the_field_services_should_be_created_with_the_Product_Number_which_is_not_a_Service_Product_that_is_not_mapped() {
		fieldServiceContractPage.fieldServicesShouldBeCreatedWithTheProductNumberWhichIsNotServiceProduct();
	}

	@Then("^the contract should be created without dates$")
	public void the_contract_should_be_created_without_dates() {
		fieldServiceContractPage.contractShouldBeCreatedWithoutDates();
	}

	@And("^the field services should be created without dates$")
	public void the_field_services_should_be_created_without_dates() {
		fieldServiceContractPage.fieldServicesShouldBeCreatedWithoutDates();
	}

	@And("^a contract already exists with blank start-end dates$")
	public void a_contract_already_exists_with_blank_start_end_dates() {
		fieldServiceContractPage.contractAlreadyExistedWithBlankstartEndDate();
	}

	@When("^the contract is updated with valid start-end dates$")
	public void the_contract_is_updated_with_valid_start_end_dates() {
		fieldServiceContractPage.contractIsUpdatedWithValidStartEndDates();
	}

	@Then("^start-end dates in the contract should be populated$")
	public void start_end_dates_in_the_contract_should_be_populated() {
		fieldServiceContractPage.startEndDatesIntheContractShouldBePopulated();
	}

	@And("^start-end dates in the field services should be populated$")
	public void start_end_dates_in_the_field_services_should_be_populated() {
		fieldServiceContractPage.startEndDatesInFieldServicesShouldBePopulated();
	}

	@And("^a contract was created with a Product Number which is not a Service Product$")
	public void a_contract_was_created_with_a_Product_Number_which_is_not_a_Service_Product() {
		fieldServiceContractPage.contractWasCreatedWithProductNumberIsNotServiceProduct();
	}

	@Then("^the contract should contain the updated values with the Product Number which is not a Service Product that is not mapped$")
	public void the_contract_should_contain_the_updated_values_with_the_Product_Number_which_is_not_a_Service_Product_that_is_not_mapped() {
		fieldServiceContractPage.contractShouldContainTheUpdatedValuesWithTheProductNumberWhichIsNotServiceProduct();
	}

	@And("^the field services should contain the updated values with the Product Number which is not a Service Product that is not mapped$")
	public void the_field_services_should_contain_the_updated_values_with_the_Product_Number_which_is_not_a_Service_Product_that_is_not_mapped() {
		fieldServiceContractPage
				.fieldServiceShouldContainTheUpdatedValuesWithTheProductNumberWhichIsNotServiceProduct();
	}

	@Then("^the contract should be created with the inactive Product Number$")
	public void the_contract_should_be_created_with_the_inactive_Product_Number() {
		fieldServiceContractPage.contractCreatedWith("an inactive Product Number");
	}

	@And("^the field services should be created with the inactive Product Number$")
	public void the_field_services_should_be_created_with_the_inactive_Product_Number() {
		fieldServiceContractPage.fieldServiceShouldBecreatedWithTheInactiveProductNumber();
	}

	@And("^a contract was created with a inactive Product Number$")
	public void a_contract_was_created_with_a_inactive_Product_Number() {
		fieldServiceContractPage.createContractWith("an inactive Product Number");
		fieldServiceContractPage.contractCreatedWith("an inactive Product Number");
		fieldServiceContractPage.fieldServiceShouldBecreatedWithTheInactiveProductNumber();
	}

	@Then("^the contract should contain the updated values with the inactive Product Number$")
	public void the_contract_should_contain_the_updated_values_with_the_inactive_Product_Number() {
		fieldServiceContractPage.contractShouldContainTheUpdatedValuesWithInactiveProductNumber();
	}

	@And("^the field services should contain the updated values with the inactive Product Number$")
	public void the_field_services_should_contain_the_updated_values_with_the_inactive_Product_Number() {
		fieldServiceContractPage.fieldserviceShouldContainTheUpdatedValuesWithInactiveProductNumber();
	}

	@And("^a contract already exists with a inactive Product Number$")
	public void a_contract_already_exists_with_a_inactive_Product_Number() {
		fieldServiceContractPage.createContractWith("an inactive Product Number");
	}

	@Given("^a contract already exists$")
	public void contract_already_exists() {
		fieldServiceContractPage.contractAlreadyExisted();
	}

	@When("^the \"([^\"]*)\" in the contract is updated$")
	public void userUpdateField(String field) {
		fieldServiceContractPage.updateField(field);
	}

	@Then("^the \"([^\"]*)\" in the field service should be changed$")
	public void fieldServiceShouldBeChanged(String field) {
		fieldServiceContractPage.verifyUpdatedField(field);
	}

	@When("^the Install to UCN is updated$")
	public void the_Install_to_UCN_is_updated() {
		fieldServiceContractPage.installtoUCNIsUpdated();
	}

	@Then("^Install to UCN in the contract should be changed$")
	public void Install_to_UCN_in_the_contract_should_be_changed() {
		fieldServiceContractPage.intallToUCNInContractShouldBeChanged();
	}

	@Given("a contract already exists with \"([^\"]*)\" line quantity$")
	public void a_contract_already_exists_with_line_quantity_items(String arg1) {
		TestBaseProvider.getTestBase().getContext().setProperty("FSC_LINE_QUANTITY", arg1);
		fieldServiceContractPage.createContractWith("Line Quantity");
	}

	@When("^the contract is \"([^\"]*)\"$")
	public void the_contract_is_terminated(String contractStatus) {
		fieldServiceContractPage.contractIs(contractStatus);
	}

	@And("^the contract contains \"([^\"]*)\" open status field services$")
	public void the_contract_contains_open_status_field_services(String arg1) {
		fieldServiceContractPage.contractContrainsOpenStatusFieldServices(arg1);
	}

	@And("^the contract contains \"([^\"]*)\" assigned status field services$")
	public void the_contract_contains_assigned_status_field_services(String arg1) {
		fieldServiceContractPage.contractContainsAssignedStatusFieldServices(arg1);
	}

	@And("^the contract contains \"([^\"]*)\" delivered status field services$")
	public void the_contract_contains_delivered_status_field_services(String arg1) {
		fieldServiceContractPage.contractContainsDeliveredStatusFieldServices(arg1);
	}

	@And("^the contract contains \"([^\"]*)\" cancelled status field services$")
	public void the_contract_contains_cancelled_status_field_services(String arg1) {
		fieldServiceContractPage.contractContainsCancelledStatusFieldServices(arg1);
	}

	@And("^the contract contains \"([^\"]*)\" cancelled/delivered status field services$")
	public void the_contract_contains_cancelled_delivered_status_field_services(String arg1) {
		fieldServiceContractPage.contractContainsCancelledDeliveredStatusFieldServices(arg1);
	}

	@Then("^the contract should contain \"([^\"]*)\" cancelled status field services$")
	public void the_contract_should_contain_cancelled_status_field_services(String arg1) {
		fieldServiceContractPage.contractShouldContainCancelledStatusFieldServices(arg1);
	}

	@And("^the contract should contain \"([^\"]*)\" cancelled/delivered status field Delivered$")
	public void the_contract_should_contain_cancelled_delivered_status_field_Delivered(String arg1) {
		fieldServiceContractPage.contractShouldContainCancelledDeliveredStatusFieldDelivered(arg1);
	}

	@And("^user clicks on view all Work Orders$")
	public void userClickOnViewAll() {
		fieldServiceContractPage.clickOnViewAllWOrkOrders();
	}

	@And("^the contract should contain \"([^\"]*)\" cancelled/delivered status field$")
	public void the_contract_should_contain_cancelled_delivered_status_field(String arg1) {
		fieldServiceContractPage.contractShouldContainCancelledDeliveredStatusField(arg1);
	}

	@When("^the Line Item Quantity is updated to \"([^\"]*)\"$")
	public void the_Line_Item_Quantity_is_updated_to(String arg1) {
		fieldServiceContractPage.the_Line_Item_Quantity_is_updated_to(arg1);
	}

	@Then("^the contract should contain \"([^\"]*)\" open status field services$")
	public void the_contract_should_contain_open_status_field_services(String arg1) {
		fieldServiceContractPage.the_contract_should_contain_open_status_field_services(arg1);
	}

	@Then("^the contract should contain \"([^\"]*)\" assigned status field services$")
	public void the_contract_should_contain_assigned_status_field_services(String arg1) {
		fieldServiceContractPage.the_contract_should_contain_assigned_status_field_services(arg1);
	}

	@Then("^the contract should contain \"([^\"]*)\" delivered status field services$")
	public void the_contract_should_contain_delivered_status_field_services(String arg1) {
		fieldServiceContractPage.the_contract_should_contain_delivered_status_field_services(arg1);
	}

	@Then("^the contract should contain \"([^\"]*)\" cancelled/delivered status field services$")
	public void the_contract_should_contain_cancelled_delivered_status_field_services(String arg1) {
		fieldServiceContractPage.the_contract_should_contain_cancelled_delivered_status_field_services(arg1);
	}

	@When("^a contract is created$")
	public void a_contract_is_created() {
		fieldServiceContractPage.a_contract_is_created();
	}

	@When("^one of the services is assigned$")
	public void one_of_the_services_is_assigned() {
		fieldServiceContractPage.one_of_the_services_is_assigned();
	}

	@When("^the same service is delivered with the service date that is \"([^\"]*)\"$")
	public void the_same_service_is_delivered_with_the_service_date_that_is(String date) {
		fieldServiceContractPage.the_same_service_is_delivered_with_the_service_date_that_is(date);
	}

	@Then("^the Revenue Message field should be populated with todays date$")
	public void the_Revenue_Message_field_should_be_populated_with_todays_date() {
		fieldServiceContractPage.the_Revenue_Message_field_should_be_populated_with_todays_date();
	}

	@Then("^the contract should be created with product number which is not service product from otc$")
	public void the_contract_should_be_created_with_product_number_which_is_not_service_product_from_otc() {
		SCHUtils.waitforContractToSyncInSFDCFromOTC();
		fieldServiceContractPage.contractShouldBeCreatedWithProductNumberWhichIsNotServiceProductFromOTC();
	}

	@And("^the field services should be created with product number which is not service product from otc$")
	public void the_field_services_should_be_created_with_product_number_which_is_not_service_product_from_otc() {
		fieldServiceContractPage.fieldServicesShouldBeCreatedWithProductNumberWhichIsNotServiceProductFromOTC();
	}

	@Then("^the contract should be created with the inactive Product Number from otc$")
	public void the_contract_should_be_created_with_the_inactive_Product_Number_from_otc() {
		SCHUtils.waitforContractToSyncInSFDCFromOTC();
		fieldServiceContractPage.contractShouldBeCreatedWithInactiveProductNumberFromOTC();
	}

	@And("^the field services should be created with the inactive Product Number from otc$")
	public void the_field_services_should_be_created_with_the_inactive_Product_Number_from_otc() {
		fieldServiceContractPage.fieldServiceShouldbeCreatedWithInactiveProductNumberFromOTC();
	}

	@When("^the contract is \"([^\"]*)\" from OTC$")
	public void updateContractStatusFromOTC(String contractStatus) {
		fieldServiceContractPage.updateContractStatusFromOTC(contractStatus);
	}

	@When("^the Line Item Quantity is updated to \"([^\"]*)\" from OTC$")
	public void updateLineItemQuantityThroughOTC(String updatedValue) {
		fieldServiceContractPage.updateLineItemQuantityThroughOTC(updatedValue);
	}

}
