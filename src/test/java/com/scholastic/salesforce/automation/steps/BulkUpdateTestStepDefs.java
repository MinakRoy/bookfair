package com.scholastic.salesforce.automation.steps;

import com.scholastic.salesforce.automation.pages.BulkUpdateTestPage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BulkUpdateTestStepDefs {

	BulkUpdateTestPage bulkUpdateTestpage = new BulkUpdateTestPage();

	@When("^the scheduler assigns \"([^\"]*)\" field services to a consultant$")
	public void the_scheduler_assigns_field_services_to_a_consultant(String assignCount) {
		bulkUpdateTestpage.schedulerAssignFieldServicesToAConsultant(assignCount);
	}

	@Then("^the consultant should be book for the \"([^\"]*)\" assigned services$")
	public void the_consultant_should_be_book_for_the_assigned_services(String assignCount) {
		bulkUpdateTestpage.consultantShouldBeBookForAssignedServices(assignCount);
	}

	@When("^the scheduler delivers \"([^\"]*)\" field services for the consultant$")
	public void the_scheduler_delivers_field_services_for_the_consultant(String deliverCount) {
		bulkUpdateTestpage.schdulerDeliversFieldServicesForConsultant(deliverCount);
	}

	@Then("^the \"([^\"]*)\" field services should be marked as delivered$")
	public void the_field_services_should_be_marked_as_delivered(String deliverCount) {
		bulkUpdateTestpage.fieldServicesShouldBeMarkedAsDelivered(deliverCount);
	}

	@And("^the consultant should be book for the \"([^\"]*)\" assigned service bulk update$")
	public void the_consultant_should_be_book_for_the_assigned_service_bulk_update(String assignCount) {
		bulkUpdateTestpage.the_scheduler_assigns_field_services_to_a_consultant_bulk_update(assignCount);
	}

	@And("^the scheduler delivers \"([^\"]*)\" field services for the consultant bulk update$")
	public void the_scheduler_delivers_field_services_for_the_consultant_bulk_update(String deliverCount) {
		bulkUpdateTestpage.the_scheduler_delivers_field_services_for_the_consultant_bulk_update(deliverCount);
	}

	@And("^a consultant is already booked for a specific date$")
	public void a_consultant_is_already_booked_for_a_specific_date() {
		bulkUpdateTestpage.a_consultant_is_already_booked_for_a_specific_date();
	}

	@When("^the scheduler schedules the same consultant for another booking for the same date by bulk update$")
	public void the_scheduler_schedules_the_same_consultant_for_another_booking_for_the_same_date_by_bulk_update() {
		bulkUpdateTestpage
				.the_scheduler_schedules_the_same_consultant_for_another_booking_for_the_same_date_by_bulk_update();
	}

	@Then("^the scheduler should be prompted that the consultant is over booked$")
	public void the_scheduler_should_be_prompted_that_the_consultant_is_over_booked() {
		bulkUpdateTestpage.the_scheduler_should_be_prompted_that_the_consultant_is_over_booked();
	}

	@Then("^the scheduler should be able to bypass the prompt$")
	public void the_scheduler_should_be_able_to_bypass_the_prompt() {
		bulkUpdateTestpage.the_scheduler_should_be_able_to_bypass_the_prompt();
	}

	@Then("^the schedule should be able to book the service to the consultant$")
	public void the_schedule_should_be_able_to_book_the_service_to_the_consultant() {
		bulkUpdateTestpage.the_schedule_should_be_able_to_book_the_service_to_the_consultant();
	}

	@When("^the scheduler schedules fields services from different contracts to consultants$")
	public void the_scheduler_schedules_fields_services_from_different_contracts_to_consultants() {
		bulkUpdateTestpage.the_scheduler_schedules_fields_services_from_different_contracts_to_consultants();
	}

	@Then("^the consultant should be booked for the assigned services$")
	public void the_consultant_should_be_booked_for_the_assigned_services() {
		bulkUpdateTestpage.the_consultant_should_be_booked_for_the_assigned_services();
	}

	@When("^the scheduler schedules multiple field services without all requirements$")
	public void the_scheduler_schedules_multiple_field_services_without_all_requirements() {
		bulkUpdateTestpage.the_scheduler_schedules_multiple_field_services_without_all_requirements();
	}

	@Then("^the individual error should be viewable by the user$")
	public void the_individual_error_should_be_viewable_by_the_user() {
		bulkUpdateTestpage.the_individual_error_should_be_viewable_by_the_user();
	}

	@When("^the user navigates to a service that has a \"([^\"]*)\" status$")
	public void the_user_navigates_to_a_service_that_has_a_status(String serviceStatus) {
		bulkUpdateTestpage.the_user_navigates_to_a_service_that_has_a_status(serviceStatus);
	}

	@Then("^the user should not be able to edit the \"([^\"]*)\" field on the bulk update page$")
	public void the_user_should_not_be_able_to_edit_the_field_on_the_bulk_update_page(String ServiceStatus) {
		bulkUpdateTestpage.the_user_should_not_be_able_to_edit_the_field_on_the_bulk_update_page(ServiceStatus);
	}

	@When("^the service date is entered for a field service in bulk update$")
	public void the_service_date_is_entered_for_a_field_service_in_bulk_update() {
		bulkUpdateTestpage.the_service_date_is_entered_for_a_field_service_in_bulk_update();
	}

	@Then("^the start dates should be 9 AM$")
	public void the_start_dates_should_be_AM() {
		bulkUpdateTestpage.the_start_dates_should_be_AM();
	}

	@And("^the end date should be 5 PM$")
	public void the_end_date_should_be_PM() {
		bulkUpdateTestpage.the_end_date_should_be_PM();
	}

}
