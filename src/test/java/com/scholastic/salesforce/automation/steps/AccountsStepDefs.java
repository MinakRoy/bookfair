package com.scholastic.salesforce.automation.steps;

import com.scholastic.salesforce.automation.pages.AccountsPage;
import com.scholastic.salesforce.automation.pages.ReportPage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AccountsStepDefs {

	AccountsPage accountsPage = new AccountsPage();

	@When("^a client is created with the previously invalid Install to UCN$")
	public void a_client_is_created_with_the_previously_invalid_Install_to_UCN() {
		accountsPage.clientIsCreatedWithThePreviouslyInvalidInstallToUCN();
	}

	@When("^the attempts to create a \"([^\"]*)\"$")
	public void the_attempts_to_create(String type) {
		accountsPage.attemptsToCreateAccount(type);
	}

	@Then("^they should be \"([^\"]*)\" unsuccessful$")
	public void they_should_be_unsuccessful(String type) {
		accountsPage.accountShouldNotBeCreated(type);
	}

	@Then("^the new account should be created in SFDC$")
	public void the_new_account_should_be_created_in_SFDC() {
		accountsPage.the_new_account_should_be_created_in_SFDC("CHILD");
	}

	@Then("^the new account should be created in SFDC with all address$")
	public void the_new_account_should_be_created_in_SFDC_with_all_address() {
		accountsPage.the_new_account_should_be_created_in_SFDC("ADDRESS");
	}

	@Then("^the new parent account should be created in SFDC$")
	public void the_new_parent_account_should_be_created_in_SFDC() {
		accountsPage.the_new_account_should_be_created_in_SFDC("CHILD");
	}

	@And("^a Account Relationship is created with the account populated in the Related Child Institution field$")
	public void a_Account_Relationship_is_created_with_the_account_populated_in_the_Related_Child_Institution_field() {
		accountsPage
				.a_Account_Relationship_is_created_with_the_account_populated_in_the_Related_Child_Institution_field();
	}

	@And("^the Parent Account is populated in the Parent Institution field$")
	public void the_Parent_Account_is_populated_in_the_Parent_Institution_field() {
		accountsPage.the_Parent_Account_is_populated_in_the_Parent_Institution_field();
	}

	@And("^the creation date is in the Start Date$")
	public void the_creation_date_is_in_the_Start_Date() {
		accountsPage.the_creation_date_is_in_the_Start_Date();
	}

	@And("^the End date is left blank$")
	public void the_End_date_is_left_blank() {
		accountsPage.the_End_date_is_left_blank();
	}

	@And("^no Account Relationship is created$")
	public void no_Account_Relationship_is_created() {
		accountsPage.no_Account_Relationship_is_created();
	}

	@And("^the user is on a page with account fields$")
	public void the_user_is_on_a_page_with_account_fields() {
		accountsPage.the_user_is_on_a_page_with_account_fields();
	}

	@When("^the user attempts to edit any account field$")
	public void the_user_attempts_to_edit_any_account_field() {
		accountsPage.the_user_attempts_to_edit_any_account_field();
	}

	@Then("^they should not be able to make the update$")
	public void they_should_not_be_able_to_make_the_update() {
		accountsPage.they_should_not_be_able_to_make_the_update();
	}

	@Then("^they should not be able to edit account address field$")
	public void they_should_not_be_able_to_make_the_edit_account_address_field() {
		accountsPage.shouldNotBeAbleToMakeTheEditAccountAddresField();
	}

	@Then("^they should not be able to edit account relationship field$")
	public void they_should_not_be_able_to_edit_account_relationship_field() {
		accountsPage.shouldNotBeAbleToEditAccountRelationShipField();
	}

	@When("^the user attempts to edit the \"([^\"]*)\" field$")
	public void the_user_attempts_to_edit_the_field(String arg1) {
		accountsPage.the_user_attempts_to_edit_the_field(arg1);
	}

	@Then("^they should be able to make the update \"([^\"]*)\" field$")
	public void they_should_be_able_to_make_the_update(String arg1) {
		accountsPage.they_should_be_able_to_make_the_update(arg1);
	}

	@When("^the user attempts to edit any account address field$")
	public void the_user_attempts_to_edit_any_account_address_field() {
		accountsPage.the_user_attempts_to_edit_any_account_address_field();
	}

	@When("^the user attempts to edit any account relationship field$")
	public void the_user_attempts_to_edit_any_account_relationship_field() {
		accountsPage.userAttemptsToEditAnyAccountRelationshipField();
	}

	@And("^the mailing address should also populate the mailing address in the top bar$")
	public void the_mailing_address_should_also_populate_the_mailing_address_in_the_top_bar() {
		accountsPage.the_mailing_address_should_also_populate_the_mailing_address_in_the_top_bar();
	}

	@And("^the mailing county should be populated based on the mailing county code$")
	public void the_mailing_county_should_be_populated_based_on_the_mailing_county_code() {
		accountsPage.mailingCountyShouldBePopulatedBasedOnMailingCountyCode();
	}

	@Then("^the account in the Related Child institution field$")
	public void the_account_in_the_Related_Child_institution_field() {
		accountsPage.accountInRelatedChildInstitutionField();
	}

	@Then("^the Opportunities should be moved to the receiving account$")
	public void the_Opportunities_should_be_moved_to_the_receiving_account() {
		accountsPage.oportunityShouldMovedToReceivingAccount();
	}

	@And("^the receiving account should have \"([^\"]*)\" number of Opportunities$")
	public void the_receiving_account_should_have_number_of_Opportunities(String arg1) {
		accountsPage.ReceivingAccountShouldHaveNumberOfOpportunities(arg1);
	}

	@Then("^the Tasks should be moved to the receiving account$")
	public void the_Tasks_should_be_moved_to_the_receiving_account() {
		accountsPage.taskShouldMovedToReceivingAccount();
	}

	@And("^the receiving account should have \"([^\"]*)\" number of Tasks$")
	public void the_receiving_account_should_have_number_of_Tasks(String arg1) {
		accountsPage.receivingAccountShouldHaveNumberOfTaskExists(arg1);
	}

	@Then("^the Contacts should be moved to the receiving account$")
	public void the_Contacts_should_be_moved_to_the_receiving_account() {
		accountsPage.contactsShouldMovedToReceivingAccount();
	}

	@And("^the receiving account should have \"([^\"]*)\" number of Contacts$")
	public void the_receiving_account_should_have_number_of_Contacts(String arg1) {
		accountsPage.receivingAccountShouldHaveNumberOfContactsExists(arg1);
	}

	@Then("^the Events should be moved to the receiving account$")
	public void the_Events_should_be_moved_to_the_receiving_account() {
		accountsPage.eventsShouldMovedToReceivingAccount();
	}

	@And("^the receiving account should have \"([^\"]*)\" number of Events$")
	public void the_receiving_account_should_have_number_of_Events(String arg1) {
		accountsPage.receivingAccountShouldHaveNumberOfEventsExists(arg1);
	}

	@When("^the \"([^\"]*)\" record is searched in Salesforce$")
	public void the_record_is_searched_in_Salesforce(String recordNumber) {
		accountsPage.the_record_is_searched_in_Salesforce(recordNumber);
	}

	@Then("^the values of the \"([^\"]*)\" record should match the \"([^\"]*)\" record returned in the search results$")
	public void the_values_of_the_record_should_match_the_record_returned_in_the_search_results(String recordNumber,
			String type) {
		accountsPage.the_values_of_the_record_should_match_the_record_returned_in_the_search_results(recordNumber,
				type);
	}

	@When("^the \"([^\"]*)\" district record is searched in Salesforce$")
	public void the_district_record_is_searched_in_salesforce(String recordNumber) {
		accountsPage.the_district_record_is_searched_in_salesforce(recordNumber);
	}

	@When("^the \"([^\"]*)\" territory record is searched in Salesforce$")
	public void the_territory_record_is_searched_in_salesforce(String recordNumber) {
		HomePageStepDefs homePageStep = new HomePageStepDefs();
		homePageStep.userLoginInToSystem();
		ReportPage reportPage = new ReportPage();
		reportPage.searchForTerritoryRecordInSalesforce(recordNumber);
	}
}
