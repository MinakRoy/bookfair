package com.scholastic.salesforce.automation.steps;

import com.scholastic.salesforce.automation.pages.TMSCRMHomePage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class TMSCRMHomePageStepDefs {

	TMSCRMHomePage tmsCRMHomePage = new TMSCRMHomePage();

	@Given("^a new account details are sent from CDB$")
	public void a_new_account_details_are_sent_from_CDB() {
		tmsCRMHomePage.a_new_account_details_are_sent_from_CDB();
	}

	@Given("^a new account is sent from CDB \"([^\"]*)\" a Parent Account$")
	public void a_new_account_is_sent_from_CDB_With_or_Without_a_Parent_Account(String parentStatus) {
		tmsCRMHomePage.a_new_account_is_sent_from_CDB_With_or_Without_a_Parent_Account(parentStatus);
	}

	@Given("^two valid accounts exist$")
	public void two_valid_accounts_exist() {
		tmsCRMHomePage.twoValidAccountExist();
	}

	@Given("^a new account is sent from CDB with all addresses$")
	public void a_new_account_is_sent_from_CDB_with_all_addresses() {
		tmsCRMHomePage.newAccountIsSentFromCDBWithAllAddresses();
	}

	@Given("^a new account is sent from CDB with only one address$")
	public void a_new_account_is_sent_from_CDB_with_only_one_address() {
		tmsCRMHomePage.newAccountSentFromCDBWithOnlyOneAddress();
	}

	@Given("^an account exist with Parent Account field populated in the Account Relationship$")
	public void an_account_exist_with_Parent_Account_field_populated_in_the_Account_Relationship() {
		tmsCRMHomePage.accountExistWithParentAccountFieldPopulatedInAccountRelationship();
	}

	@When("^the Parent Account is modified$")
	public void the_Parent_Account_is_modified() {
		tmsCRMHomePage.parentAccountIsModified();
	}

	@Given("^valid accounts exist with a Account listed in the Related Child Institution field$")
	public void valid_accounts_exist_with_a_Account_listed_in_the_Related_Child_Institution_field() throws Throwable {
		tmsCRMHomePage.validAccountExistWithParentAccount();
	}

	@And("^an valid account exists to receive a merge$")
	public void an_valid_account_exists_to_receive_a_merge() throws Throwable {
		tmsCRMHomePage.validAccountExistToReceiveMerge();
	}

	@Given("^that an account exists in SalesForce$")
	public void that_an_account_exists_in_SalesForce() {
		tmsCRMHomePage.anAccountExistsInSalesforce();
	}

	@When("^merge source account with target account$")
	public void mergeSourceAccountWithTargetAccount() {
		tmsCRMHomePage.mergetSourceAccountWithTargetAccount();
	}

	@When("^update enterprise status on account as closed$")
	public void update_enterprise_status_on_account_as_closed() {
		tmsCRMHomePage.updateEnterpriseStatusAsClosed();
	}
}
