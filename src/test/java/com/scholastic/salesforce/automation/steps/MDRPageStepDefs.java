package com.scholastic.salesforce.automation.steps;

import com.scholastic.salesforce.automation.pages.MDRPage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MDRPageStepDefs {

	MDRPage mdrPage = new MDRPage();

	@When("^the MDR Monthly Account file is processed$")
	public void the_MDR_Monthly_Account_file_is_processed() {
		mdrPage.monthlyAccountFileIsProcessed();
	}

	@And("^the file contains an entry that already exists in Salesforce$")
	public void the_file_contains_an_entry_that_already_exists_in_Salesforce() {
		mdrPage.fileContainsAnEntryThatAlreadyExissInSalesforce();
	}

	@When("^the Monthly Account file is processed$")
	public void the_Monthly_Account_file_is_processed() {
		mdrPage.existingMonthlyAccountFileIsProcessed();
	}

	@Given("^a MDR Monthly Account file is received that contains more records than the batch size$")
	public void a_MDR_Monthly_Account_file_is_received_that_contains_more_records_than_the_batch_size() {
		mdrPage.a_MDR_Monthly_Account_file_is_received_that_contains_more_records_than_the_batch_size();
	}

	@When("the MDR Monthly Role file is processed")
	public void the_MDR_Monthly_Role_file_is_processed() {
		mdrPage.theMonthlyRoleFileIsProcessed();
	}

	@Given("^a MDR Monthly Account error is logged in the separate file$")
	public void a_MDR_Monthly_Account_error_is_logged_in_the_separate_file() {
		mdrPage.monthlyAccountErrorIsLoggedInSeparateFile();
	}

	@When("^the Daily error Account file is processed$")
	public void the_Daily_error_Account_file_is_processed() {
		mdrPage.theDailyErrorAccountFileIsProcessed();
	}

	@Given("^a MDR Monthly Account error file exist with failed error entries$")
	public void a_MDR_Monthly_Account_error_file_exist_with_failed_error_entries() {
		mdrPage.a_MDR_Monthly_Account_error_file_exist_with_failed_error_entries();
	}

	@When("^a new MDR Monthly Account file is processed$")
	public void a_new_MDR_Monthly_Account_file_is_processed() {
		mdrPage.a_new_MDR_Monthly_Account_file_is_processed();
	}

	@And("^it contains an error$")
	public void it_contains_an_error() {
		mdrPage.it_contains_an_error();
	}

	@Then("^the new error should be the only error in the error file$")
	public void the_new_error_should_be_the_only_error_in_the_error_file() {
		mdrPage.the_new_error_should_be_the_only_error_in_the_error_file();
	}

	@Given("^a MDR Monthly Account File contains all the fields$")
	public void a_MDR_Monthly_Account_File_is_contains_all_the_fields() {
		mdrPage.a_MDR_Monthly_Account_File_is_contains_all_the_fields();
	}

	@When("^the MDR Monthly Account file is processed with all the fields$")
	public void the_MDR_Monthly_Account_file_is_processed_with_all_thes_fields() {
		mdrPage.the_MDR_Monthly_Account_file_is_processed_with_all_thes_fields();
	}
}
