package com.scholastic.salesforce.automation.steps;

import com.scholastic.salesforce.automation.support.DataToS3;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DataToS3SstepDefs {

	DataToS3 dataToS3 = new DataToS3();

	@Given("^BI Data is in SFDC$")
	public void bi_Data_is_in_SFDC() {
		System.out.println("Not Doing Any Action in SFDC, we are pre-assuming data is exported.");
	}

	@When("^the SFDC \"([^\"]*)\" Data is loaded for BI$")
	public void the_SFDC_Data_is_loaded_for_BI(String arg1) {
		System.out.println("Not Doing Any Action in SFDC, we are pre-assuming data is exported.");
	}

	@When("^the SFDC Data is set to S(\\d+) by JustTranform$")
	public void the_SFDC_Data_is_set_to_S_by_JustTranform(int arg1) {
		System.out.println("Not Doing Any Action in SFDC, we are pre-assuming data is exported.");
	}

	@When("^the SFDC \"([^\"]*)\" Data is set to S(\\d+) by JustTranform$")
	public void the_SFDC_Data_is_set_to_S_by_JustTranform(String arg1, int arg2) {
		System.out.println("Not Doing Any Action in SFDC, we are pre-assuming data is exported.");
	}

	@Then("^the \"([^\"]*)\" files should saved in the \"([^\"]*)\"$")
	public void the_files_should_saved_in_the(String type, String location) {
		dataToS3.the_files_should_saved_in_the(type, location);
	}

	@Then("^the files saved should be CSV files$")
	public void the_files_saved_should_be_CSV_files() {
		dataToS3.the_files_saved_should_be_CSV_files();
	}

	@Then("^the \"([^\"]*)\" files should have a \"([^\"]*)\" naming convention$")
	public void the_files_should_have_a_naming_convention(String type, String format) {
		dataToS3.the_files_should_have_a_naming_convention(type, format);
	}

	@Given("^Download latest \"([^\"]*)\" file from amazon server$")
	public void download_latest_file_from_S_Server(String type) {
		dataToS3.download_latest_file_from_S_Server(type);
	}

	@Then("^the \"([^\"]*)\" files should contain all fields for that type$")
	public void the_files_should_contain_all_fields_for_that_type(String type) {
		dataToS3.the_files_should_contain_all_fields_for_that_type(type);
	}

}
