package com.scholastic.salesforce.automation.steps;

import com.scholastic.salesforce.automation.pages.ProductsPage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class ProductsStepDefs {

	ProductsPage productsPage = new ProductsPage();

	@And("^a Product is created with the previously invalid Product Number$")
	public void a_Product_is_created_with_the_previously_invalid_Product_Number() {
		productsPage.productIsCreatedWithTheInvalidProductNumber();
	}

	@When("^the inactive Product Number is reactivated$")
	public void the_inactive_Product_Number_is_reactivated() {
		productsPage.reactiveInactiveProductNumber();
	}

}
