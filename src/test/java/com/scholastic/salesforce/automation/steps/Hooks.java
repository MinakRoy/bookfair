package com.scholastic.salesforce.automation.steps;

import static com.scholastic.torque.common.TestBaseProvider.getTestBase;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.saucelabs.saucerest.SauceREST;
import com.scholastic.salesforce.automation.pages.FieldServiceContractPage;
import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

	@Before
	public void beforeHook(Scenario scenario) {
		synchronized (this) {
			TestBase testBase = TestBaseProvider.getTestBase();
			testBase.getContext().clearProperty("tdm.UserProfile");
			testBase.getContext().subset("testexecution").clear();
			System.setProperty("webdriver.firefox.marionette", "servers/geckodriver.exe");
			String session = testBase.getSessionID();
			String[] nameArray = scenario.getName().split(":\\[");
			String testcaseid = null;
			if (nameArray.length == 2) {
				testcaseid = nameArray[1].replace("]", "");
				TestBaseProvider.getTestBase().getContext().setProperty("testcaseid", testcaseid.trim());
				TestBaseProvider.getTestBase().setTestDataFromXml(testcaseid);
			} else {
				System.out.println("Test data key was not passed");
			}
			if (!session.equalsIgnoreCase("") && !testBase.getContext().getString("sauce").equalsIgnoreCase("false")) {
				SauceREST sClient = new SauceREST(testBase.getContext().getString("sauce.username"),
						testBase.getContext().getString("sauce.access.key"));
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("name", scenario.getName());
				sClient.updateJobInfo(session, params);
			}
		}
		getTestBase().getDriver().manage().deleteAllCookies();
		if (!(TestBaseProvider.getTestBase().getContext().getString("driver.name").equalsIgnoreCase("Android")
				|| TestBaseProvider.getTestBase().getContext().getString("driver.name").equalsIgnoreCase("IOs")
				|| TestBaseProvider.getTestBase().getContext().getString("driver.name").equalsIgnoreCase("Remote"))) {
			TestBaseProvider.getTestBase().getDriver().manage().window().maximize();
		}
	}

	@After
	public void afterHook(Scenario scenario) {
		synchronized (this) {
			FieldServiceContractPage.workOrderNumber = 0;
			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			if (scenario.isFailed()) {
				try {
					scenario.write("Current Page URL is " + driver.getCurrentUrl());
					byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
					scenario.embed(screenshot, "image/png");

				} catch (WebDriverException somePlatformsDontSupportScreenshots) {
					System.err.println(somePlatformsDontSupportScreenshots.getMessage());
				}

			}
			String session = TestBaseProvider.getTestBase().getSessionID();
			if (!scenario.isFailed())
				System.out.println(
						"ScenarioFailed=" + scenario.getName() + "<>Session=" + session + "<>Status=Passed<>Platform="
								+ TestBaseProvider.getTestBase().getContext().getString("driver.name"));
			else
				System.out.println(
						"ScenarioFailed=" + scenario.getName() + "<>Session=" + session + "<>Status=Failed<>Platform="
								+ TestBaseProvider.getTestBase().getContext().getString("driver.name"));
			System.out.println("SauceOnDemandSessionID=" + session + " job-name=" + scenario.getName());
			if (!session.equalsIgnoreCase("")
					&& !TestBaseProvider.getTestBase().getContext().getString("sauce").equalsIgnoreCase("false")) {
				TestBase testBase = TestBaseProvider.getTestBase();
				SauceREST sClient = new SauceREST(testBase.getContext().getString("sauce.username"),
						testBase.getContext().getString("sauce.access.key"));
				System.out.println("SessionID::" + session);
				if (scenario.isFailed())
					sClient.jobFailed(session);
				else
					sClient.jobPassed(session);
			}
			TestBaseProvider.getTestBase().tearDown();
		}
	}
}