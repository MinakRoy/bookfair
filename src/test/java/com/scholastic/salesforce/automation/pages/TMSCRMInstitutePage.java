package com.scholastic.salesforce.automation.pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.scholastic.salesforce.automation.beans.InstituteBean;
import com.scholastic.salesforce.automation.steps.HomePageStepDefs;
import com.scholastic.salesforce.automation.support.ConstantUtils;
import com.scholastic.salesforce.automation.support.SCHUtils;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

public class TMSCRMInstitutePage extends BaseTestPage<TMSCRMHomePage> {

	@FindBy(locator = "TMS_LINK_CREATE_NEW_INSTITUTE")
	private WebElement linkCreateNewInstitute;
	@FindBy(locator = "TMS_TXT_INSTITUTE_NAME")
	private WebElement txtInstituteName;
	@FindBy(locator = "TMS_TXT_POSTAL")
	private WebElement txtPostal;
	@FindBy(locator = "TMS_TXT_ZIP")
	private WebElement txtZip;
	@FindBy(locator = "TMS_TXT_ADDRESS_ONE")
	private WebElement txtAddressOne;
	@FindBy(locator = "TMS_TXT_ENROLLMENT")
	private WebElement txtEnrollment;
	@FindBy(locator = "TMS_TXT_PHONE")
	private WebElement txtPhone;
	@FindBy(locator = "TMS_TXT_CUSTOMER_GROUP")
	private WebElement txtCustomerGroup;
	@FindBy(locator = "TMS_TXT_CUSTOMER_TYPE")
	private WebElement txtCustomerType;
	@FindBy(locator = "TMS_LINK_ADDRESS")
	private WebElement linkAddress;
	@FindBy(locator = "TMS_LINK_INS_SUBMIT")
	private WebElement linkInsSubmit;
	@FindBy(locator = "TMS_LINK_CONFIRM_SAVE")
	private WebElement linkConfirmSave;
	@FindBy(locator = "TMS_LABLE_UCN")
	private WebElement lableUCN;
	@FindBy(locator = "TMS_INPUT_UCN_SEARCH")
	private WebElement inputUCNSearch;
	@FindBy(locator = "TMS_BUTTON_LOOK_UP")
	private WebElement buttonLookUp;
	@FindBy(locator = "TMS_LINK_SR_ACCOUNT_ID")
	private List<WebElement> linkSrAccountID;
	@FindBy(locator = "TMS_LINK_SEARCH_PARENT")
	private WebElement linkSearchParent;
	@FindBy(locator = "TMS_LINK_ADDITIONAL_INFO")
	private WebElement linkAdditionalInfo;
	@FindBy(locator = "TMS_LINK_ACCOUNT_ACTION")
	private WebElement linkAccountAction;
	@FindBy(locator = "TMS_LINK_OPTION_MERGE_ACCOUNT_ACTION")
	private WebElement linkOptionMergeAccountAction;
	@FindBy(locator = "TMS_LINK_OPTION_CLOSE_ACCOUNT_ACTION")
	private WebElement linkOptionCloseAccountAction;
	@FindBy(locator = "TMS_INPUT_MERGE_TO_UCN")
	private WebElement inputMergeToUCN;
	@FindBy(locator = "TMS_BUTTON_SEARCH_UCN")
	private WebElement buttonSearchUCN;
	@FindBy(locator = "TMS_BUTTON_REFRESH")
	private WebElement buttonRefresh;
	@FindBy(locator = "TMS_LABEL_INSTITUTE_STATUS")
	private WebElement labelInstituteStatus;
	@FindBy(locator = "TMS_LABEL_ENTERPRISE_STATUS")
	private WebElement labelEnterpriseStatus;
	@FindBy(locator = "TMS_INPUT_LOC_ADD_POSTAL")
	private WebElement inputLocAddPostal;
	@FindBy(locator = "TMS_INPUT_LOC_ADD_ADD1")
	private WebElement inputLocAddAdd1;
	@FindBy(locator = "TMS_INPUT_LOC_ADD_ZIP4")
	private WebElement inputLocAddZip4;
	@FindBy(locator = "TMS_INPUT_INSTITUTE_ID")
	private WebElement inputInstituteID;
	@FindBy(locator = "TMS_BUTTON_SEARCH_INSTITUTE")
	private WebElement buttonSearchInstitute;
	@FindBy(locator = "TMS_LINK_INSTITUTE_NAME")
	private WebElement linkInstituteName;
	@FindBy(locator = "TMS_LINK_YES_END_RELATIONSHIP")
	private WebElement linkYesEndRelationShip;
	@FindBy(locator = "TMS_LINK_MANAGE_RELATIONSHIPS")
	private WebElement linkManageRelationships;
	@FindBy(locator = "TMS_BUTTON_END_RELATIONSHIP")
	private WebElement buttonEndRelationship;
	@FindBy(locator = "TMS_OPTION_STATUS")
	private WebElement optionStatus;
	@FindBy(locator = "TMS_BUTTON_REQUEST_NEW_MEMBERSHIP")
	private WebElement buttonRequestNewMembership;
	@FindBy(locator = "TMS_INPUT_RELATED_CHILD")
	private WebElement inputRelatedChild;
	@FindBy(locator = "TMS_BUTTON_SEARCH_RELATED_CHILD")
	private WebElement buttonSearchRelatedChild;
	@FindBy(locator = "TMS_INPUT_SEARCH_UCN")
	private WebElement inputSearchUCN;
	@FindBy(locator = "TMS_LINK_SEARCH_ACCOUNT_ID")
	private WebElement linkSearchAccountID;
	@FindBy(locator = "TMS_OPTION_APPROVE")
	private WebElement optionApprove;
	@FindBy(locator = "TMS_LINK_ACCOUNT_CLOSE_REASON")
	private WebElement linkAccountCloseReason;

	public WebElement getLinkAccountCloseReason() {
		return linkAccountCloseReason;
	}

	public WebElement getOptionApprove() {
		return optionApprove;
	}

	public WebElement getLinkSearchAccountID() {
		return linkSearchAccountID;
	}

	public WebElement getInputSearchUCN() {
		return inputSearchUCN;
	}

	public WebElement getInputRelatedChild() {
		return inputRelatedChild;
	}

	public WebElement getButtonSearchRelatedChild() {
		return buttonSearchRelatedChild;
	}

	public WebElement getButtonRequestNewMembership() {
		return buttonRequestNewMembership;
	}

	public WebElement getOptionStatus() {
		return optionStatus;
	}

	public WebElement getLinkManageRelationships() {
		return linkManageRelationships;
	}

	public WebElement getButtonEndRelationship() {
		return buttonEndRelationship;
	}

	public WebElement getInputInstituteID() {
		return inputInstituteID;
	}

	public WebElement getButtonSearchInstitute() {
		return buttonSearchInstitute;
	}

	public WebElement getLinkInstituteName() {
		return linkInstituteName;
	}

	public WebElement getLinkYesEndRelationShip() {
		return linkYesEndRelationShip;
	}

	public WebElement getInputLocAddPostal() {
		return inputLocAddPostal;
	}

	public WebElement getInputLocAddAdd1() {
		return inputLocAddAdd1;
	}

	public WebElement getInputLocAddZip4() {
		return inputLocAddZip4;
	}

	public WebElement getLabelEnterpriseStatus() {
		return labelEnterpriseStatus;
	}

	public WebElement getLabelInstituteStatus() {
		return labelInstituteStatus;
	}

	public WebElement getLinkAccountAction() {
		return linkAccountAction;
	}

	public WebElement getLinkOptionMergeAccountAction() {
		return linkOptionMergeAccountAction;
	}

	public WebElement getLinkOptionCloseAccountAction() {
		return linkOptionCloseAccountAction;
	}

	public WebElement getInputMergeToUCN() {
		return inputMergeToUCN;
	}

	public WebElement getButtonSearchUCN() {
		return buttonSearchUCN;
	}

	public WebElement getButtonRefresh() {
		return buttonRefresh;
	}

	public WebElement getLinkAdditionalInfo() {
		return linkAdditionalInfo;
	}

	public WebElement getLinkSearchParent() {
		return linkSearchParent;
	}

	public WebElement getInputUCNSearch() {
		return inputUCNSearch;
	}

	public WebElement getButtonLookUp() {
		return buttonLookUp;
	}

	public List<WebElement> getLinkSrAccountID() {
		return linkSrAccountID;
	}

	public WebElement getLableUCN() {
		return lableUCN;
	}

	public WebElement getLinkConfirmSave() {
		return linkConfirmSave;
	}

	public WebElement getLinkInsSubmit() {
		return linkInsSubmit;
	}

	public WebElement getLinkCreateNewInstitute() {
		return linkCreateNewInstitute;
	}

	public WebElement getTxtInstituteName() {
		return txtInstituteName;
	}

	public WebElement getTxtPostal() {
		return txtPostal;
	}

	public WebElement getTxtZip() {
		return txtZip;
	}

	public WebElement getTxtAddressOne() {
		return txtAddressOne;
	}

	public WebElement getTxtEnrollment() {
		return txtEnrollment;
	}

	public WebElement getTxtPhone() {
		return txtPhone;
	}

	public WebElement getTxtCustomerGroup() {
		return txtCustomerGroup;
	}

	public WebElement getTxtCustomerType() {
		return txtCustomerType;
	}

	public WebElement getLinkAddress() {
		return linkAddress;
	}

	@Override
	protected void openPage() {
	}

	public void createInstitute(String customerGroup, String customerType, String instituteType) {
		InstituteBean insBean = InstituteBean.getInstituteDataBean();
		WaitUtils.waitForDisplayed(getTxtInstituteName());
		getTxtInstituteName().sendKeys(insBean.getInstituteName());
		getTxtPostal().sendKeys(insBean.getPostalCode());
		getTxtPostal().sendKeys(Keys.TAB);
		SCHUtils.waitForAjaxToComplete();
		getTxtZip().sendKeys(insBean.getZipCode());
		getTxtAddressOne().sendKeys(insBean.getAddressOne());
		getTxtPhone().sendKeys(insBean.getPhoneNumber());
		getTxtEnrollment().sendKeys(insBean.getEnrollment());
		if (customerGroup.equals("default")) {
			insBean.setCustomerGroup(ConstantUtils.INSTITUTE_CUSTOMER_GROUP);
		} else {
			insBean.setCustomerGroup(customerGroup);
		}
		if (customerType.equals("default")) {
			insBean.setCustomerType(ConstantUtils.INSTITUTE_CUSTOMER_TYPE);
		} else {
			insBean.setCustomerType(customerType);
		}
		getTxtCustomerGroup().sendKeys(insBean.getCustomerGroup());
		SCHUtils.waitForAjaxToComplete();
		getTxtCustomerType().sendKeys(insBean.getCustomerType());
		getLinkInsSubmit().click();
		WaitUtils.waitForDisplayed(getLinkConfirmSave());
		getLinkConfirmSave().click();
		WaitUtils.waitForDisplayed(getLableUCN());
		System.out.println(instituteType + " contract created at " + printCurrentDateTime());
		System.out.println(instituteType + " Created UCN Number is " + getLableUCN().getText());
		TestBaseProvider.getTestBase().getContext().setProperty("CREATED_INSTITUTE_UCN_" + instituteType.toUpperCase(),
				getLableUCN().getText());
		TestBaseProvider.getTestBase().getContext()
				.setProperty("testexecution.INSTITUTEDETAILS_" + instituteType.toUpperCase(), insBean);
	}

	public static String printCurrentDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public void clickOnCreateNewInstitute() {
		WebElement frameElement = getDriver().findElement(By.name("TargetContent"));
		getDriver().switchTo().frame(frameElement);
		WaitUtils.waitForDisplayed(getLinkCreateNewInstitute());
		SCHUtils.clickUsingJavaScript(getLinkCreateNewInstitute());
	}

	public void createInstituteWithParent(String string, String string2, String instituteType) {
		addParentToCreateInstitute();
		createInstitute("default", "default", instituteType);
	}

	public void addParentToCreateInstitute() {
		WaitUtils.waitForDisplayed(getLinkSearchParent());
		SCHUtils.clickUsingJavaScript(getLinkSearchParent());
		WaitUtils.waitForDisplayed(getInputUCNSearch());
		String UCNNumber = TestBaseProvider.getTestBase().getString("CREATED_INSTITUTE_UCN_CHILD");
		getInputUCNSearch().sendKeys(UCNNumber);
		SCHUtils.clickUsingJavaScript(getButtonLookUp());
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.waitForLoaderToDismiss();
		int size = getLinkSrAccountID().size();
		if (size < 0) {
			Assert.fail("No Account Found for UCN Number " + UCNNumber);
		} else {
			SCHUtils.clickUsingJavaScript(getLinkSrAccountID().get(0));
			SCHUtils.waitForAjaxToComplete();
		}
	}

	public void mergeToUCNInOneAccounts() {
		WaitUtils.waitForDisplayed(getLinkAdditionalInfo());
		SCHUtils.clickUsingJavaScript(getLinkAdditionalInfo());
		WaitUtils.waitForDisplayed(getLinkAccountAction());
		getLinkAccountAction().sendKeys(ConstantUtils.ACCOUNT_MERGE);
		WaitUtils.waitForDisplayed(getInputMergeToUCN());
		String firstUCN = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_FIRST");
		getInputMergeToUCN().sendKeys(firstUCN);
		SCHUtils.clickUsingJavaScript(getButtonSearchUCN());
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.waitForLoaderToDismiss();
		int size = getLinkSrAccountID().size();
		if (size < 0) {
			Assert.fail("No Account Found for UCN Number " + firstUCN);
		} else {
			SCHUtils.clickUsingJavaScript(getLinkSrAccountID().get(0));
			SCHUtils.waitForAjaxToComplete();
		}
		SCHUtils.clickUsingJavaScript(getLinkInsSubmit());
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getButtonRefresh());
		SCHUtils.clickUsingJavaScript(getButtonRefresh());
	}

	public void verifyAccountStatusUpdatedToTerminated() {
		WaitUtils.waitForDisplayed(getLabelEnterpriseStatus());
		System.out.println("terminated Status :: " + getLabelEnterpriseStatus().getText());
		AssertUtils.assertTextMatches(getLabelEnterpriseStatus(),
				Matchers.containsString(ConstantUtils.ENTERPRISE_STATUS_TERMINATED));

		// navigate to SFDC search for terminated account and verify
		HomePageStepDefs homeSteps = new HomePageStepDefs();
		homeSteps.userLoginInToSystem();
		HomePage homePage = new HomePage();
		homePage.navigateToMenu("Accounts");
		AccountsPage accountPage = new AccountsPage();
		String ucnNumber = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_SECOND");
		System.out.println("UCN :: " + ucnNumber);
		accountPage.searchForAccount(ucnNumber);
		InstituteBean instituteBean = (InstituteBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.INSTITUTEDETAILS_SECOND");
		accountPage.navigateToCreatedAccount(instituteBean);
		WaitUtils.waitForDisplayed(accountPage.getLinkDetails());
		accountPage.getLinkDetails().click();
		SCHUtils.waitForAjaxToComplete();
		accountPage.verifyaccountStatusUpdatedToTerminated();
	}

	public void fillInstituteDetail(String customerGroup, String customerType, String instituteType) {
		InstituteBean insBean = InstituteBean.getInstituteDataBean();
		WaitUtils.waitForDisplayed(getTxtInstituteName());
		getTxtInstituteName().sendKeys(insBean.getInstituteName());
		getTxtPostal().sendKeys(insBean.getPostalCode());
		getTxtPostal().sendKeys(Keys.TAB);
		SCHUtils.waitForAjaxToComplete();
		getTxtZip().sendKeys(insBean.getZipCode());
		getTxtAddressOne().sendKeys(insBean.getAddressOne());
		getTxtPhone().sendKeys(insBean.getPhoneNumber());
		getTxtEnrollment().sendKeys(insBean.getEnrollment());
		if (customerGroup.equals("default")) {
			insBean.setCustomerGroup(ConstantUtils.INSTITUTE_CUSTOMER_GROUP);
		} else {
			insBean.setCustomerGroup(customerGroup);
		}
		if (customerType.equals("default")) {
			insBean.setCustomerType(ConstantUtils.INSTITUTE_CUSTOMER_TYPE);
		} else {
			insBean.setCustomerType(customerType);
		}
		getTxtCustomerGroup().sendKeys(insBean.getCustomerGroup());
		SCHUtils.waitForAjaxToComplete();
		getTxtCustomerType().sendKeys(insBean.getCustomerType());
		TestBaseProvider.getTestBase().getContext()
				.setProperty("testexecution.INSTITUTEDETAILS_" + instituteType.toUpperCase(), insBean);
	}

	public void fillInstituteLocationAddress() {
		WaitUtils.waitForDisplayed(getLinkAddress());
		SCHUtils.clickUsingJavaScript(getLinkAddress());
		WaitUtils.waitForDisplayed(getInputLocAddPostal());
		getInputLocAddPostal().sendKeys(ConstantUtils.LOC_ADD_POSTAL);
		getInputLocAddPostal().sendKeys(Keys.TAB);
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getInputLocAddAdd1());
		getInputLocAddAdd1().sendKeys(ConstantUtils.INSTITUTE_ADDRESS_ONE);
		// getInputLocAddZip4().sendKeys(ConstantUtils.LOC_ADD_ZIP4);
		getLinkInsSubmit().click();
		WaitUtils.waitForDisplayed(getLinkConfirmSave());
		getLinkConfirmSave().click();
		SCHUtils.waitForAjaxToComplete();
		getLinkConfirmSave().click();
		WaitUtils.waitForDisplayed(getLableUCN());
		TestBaseProvider.getTestBase().getContext().setProperty("CREATED_INSTITUTE_UCN_ADDRESS",
				getLableUCN().getText());
	}

	public void modifyParentAccount() {
		// navigate to content frame
		WebElement frameElement = getDriver().findElement(By.name("TargetContent"));
		getDriver().switchTo().frame(frameElement);

		// search with parent account
		WaitUtils.waitForDisplayed(getInputInstituteID());
		String ParentUCN = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_CHILD");
		System.out.println("parent UCN :: " + ParentUCN);
		getInputInstituteID().sendKeys(ParentUCN);
		SCHUtils.clickUsingJavaScript(getButtonSearchInstitute());

		WaitUtils.waitForDisplayed(getLinkInstituteName());
		SCHUtils.clickUsingJavaScript(getLinkInstituteName());

		WaitUtils.waitForDisplayed(getLinkManageRelationships());
		SCHUtils.clickUsingJavaScript(getLinkManageRelationships());

		WaitUtils.waitForDisplayed(getButtonEndRelationship());
		SCHUtils.clickUsingJavaScript(getButtonEndRelationship());

		WaitUtils.waitForDisplayed(getLinkYesEndRelationShip());
		SCHUtils.clickUsingJavaScript(getLinkYesEndRelationShip());

		clickSubmitAndRefresh();
		SCHUtils.waitForAjaxToComplete();

		WaitUtils.waitForDisplayed(getOptionStatus());
		getOptionStatus().sendKeys(ConstantUtils.APPROVE_END_REQUEST);

		clickSubmitAndRefresh();

		WaitUtils.waitForDisplayed(getButtonRequestNewMembership());
		SCHUtils.clickUsingJavaScript(getButtonRequestNewMembership());

		WaitUtils.waitForDisplayed(getButtonSearchRelatedChild());
		SCHUtils.clickUsingJavaScript(getButtonSearchRelatedChild());
		SCHUtils.waitForAjaxToComplete();

		String newParentUCN = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_NEW");
		System.out.println("new Parent UCN :: " + newParentUCN);
		WaitUtils.waitForDisplayed(getInputSearchUCN());
		getInputSearchUCN().sendKeys(newParentUCN);
		SCHUtils.clickUsingJavaScript(getButtonLookUp());

		WaitUtils.waitForDisplayed(getLinkSearchAccountID());
		SCHUtils.clickUsingJavaScript(getLinkSearchAccountID());

		clickSubmitAndRefresh();

		WaitUtils.waitForDisplayed(getOptionApprove());
		getOptionApprove().sendKeys(ConstantUtils.APPROVE_NEW_REQUEST);

		clickSubmitAndRefresh();
	}

	private void clickSubmitAndRefresh() {
		WaitUtils.waitForDisplayed(getLinkInsSubmit());
		SCHUtils.clickUsingJavaScript(getLinkInsSubmit());

		WaitUtils.waitForDisplayed(getButtonRefresh());
		SCHUtils.clickUsingJavaScript(getButtonRefresh());
	}

	public void mergeToUCNToParentAccount() {
		// search for parent ins
		getDriver().switchTo().defaultContent();
		WebElement frameElement = getDriver().findElement(By.name("NAV"));
		getDriver().switchTo().frame(frameElement);
		TMSCRMHomePage tmsHomePage = new TMSCRMHomePage();
		tmsHomePage.navigateToInstitutionSearchPage();

		// switch to content frame.
		getDriver().switchTo().defaultContent();
		WebElement contentFrameElement = getDriver().findElement(By.name("TargetContent"));
		getDriver().switchTo().frame(contentFrameElement);

		WaitUtils.waitForDisplayed(getInputInstituteID());
		String ParentUCN = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_CHILD");
		System.out.println("parent UCN :: " + ParentUCN);
		getInputInstituteID().sendKeys(ParentUCN);
		SCHUtils.clickUsingJavaScript(getButtonSearchInstitute());

		WaitUtils.waitForDisplayed(getLinkInstituteName());
		SCHUtils.clickUsingJavaScript(getLinkInstituteName());

		WaitUtils.waitForDisplayed(getLinkAdditionalInfo());
		SCHUtils.clickUsingJavaScript(getLinkAdditionalInfo());
		WaitUtils.waitForDisplayed(getLinkAccountAction());
		getLinkAccountAction().sendKeys(ConstantUtils.ACCOUNT_MERGE);
		WaitUtils.waitForDisplayed(getInputMergeToUCN());
		String newUCN = (String) TestBaseProvider.getTestBase().getContext().getProperty("CREATED_INSTITUTE_UCN_NEW");
		getInputMergeToUCN().sendKeys(newUCN);
		SCHUtils.clickUsingJavaScript(getButtonSearchUCN());
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.waitForLoaderToDismiss();
		int size = getLinkSrAccountID().size();
		if (size < 0) {
			Assert.fail("No Account Found for UCN Number " + newUCN);
		} else {
			SCHUtils.clickUsingJavaScript(getLinkSrAccountID().get(0));
			SCHUtils.waitForAjaxToComplete();
		}
		SCHUtils.clickUsingJavaScript(getLinkInsSubmit());
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getButtonRefresh());
		SCHUtils.clickUsingJavaScript(getButtonRefresh());
		SCHUtils.waitForAjaxToComplete();
	}

	public void enterpriseStatusUpdatedToClosed() {
		// Before terminate the contract waiting 5 minutes to sync SFDC
		for (int i = 0; i < 5; i++) {
			TestBaseProvider.getTestBase().getDriver().getTitle();
			SCHUtils.pause(60);
		}

		WaitUtils.waitForDisplayed(getLinkAdditionalInfo());
		SCHUtils.clickUsingJavaScript(getLinkAdditionalInfo());
		WaitUtils.waitForDisplayed(getLinkAccountAction());
		getLinkAccountAction().sendKeys(ConstantUtils.ACCOUNT_CLOSE);

		WaitUtils.waitForDisplayed(getLinkAccountCloseReason());
		getLinkAccountCloseReason().sendKeys(ConstantUtils.ACCOUNT_CLOSE_REASON);
		SCHUtils.waitForAjaxToComplete();

		SCHUtils.clickUsingJavaScript(getLinkInsSubmit());
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getButtonRefresh());
		SCHUtils.clickUsingJavaScript(getButtonRefresh());
		SCHUtils.waitForAjaxToComplete();
	}

	public void mergeSourceAccountWithTargetAccount() {
		// search for parent ins
		getDriver().switchTo().defaultContent();
		WebElement frameElement = getDriver().findElement(By.name("NAV"));
		getDriver().switchTo().frame(frameElement);
		TMSCRMHomePage tmsHomePage = new TMSCRMHomePage();
		tmsHomePage.navigateToInstitutionSearchPage();

		// switch to content frame.
		getDriver().switchTo().defaultContent();
		WebElement contentFrameElement = getDriver().findElement(By.name("TargetContent"));
		getDriver().switchTo().frame(contentFrameElement);

		WaitUtils.waitForDisplayed(getInputInstituteID());
		String sourceUCN = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_SECOND");
		System.out.println("parent UCN :: " + sourceUCN);
		getInputInstituteID().sendKeys(sourceUCN);
		SCHUtils.clickUsingJavaScript(getButtonSearchInstitute());

		WaitUtils.waitForDisplayed(getLinkInstituteName());
		SCHUtils.clickUsingJavaScript(getLinkInstituteName());

		WaitUtils.waitForDisplayed(getLinkAdditionalInfo());
		SCHUtils.clickUsingJavaScript(getLinkAdditionalInfo());
		WaitUtils.waitForDisplayed(getLinkAccountAction());
		getLinkAccountAction().sendKeys(ConstantUtils.ACCOUNT_MERGE);
		WaitUtils.waitForDisplayed(getInputMergeToUCN());
		String targetUCN = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_FIRST");
		getInputMergeToUCN().sendKeys(targetUCN);
		SCHUtils.clickUsingJavaScript(getButtonSearchUCN());
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.waitForLoaderToDismiss();
		int size = getLinkSrAccountID().size();
		if (size < 0) {
			Assert.fail("No Account Found for UCN Number " + targetUCN);
		} else {
			SCHUtils.clickUsingJavaScript(getLinkSrAccountID().get(0));
			SCHUtils.waitForAjaxToComplete();
		}
		SCHUtils.clickUsingJavaScript(getLinkInsSubmit());
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getButtonRefresh());
		SCHUtils.clickUsingJavaScript(getButtonRefresh());
		SCHUtils.waitForAjaxToComplete();
	}

	public void navigateToFirstInstitute() {
		// switch to content frame.
		getDriver().switchTo().defaultContent();
		WebElement contentFrameElement = getDriver().findElement(By.name("TargetContent"));
		getDriver().switchTo().frame(contentFrameElement);

		WaitUtils.waitForDisplayed(getInputInstituteID());
		String closeUCN = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_FIRST");
		System.out.println("Close UCN :: " + closeUCN);
		getInputInstituteID().sendKeys(closeUCN);
		SCHUtils.clickUsingJavaScript(getButtonSearchInstitute());

		WaitUtils.waitForDisplayed(getLinkInstituteName());
		SCHUtils.clickUsingJavaScript(getLinkInstituteName());
		SCHUtils.waitForAjaxToComplete();
	}

	public void updateEnterpriseStatusAsClose() {
		WaitUtils.waitForDisplayed(getLinkAdditionalInfo());
		SCHUtils.clickUsingJavaScript(getLinkAdditionalInfo());
		WaitUtils.waitForDisplayed(getLinkAccountAction());
		getLinkAccountAction().sendKeys(ConstantUtils.ACCOUNT_CLOSE);

		WaitUtils.waitForDisplayed(getLinkAccountCloseReason());
		getLinkAccountCloseReason().sendKeys(ConstantUtils.ACCOUNT_CLOSE_REASON);
		SCHUtils.waitForAjaxToComplete();

		SCHUtils.clickUsingJavaScript(getLinkInsSubmit());
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getButtonRefresh());
		SCHUtils.clickUsingJavaScript(getButtonRefresh());
		SCHUtils.waitForAjaxToComplete();
	}
}