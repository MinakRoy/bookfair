package com.scholastic.salesforce.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.scholastic.salesforce.automation.support.SCHUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;

public class TMSCRMHomePage extends BaseTestPage<TestPage> {

	@FindBy(locator = "TMS_INPUT_USERNAME")
	private WebElement inputUsername;
	@FindBy(locator = "TMS_INPUT_PASSWORD")
	private WebElement inputPassword;
	@FindBy(locator = "TMS_BUTTON_LOG_IN")
	private WebElement buttonLogin;
	@FindBy(locator = "TMS_LINK_INSTITUE_SEARCH")
	private WebElement linkInstituteSearch;
	@FindBy(locator = "TMS_LINK_SIGN_OUT")
	private WebElement linkSignOut;
	@FindBy(locator = "TMS_LINK_HOME")
	private WebElement linkHome;

	public WebElement getLinkHome() {
		return linkHome;
	}

	public WebElement getLinkSignOut() {
		return linkSignOut;
	}

	public WebElement getLinkInstituteSearch() {
		return linkInstituteSearch;
	}

	public WebElement getInputUsername() {
		return inputUsername;
	}

	public WebElement getInputPassword() {
		return inputPassword;
	}

	public WebElement getButtonLogin() {
		return buttonLogin;
	}

	@Override
	protected void openPage() {
		getDriver().get(getTestBase().getString("tmc.crm.base.url"));
	}

	public void doLogin() {
		String username = TestBaseProvider.getTestBase().getString("tmc.username");
		String password = TestBaseProvider.getTestBase().getString("tmc.password");
		getInputUsername().clear();
		getInputPassword().clear();
		getInputUsername().sendKeys(username);
		getInputPassword().sendKeys(password);
		getButtonLogin().click();
		WaitUtils.waitForDisplayed(getLinkSignOut());
	}

	public void a_new_account_details_are_sent_from_CDB() {
		openPage();
		doLogin();
		navigateToInstitutionSearchPage();
		TMSCRMInstitutePage tmsCRMInstitutePage = new TMSCRMInstitutePage();
		tmsCRMInstitutePage.clickOnCreateNewInstitute();
		tmsCRMInstitutePage.createInstitute("default", "default", "CHILD");
	}

	public void navigateToInstitutionSearchPage() {
		WaitUtils.waitForDisplayed(getLinkInstituteSearch());
		SCHUtils.clickUsingJavaScript(getLinkInstituteSearch());
	}

	public void a_new_account_is_sent_from_CDB_With_or_Without_a_Parent_Account(String parentStatus) {
		if (parentStatus.equals("with")) {
			a_new_account_details_are_sent_from_CDB();
			TMSCRMInstitutePage tmsCRMInstitutePage = new TMSCRMInstitutePage();
			getDriver().switchTo().defaultContent();
			WebElement frameElement = getDriver().findElement(By.name("NAV"));
			getDriver().switchTo().frame(frameElement);
			navigateToInstitutionSearchPage();
			tmsCRMInstitutePage.clickOnCreateNewInstitute();
			tmsCRMInstitutePage.createInstituteWithParent("default", "default", "PARENT");
		} else if (parentStatus.equals("without")) {
			a_new_account_details_are_sent_from_CDB();
		} else {
			Assert.fail("Wrong param " + parentStatus + " passed.");
		}
	}

	public void twoValidAccountExist() {
		openPage();
		doLogin();
		navigateToInstitutionSearchPage();
		TMSCRMInstitutePage tmsCRMInstitutePage = new TMSCRMInstitutePage();
		tmsCRMInstitutePage.clickOnCreateNewInstitute();
		tmsCRMInstitutePage.createInstitute("default", "default", "FIRST");
		getDriver().switchTo().defaultContent();
		WebElement frameElement = getDriver().findElement(By.name("NAV"));
		getDriver().switchTo().frame(frameElement);
		navigateToInstitutionSearchPage();
		tmsCRMInstitutePage.clickOnCreateNewInstitute();
		tmsCRMInstitutePage.createInstitute("default", "default", "SECOND");
		for (int i = 0; i < 5; i++) {
			TestBaseProvider.getTestBase().getDriver().getTitle();
			SCHUtils.pause(60);
		}
	}

	public void newAccountIsSentFromCDBWithAllAddresses() {
		openPage();
		doLogin();
		navigateToInstitutionSearchPage();
		TMSCRMInstitutePage tmsCRMInstitutePage = new TMSCRMInstitutePage();
		tmsCRMInstitutePage.clickOnCreateNewInstitute();
		tmsCRMInstitutePage.fillInstituteDetail("default", "default", "ADDRESS");
		tmsCRMInstitutePage.fillInstituteLocationAddress();
	}

	public void newAccountSentFromCDBWithOnlyOneAddress() {
		openPage();
		doLogin();
		navigateToInstitutionSearchPage();
		TMSCRMInstitutePage tmsCRMInstitutePage = new TMSCRMInstitutePage();
		tmsCRMInstitutePage.clickOnCreateNewInstitute();
		tmsCRMInstitutePage.createInstitute("default", "default", "CHILD");
	}

	public void accountExistWithParentAccountFieldPopulatedInAccountRelationship() {
		a_new_account_is_sent_from_CDB_With_or_Without_a_Parent_Account("with");
		SCHUtils.waitforDataToSyncInCDB();
	}

	public void parentAccountIsModified() {
		TMSCRMInstitutePage tmsCRMInstitutePage = new TMSCRMInstitutePage();
		getDriver().switchTo().defaultContent();
		WebElement frameElement = getDriver().findElement(By.name("NAV"));
		getDriver().switchTo().frame(frameElement);
		navigateToInstitutionSearchPage();
		tmsCRMInstitutePage.clickOnCreateNewInstitute();
		tmsCRMInstitutePage.createInstitute("default", "default", "NEW");

		// navigate to ins search page
		getDriver().switchTo().defaultContent();
		frameElement = getDriver().findElement(By.name("NAV"));
		getDriver().switchTo().frame(frameElement);
		navigateToInstitutionSearchPage();

		tmsCRMInstitutePage.modifyParentAccount();
	}

	public void validAccountExistWithParentAccount() {
		// with parent
		a_new_account_is_sent_from_CDB_With_or_Without_a_Parent_Account("with");
	}

	public void validAccountExistToReceiveMerge() {
		TMSCRMInstitutePage tmsCRMInstitutePage = new TMSCRMInstitutePage();
		getDriver().switchTo().defaultContent();
		WebElement frameElement = getDriver().findElement(By.name("NAV"));
		getDriver().switchTo().frame(frameElement);
		navigateToInstitutionSearchPage();
		tmsCRMInstitutePage.clickOnCreateNewInstitute();
		tmsCRMInstitutePage.createInstitute("default", "default", "NEW");
	}

	public void anAccountExistsInSalesforce() {
		a_new_account_details_are_sent_from_CDB();
	}

	public void mergetSourceAccountWithTargetAccount() {
		openPage();
		doLogin();
		navigateToInstitutionSearchPage();
		TMSCRMInstitutePage tmsCRMInstitutePage = new TMSCRMInstitutePage();
		tmsCRMInstitutePage.clickOnCreateNewInstitute();
		tmsCRMInstitutePage.mergeSourceAccountWithTargetAccount();
		// waiting 5 min to sync data from cdb to SFDC
		for (int i = 0; i < 6; i++) {
			TestBaseProvider.getTestBase().getDriver().getTitle();
			SCHUtils.pause(50);
		}
	}

	public void createFirstAccount() {
		openPage();
		doLogin();
		navigateToInstitutionSearchPage();
		TMSCRMInstitutePage tmsCRMInstitutePage = new TMSCRMInstitutePage();
		tmsCRMInstitutePage.clickOnCreateNewInstitute();
		tmsCRMInstitutePage.createInstitute("default", "default", "FIRST");
	}

	public void updateEnterpriseStatusAsClosed() {
		openPage();
		doLogin();
		navigateToInstitutionSearchPage();
		TMSCRMInstitutePage tmsCRMInstitutePage = new TMSCRMInstitutePage();
		getDriver().switchTo().defaultContent();
		WebElement frameElement = getDriver().findElement(By.name("NAV"));
		getDriver().switchTo().frame(frameElement);
		TMSCRMHomePage tmsHomePage = new TMSCRMHomePage();
		tmsHomePage.navigateToInstitutionSearchPage();
		tmsCRMInstitutePage.navigateToFirstInstitute();
		tmsCRMInstitutePage.updateEnterpriseStatusAsClose();
		SCHUtils.waitforDataToSyncInCDB();
	}
}