package com.scholastic.salesforce.automation.pages;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.opencsv.CSVReader;
import com.scholastic.salesforce.automation.beans.AccountBean;
import com.scholastic.salesforce.automation.beans.FieldServiceContractBean;
import com.scholastic.salesforce.automation.beans.InstituteBean;
import com.scholastic.salesforce.automation.steps.HomePageStepDefs;
import com.scholastic.salesforce.automation.support.ConstantUtils;
import com.scholastic.salesforce.automation.support.RandomDataUtil;
import com.scholastic.salesforce.automation.support.SCHUtils;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

public class AccountsPage extends BaseTestPage<HomePage> {

	@FindBy(locator = "ACCOUNT_LINK_NEW")
	private WebElement linkNew;
	@FindBy(locator = "ACCOUNT_BUTTON_ACCOUNT_SAVE")
	private WebElement buttonAccountSave;
	@FindBy(locator = "ACCOUNT_INPUT_ACCOUNT_NAME")
	private WebElement inputAccountName;
	@FindBy(locator = "ACCOUNT_LINK_RECENTLY_VIEWED")
	private WebElement linkRecentlyViewed;
	@FindBy(locator = "ACCOUNT_LINK_ALL_ACCOUNTS")
	private WebElement linkAllAccounts;
	@FindBy(locator = "ACCOUNT_LIST_LINK_ACCOUNT_NAME")
	private List<WebElement> listLinkAccountName;
	@FindBy(locator = "ACCOUNT_LINK_ACCOUNT_RELATIONSHIP_NEW")
	private WebElement linkAccountRelationshipNew;
	@FindBy(locator = "ACCOUNT_LINK_ACCOUNT_ADDRESSES_NEW")
	private WebElement linkAccountAddressesNew;
	@FindBy(locator = "ACCOUNT_INPUT_SEARCH_TOP")
	private WebElement inputSearchTop;
	@FindBy(locator = "ACCOUNT_LINK_DETAILS")
	private WebElement linkDetails;
	@FindBy(locator = "ACCOUNT_LABEL_ACCOUNT_RELATIONSHIP_NUMBER")
	private WebElement labelAccountRelationshipNumber;
	@FindBy(locator = "ACCOUNT_LINK_RELATED")
	private WebElement linkRelated;
	@FindBy(locator = "ACCOUNT_LABEL_ERROR_SAVE")
	private WebElement labelErrorSave;
	@FindBy(locator = "ACCOUNT_SECTION_ADDITIONAL_INFORMATION")
	private WebElement sectionAdditionalInformation;
	@FindBy(locator = "ACCOUNT_LINK_ACCOUNT_NAME_SRP")
	private List<WebElement> linkAccountNameSRP;
	@FindBy(locator = "ACCOUNT_LINK_MENU_ADDRESS_TRIGGER")
	private WebElement linkMenuAddressTrigger;
	@FindBy(locator = "ACCOUNT_LINK_MENU_RELATIONSHIP_TRIGGER")
	private WebElement linkMenuRelationshipTrigger;
	@FindBy(locator = "ACCOUNT_LINK_NO_ACTION_AVAILABLE")
	private WebElement linkNoActionAvailable;
	@FindBy(locator = "ACCOUNT_LABEL_CONTACTS")
	private WebElement labelContacts;
	@FindBy(locator = "ACCOUNT_LABEL_ENTERPRISE_STATUS")
	private WebElement labelEnterpriseStatus;
	@FindBy(locator = "ACCOUNT_LINK_ACCOUNT_ADDRESS")
	private List<WebElement> linkAccountAddress;
	@FindBy(locator = "ACCOUNT_LINK_ACCOUNT_RELATIONSHIP")
	private List<WebElement> linkAccountRelationShip;
	@FindBy(locator = "ACCOUNT_LINK_PARENT_ACCOUNT_FIELD")
	private WebElement linkParentAccountField;
	@FindBy(locator = "ACCOUNT_LINK_SURVIVOR_ACCOUNT")
	private WebElement linkSurvivorAccount;
	@FindBy(locator = "ACCOUNT_LINK_VIEW_ALL_ACCOUNT_RELATIONSHIP")
	private WebElement linkViewAllAccountRelationship;
	@FindBy(locator = "ACCOUNT_LABEL_ACCOUNT_RELATIONSHIP_END_DATE")
	private WebElement labelAccountRelationshipEndDate;
	@FindBy(locator = "ACCOUNT_LINK_NEW_OPPORTUNITIES")
	private WebElement linkNewOpportunities;
	@FindBy(locator = "ACCOUNT_LINK_STAGE_OPPORTUNITY")
	private WebElement linkStageOpportunity;
	@FindBy(locator = "ACCOUNT_INPUT_CLOSE_DATE_OPPORTUNITY")
	private WebElement inputCloseDateOpportunity;
	@FindBy(locator = "ACCOUNT_LINK_VIEW_ALL_OPPORTUNITIES")
	private WebElement linkViewAllOpportunities;
	@FindBy(locator = "ACCOUNT_LIST_OPPORTUNITY")
	private List<WebElement> listOpportunity;
	@FindBy(locator = "ACCOUNT_LINK_ADD_TASK")
	private WebElement linkAddTask;
	@FindBy(locator = "ACCOUNT_INPUT_SUBJECT_TASK")
	private WebElement inputSubjectTask;
	@FindBy(locator = "ACCOUNT_LINK_SAVE_TASK")
	private WebElement linkSaveTask;
	@FindBy(locator = "ACCOUNT_LINK_LIST_ALL_NOTES")
	private List<WebElement> linkListAllNotes;
	@FindBy(locator = "ACCOUNT_LINK_NEW_CONTACTS")
	private WebElement linkNewContacts;
	@FindBy(locator = "ACCOUNT_LABEL_CONTACTS_NUMBER")
	private WebElement labelContactsNumber;
	@FindBy(locator = "ACCOUNT_LINK_VIEW_ALL_CONTACTS")
	private WebElement linkViewAllContacts;
	@FindBy(locator = "ACCOUNT_LINK_NEW_EVENTS")
	private WebElement linkNewEvents;
	@FindBy(locator = "ACCOUNT_INPUT_SUBJECT_EVENT")
	private WebElement inputSubjectEvent;
	@FindBy(locator = "ACCOUNT_LINK_SAVE_EVENT")
	private WebElement linkSaveEvent;
	@FindBy(locator = "ACCOUNT_LINK_ADD_EVENT")
	private WebElement linkAddEvent;
	@FindBy(locator = "ACCOUNT_LINK_OPPORTUNITY_DROPDOWN")
	private WebElement linkOpportunityDropDown;
	@FindBy(locator = "ACCOUNT_LINK_EDIT_OPPORTUNITY")
	private WebElement linkEditOpportunity;
	@FindBy(locator = "ACCOUNT_LABEL_ADDRESS_TOP")
	private WebElement labelAddressTop;
	@FindBy(locator = "ACCOUNT_LABEL_ADDRESS_MUNICIPALITY")
	private WebElement labelAddressMunicipality;
	@FindBy(locator = "ACCOUNT_LINK_EDIT_DROPDOWN")
	private WebElement linkEditDropDown;
	@FindBy(locator = "ACCOUNT_LINK_NEW_NOTES")
	private WebElement linkNewNotes;
	@FindBy(locator = "ACCOUNT_INPUT_NOTE_TITLE")
	private WebElement inputNoteTitle;
	@FindBy(locator = "ACCOUNT_INPUT_NOTE_DISCRIPTION")
	private WebElement inputNoteDiscription;
	@FindBy(locator = "ACCOUNT_BUTTON_DONE_NOTE")
	private WebElement buttonDoneNote;
	@FindBy(locator = "ACCOUNT_LINK_VIEW_ALL_NOTE")
	private WebElement linkViewAllNote;
	@FindBy(locator = "ACCOUNT_HEADER_LABEL_CUSTOMER_DETAILS")
	private WebElement labelHeaderCustomerDetails;
	@FindBy(locator = "ACCOUNT_LINK_TIER_SELECT")
	private WebElement linkTierSelect;
	@FindBy(locator = "ACCOUNT_LINK_MORE_STEPS")
	private WebElement linkMoreSteps;

	public WebElement getLinkMoreSteps() {
		return linkMoreSteps;
	}

	public WebElement getLinkTierSelect() {
		return linkTierSelect;
	}

	public WebElement getLabelHeaderCustomerDetails() {
		return labelHeaderCustomerDetails;
	}

	public WebElement getLinkViewAllNote() {
		return linkViewAllNote;
	}

	public WebElement getLinkNewNotes() {
		return linkNewNotes;
	}

	public WebElement getInputNoteTitle() {
		return inputNoteTitle;
	}

	public WebElement getInputNoteDiscription() {
		return inputNoteDiscription;
	}

	public WebElement getButtonDoneNote() {
		return buttonDoneNote;
	}

	public WebElement getLinkEditDropDown() {
		return linkEditDropDown;
	}

	public WebElement getLabelAddressTop() {
		return labelAddressTop;
	}

	public WebElement getLabelAddressMunicipality() {
		return labelAddressMunicipality;
	}

	public WebElement getLinkOpportunityDropDown() {
		return linkOpportunityDropDown;
	}

	public WebElement getLinkEditOpportunity() {
		return linkEditOpportunity;
	}

	public WebElement getLinkAddEvent() {
		return linkAddEvent;
	}

	public WebElement getLinkSaveEvent() {
		return linkSaveEvent;
	}

	public WebElement getInputSubjectEvent() {
		return inputSubjectEvent;
	}

	public WebElement getLinkNewEvents() {
		return linkNewEvents;
	}

	public WebElement getLinkViewAllContacts() {
		return linkViewAllContacts;
	}

	public WebElement getLabelContactsNumber() {
		return labelContactsNumber;
	}

	public WebElement getLinkNewContacts() {
		return linkNewContacts;
	}

	public List<WebElement> getLinkListAllNotes() {
		return linkListAllNotes;
	}

	public WebElement getLinkAddTask() {
		return linkAddTask;
	}

	public WebElement getInputSubjectTask() {
		return inputSubjectTask;
	}

	public WebElement getLinkSaveTask() {
		return linkSaveTask;
	}

	public List<WebElement> getListOpportunity() {
		return listOpportunity;
	}

	public WebElement getLinkViewAllOpportunities() {
		return linkViewAllOpportunities;
	}

	public WebElement getInputCloseDateOpportunity() {
		return inputCloseDateOpportunity;
	}

	public WebElement getLinkStageOpportunity() {
		return linkStageOpportunity;
	}

	public WebElement getLinkNewOpportunities() {
		return linkNewOpportunities;
	}

	public WebElement getLabelAccountRelationshipEndDate() {
		return labelAccountRelationshipEndDate;
	}

	public WebElement getLinkViewAllAccountRelationship() {
		return linkViewAllAccountRelationship;
	}

	public WebElement getLinkSurvivorAccount() {
		return linkSurvivorAccount;
	}

	public WebElement getLinkParentAccountField() {
		return linkParentAccountField;
	}

	public List<WebElement> getLinkAccountRelationShip() {
		return linkAccountRelationShip;
	}

	public List<WebElement> getLinkAccountAddress() {
		return linkAccountAddress;
	}

	public WebElement getLabelEnterpriseStatus() {
		return labelEnterpriseStatus;
	}

	public WebElement getLabelContacts() {
		return labelContacts;
	}

	public WebElement getLinkMenuAddressTrigger() {
		return linkMenuAddressTrigger;
	}

	public WebElement getLinkMenuRelationshipTrigger() {
		return linkMenuRelationshipTrigger;
	}

	public WebElement getLinkNoActionAvailable() {
		return linkNoActionAvailable;
	}

	public List<WebElement> getLinkAccountNameSRP() {
		return linkAccountNameSRP;
	}

	public WebElement getSectionAdditionalInformation() {
		return sectionAdditionalInformation;
	}

	public WebElement getLabelErrorSave() {
		return labelErrorSave;
	}

	public WebElement getLinkRelated() {
		return linkRelated;
	}

	public WebElement getLabelAccountRelationshipNumber() {
		return labelAccountRelationshipNumber;
	}

	public WebElement getLinkDetails() {
		return linkDetails;
	}

	public WebElement getInputSearchTop() {
		return inputSearchTop;
	}

	public WebElement getLinkAccountRelationshipNew() {
		return linkAccountRelationshipNew;
	}

	public WebElement getLinkAccountAddressesNew() {
		return linkAccountAddressesNew;
	}

	public WebElement getLinkRecentlyViewed() {
		return linkRecentlyViewed;
	}

	public WebElement getLinkAllAccounts() {
		return linkAllAccounts;
	}

	public List<WebElement> getListLinkAccountName() {
		return listLinkAccountName;
	}

	public WebElement getInputAccountName() {
		return inputAccountName;
	}

	public WebElement getButtonAccountSave() {
		return buttonAccountSave;
	}

	public WebElement getLinkNew() {
		return linkNew;
	}

	@Override
	protected void openPage() {
	}

	public void clientIsCreatedWithThePreviouslyInvalidInstallToUCN() {
		navigatesToAccountsMenu();
		createClientWithPreviouslyInvalidToUCN();
	}

	private void createClientWithPreviouslyInvalidToUCN() {
		FieldServiceContractBean fsBean = (FieldServiceContractBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.FSCONTRACTDETAILS");
		String inValidUCNNumber = fsBean.getInstallToUCN();
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForElementToBeClickable(getLinkNew());
		getLinkNew().click();
		WebElement inputDynamicField = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_INPUT_DYNAMIC_ACCOUNT"), "UCN");
		String accountName = RandomDataUtil.getRandomAccountsName();
		WaitUtils.waitForDisplayed(inputDynamicField);
		inputDynamicField.sendKeys(inValidUCNNumber);
		getInputAccountName().sendKeys(accountName);
		TestBaseProvider.getTestBase().getContext().setProperty("CREATED_CLIENT_ACCOUNT_FOR_INVALID_UCN", accountName);
		getButtonAccountSave().click();
		SCHUtils.waitForAjaxToComplete();
	}

	public void navigatesToAccountsMenu() {
		HomePage homePage = new HomePage();
		homePage.navigateToMenu("Accounts");
	}

	public void accountShouldNotBeCreated(String type) {
		if (type.equals("Account")) {
			try {
				WaitUtils.waitForDisplayed(getLinkNew());
				Assert.fail("Link new is present");
			} catch (Exception ex) {
			}
		} else if (type.equals("Account Address")) {
			try {
				WaitUtils.waitForDisplayed(getLinkAccountAddressesNew());
				Assert.fail("New Account Address is present");
			} catch (Exception ex) {
			}
		} else if (type.equals("Account Relationship")) {
			try {
				WaitUtils.waitForDisplayed(getLinkAccountRelationshipNew());
				Assert.fail("New Account Relationship is present");
			} catch (Exception ex) {
			}
		}
	}

	public void attemptsToCreateAccount(String type) {
		if (type.equals("Account Address") || type.equals("Account Relationship")) {
			navigatesToAccountsMenu();
			navigateToAnyAccount();
			// SCHUtils.clickUsingJavaScript(getLinkRelated());
			getLinkRelated().click();
			SCHUtils.waitForAjaxToComplete();
			// SCHUtils.scrollElementIntoView(getLabelContacts());
		} else if (type.equals("Account")) {
			navigatesToAccountsMenu();
		} else {
			Assert.fail("Wrong Parameter Passed.");
		}
	}

	/*
	 * Updated by : naeemahemad mansuri Date : 14th August Reason : remove click
	 * on all account as they are removed from menu
	 */

	public void navigateToAnyAccount() {
		// getLinkRecentlyViewed().click();
		// WaitUtils.waitForDisplayed(getLinkAllAccounts());
		// SCHUtils.clickUsingJavaScript(getLinkAllAccounts());
		// SCHUtils.waitForAjaxToComplete();
		int size = getListLinkAccountName().size();
		if (size < 0) {
			Assert.fail("There are No Accounts.");
		} else {
			SCHUtils.clickUsingJavaScript(getListLinkAccountName().get(0));
			SCHUtils.waitForAjaxToComplete();
		}
	}

	public void the_new_account_should_be_created_in_SFDC(String instituteType) {
		InstituteBean instituteBean = (InstituteBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.INSTITUTEDETAILS_" + instituteType.toUpperCase());
		String UCNNumber = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_" + instituteType.toUpperCase());
		System.out.println(instituteType + " contract Search at " + TMSCRMInstitutePage.printCurrentDateTime());
		searchForAccount(UCNNumber);
		verifyAccountSearchResult(instituteBean, instituteType);
		navigateToCreatedAccount(instituteBean);
		WaitUtils.waitForDisplayed(getLinkDetails());
		getLinkDetails().click();
		SCHUtils.waitForAjaxToComplete();
		verifyAccountDetailsCreatedWithCDB(instituteBean, instituteType);
	}

	private void verifyAccountDetailsCreatedWithCDB(InstituteBean instituteBean, String instituteType) {
		WebElement dynamicField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "Account Name");
		WaitUtils.waitForDisplayed(dynamicField);
		AssertUtils.assertTextMatches(dynamicField,
				Matchers.equalToIgnoringCase(instituteBean.getInstituteName().toUpperCase()));
		dynamicField = SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"),
				"UCN");
		String UCNNumber = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_" + instituteType.toUpperCase());
		AssertUtils.assertTextMatches(dynamicField, Matchers.equalTo(UCNNumber));

		dynamicField = SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"),
				"Customer Type");
		AssertUtils.assertTextMatches(dynamicField, Matchers.equalTo(instituteBean.getCustomerType().toUpperCase()));

		dynamicField = SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"),
				"Enrollment");
		AssertUtils.assertTextMatches(dynamicField, Matchers.equalTo(instituteBean.getEnrollment()));
	}

	public void navigateToCreatedAccount(InstituteBean instituteBean) {
		WebElement dynamicField = null;
		try {
			dynamicField = SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getString("ACCOUNT_SRP_DYNAMIC_FIELD"),
					instituteBean.getInstituteName().toUpperCase());
			WaitUtils.waitForDisplayed(dynamicField);
		} catch (Exception e) {
			Assert.fail(
					"Account " + instituteBean.getInstituteName().toUpperCase() + " details not received from CDB.");
		}
		SCHUtils.clickUsingJavaScript(dynamicField);
		SCHUtils.waitForAjaxToComplete();
	}

	private void verifyAccountSearchResult(InstituteBean instituteBean, String instituteType) {
		WebElement dynamicField = null;
		try {
			dynamicField = SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getString("ACCOUNT_SRP_DYNAMIC_FIELD"),
					instituteBean.getInstituteName().toUpperCase());
			WaitUtils.waitForDisplayed(dynamicField);
		} catch (Exception ex) {
			Assert.fail("No New Account details received from CDB, Search UCN number is " + (String) TestBaseProvider
					.getTestBase().getContext().getProperty("CREATED_INSTITUTE_UCN_" + instituteType.toUpperCase()));
		}
		AssertUtils.assertTextMatches(dynamicField, Matchers.equalTo(instituteBean.getInstituteName().toUpperCase()));
		String UCNNumber = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_" + instituteType.toUpperCase());
		dynamicField = SCHUtils.findElement(TestBaseProvider.getTestBase().getString("ACCOUNT_SRP_DYNAMIC_FIELD"),
				UCNNumber);
		AssertUtils.assertTextMatches(dynamicField, Matchers.equalTo(UCNNumber));
		dynamicField = SCHUtils.findElement(TestBaseProvider.getTestBase().getString("ACCOUNT_SRP_DYNAMIC_FIELD"),
				instituteBean.getCustomerType().toUpperCase());
		AssertUtils.assertTextMatches(dynamicField, Matchers.equalTo(instituteBean.getCustomerType().toUpperCase()));
	}

	public void searchForAccount(String UCNNumber) {
		WaitUtils.waitForDisplayed(getInputSearchTop());
		getInputSearchTop().sendKeys(UCNNumber);
		WebElement dynamicSuggestion = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LINK_SUGGESTION"), UCNNumber);
		WaitUtils.waitForDisplayed(dynamicSuggestion);
		SCHUtils.clickUsingJavaScript(dynamicSuggestion);
		SCHUtils.waitForAjaxToComplete();
	}

	public void a_Account_Relationship_is_created_with_the_account_populated_in_the_Related_Child_Institution_field() {
		WaitUtils.waitForDisplayed(getLinkRelated());
		SCHUtils.clickUsingJavaScript(getLinkRelated());
		SCHUtils.pause(3);
		((JavascriptExecutor) getDriver()).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		SCHUtils.waitForAjaxToComplete();
		int size = getLinkAccountRelationShip().size();
		System.out.println("Account relationship size :: " + size);
		if (size >= 1) {
			for (int i = 0; i < size; i++) {
				AssertUtils.assertDisplayed(getLinkAccountRelationShip().get(i));
			}
		} else {
			Assert.fail("Account Relationship is not created with related child institution field.");
		}
	}

	public void the_Parent_Account_is_populated_in_the_Parent_Institution_field() {
		getLinkAccountRelationShip().get(0).click();
		// SCHUtils.clickUsingJavaScript(getLinkAccountRelationShip().get(0));
		SCHUtils.waitForAjaxToComplete();
		// getLinkDetails().click();
		// SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getLinkParentAccountField());
		InstituteBean instituteBean = (InstituteBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.INSTITUTEDETAILS_CHILD");
		String parentAccountField = instituteBean.getInstituteName().toUpperCase();
		System.out.println("parent Account Field ::" + parentAccountField);
		// AssertUtils.assertTextMatches(getLinkParentAccountField(),
		// Matchers.equalTo(""));
	}

	public void the_creation_date_is_in_the_Start_Date() {
		WebElement startDate = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_CONTRACT_FROM_LABEL"), "Start Date");
		AssertUtils.assertTextMatches(startDate, Matchers.equalTo(FieldServiceContractPage.getDate("0")));
	}

	public void the_End_date_is_left_blank() {
		WebElement endDate = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_CONTRACT_FROM_LABEL"), "End Date");
		AssertUtils.assertNotDisplayed(endDate);
	}

	public void no_Account_Relationship_is_created() {
		getLinkRelated().click();
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getLabelAccountRelationshipNumber());
		SCHUtils.scrollElementIntoView(getLabelAccountRelationshipNumber());
		String number = getLabelAccountRelationshipNumber().getText().replace("(", "").replace(")", "");
		assertEquals(number, "0", "Accout relationship is " + number + " instead of 0.");
	}

	public void the_user_is_on_a_page_with_account_fields() {
		navigatesToAccountsMenu();
	}

	public void the_user_attempts_to_edit_any_account_field() {
		navigatesToRecentlyViewvedFirstAccount();

		WebElement dynamicEditButton = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("ACCOUNT_BUTTON_EDIT_DYNAMIC_FIELD"), "Account Name");
		WaitUtils.waitForDisplayed(dynamicEditButton);
		SCHUtils.clickUsingJavaScript(dynamicEditButton);
		WebElement dynamicEditField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("ACCOUNT_INPUT_EDIT_DYNAMIC_FIELD"), "Account Name");
		WaitUtils.waitForDisplayed(dynamicEditField);
		dynamicEditField.clear();
		dynamicEditField.sendKeys(ConstantUtils.EDIT_ACCOUNT_NAME);
		dynamicEditField.sendKeys(Keys.TAB);
		WebElement buttonEditSave = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_BUTTON_EDIT_SAVE"), "");
		WaitUtils.waitForDisplayed(buttonEditSave);
		SCHUtils.clickUsingJavaScript(buttonEditSave);
		SCHUtils.waitForAjaxToComplete();
	}

	public void they_should_not_be_able_to_make_the_update() {
		Assert.assertTrue(getLabelErrorSave().isDisplayed(), "user is able to make update.");
	}

	public void the_user_attempts_to_edit_the_field(String arg1) {
		navigatesToAccountsMenu();
		// navigatesToRecentlyViewvedFirstAccount();
		searchForAccount(ConstantUtils.ACCOUNT_TO_EDIT);
		navigateToAccount(ConstantUtils.ACCOUNT_TO_EDIT);
		WaitUtils.waitForDisplayed(getLinkDetails());
		// getLinkDetails().click();
		// SCHUtils.waitForAjaxToComplete();
		SCHUtils.scrollElementIntoView(getSectionAdditionalInformation());
		WebElement dynamicEditButton = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_BUTTON_EDIT_DYNAMIC_FIELD"), arg1);
		WaitUtils.waitForDisplayed(dynamicEditButton);
		SCHUtils.clickUsingJavaScript(dynamicEditButton);
		if (arg1.equals("Tier")) {
			WaitUtils.waitForDisplayed(getLinkTierSelect());
			SCHUtils.clickUsingJavaScript(getLinkTierSelect());
			WebElement dynamicSelectTier = SCHUtils
					.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LINK_DYNAMIC_TIER_VALUE"), "2");
			SCHUtils.clickUsingJavaScript(dynamicSelectTier);
		} else if (arg1.equals("Gold Customer")) {
			WebElement dynamicEditField = SCHUtils
					.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_INPUT_EDIT_DYNAMIC_FIELD"), arg1);
			WaitUtils.waitForDisplayed(dynamicEditField);
			SCHUtils.clickUsingJavaScript(dynamicEditField);
		} else {
			Assert.fail("Wrong param " + arg1 + " passed.");
		}
		WebElement buttonEditSave = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_BUTTON_EDIT_SAVE"), "");
		WaitUtils.waitForDisplayed(buttonEditSave);
		SCHUtils.clickUsingJavaScript(buttonEditSave);
		SCHUtils.waitForAjaxToComplete();
	}

	private void navigatesToRecentlyViewvedFirstAccount() {
		int size = getListLinkAccountName().size();
		if (size < 0) {
			Assert.fail("There are No Accounts.");
		} else {
			SCHUtils.clickUsingJavaScript(getListLinkAccountName().get(0));
			SCHUtils.waitForAjaxToComplete();
		}
	}

	public void they_should_be_able_to_make_the_update(String arg1) {
		if (arg1.equals("Tier")) {
			try {
				WaitUtils.waitForDisplayed(getLabelErrorSave());
				Assert.fail("User Should be able to update " + arg1);
			} catch (Exception ex) {
			}
			// revert back tier to none
			revertBackTierToNone(arg1);
		} else if (arg1.equals("Gold Customer")) {
			try {
				WaitUtils.waitForDisplayed(getLabelErrorSave());
				Assert.fail("User Should be able to update " + arg1);
			} catch (Exception ex) {
			}
		}
	}

	public void revertBackTierToNone(String arg1) {
		WebElement dynamicEditButton = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_BUTTON_EDIT_DYNAMIC_FIELD"), arg1);
		WaitUtils.waitForDisplayed(dynamicEditButton);
		SCHUtils.clickUsingJavaScript(dynamicEditButton);
		WaitUtils.waitForDisplayed(getLinkTierSelect());
		SCHUtils.clickUsingJavaScript(getLinkTierSelect());
		WebElement dynamicSelectTier = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("ACCOUNT_LINK_DYNAMIC_TIER_VALUE"), "--None--");
		SCHUtils.clickUsingJavaScript(dynamicSelectTier);
		WebElement buttonEditSave = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_BUTTON_EDIT_SAVE"), "");
		WaitUtils.waitForDisplayed(buttonEditSave);
		SCHUtils.clickUsingJavaScript(buttonEditSave);
		SCHUtils.waitForAjaxToComplete();
	}

	public void the_user_attempts_to_edit_any_account_address_field() {
		searchForAccount(ConstantUtils.UCN_SEARCH_FOR_ADDRESS_UPDATE);
		navigateToFirstSearchAccount();
		WaitUtils.waitForDisplayed(getLinkRelated());
		SCHUtils.clickUsingJavaScript(getLinkRelated());
		SCHUtils.waitForAjaxToComplete();
		((JavascriptExecutor) getDriver()).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WaitUtils.waitForDisplayed(getLinkMenuAddressTrigger());
		SCHUtils.clickUsingJavaScript(getLinkMenuAddressTrigger());
	}

	private void navigateToFirstSearchAccount() {
		int size = getLinkAccountNameSRP().size();
		if (size < 0) {
			Assert.fail("Search UCN Account is not found.");
		} else {
			SCHUtils.clickUsingJavaScript(getLinkAccountNameSRP().get(0));
			SCHUtils.waitForAjaxToComplete();
		}
	}

	public void shouldNotBeAbleToMakeTheEditAccountAddresField() {
		WaitUtils.waitForDisplayed(getLinkNoActionAvailable());
		Assert.assertTrue(getLinkNoActionAvailable().isDisplayed(), "User is able to edit account address field");
	}

	public void shouldNotBeAbleToEditAccountRelationShipField() {
		WaitUtils.waitForDisplayed(getLinkNoActionAvailable());
		Assert.assertTrue(getLinkNoActionAvailable().isDisplayed(), "User is able to edit account relation field");
	}

	/**
	 * remove scrolling
	 */

	public void userAttemptsToEditAnyAccountRelationshipField() {
		searchForAccount(ConstantUtils.UCN_SEARCH_FOR_RELATIONSHIP_UPDATE);
		navigateToFirstSearchAccount();
		getLinkRelated().click();
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getLinkMenuRelationshipTrigger());
		SCHUtils.clickUsingJavaScript(getLinkMenuRelationshipTrigger());
	}

	public void verifyaccountStatusUpdatedToTerminated() {
		WaitUtils.waitForDisplayed(getLabelEnterpriseStatus());
		AssertUtils.assertTextMatches(getLabelEnterpriseStatus(), Matchers.equalTo("TERMINATED"));
	}

	public void verifyAddressArePopulatedWithAddressTypeDetails() {
		int size = getLinkAccountAddress().size();
		System.out.println("Size of Address :: " + size);
		for (int i = 0; i < size; i++) {
			SCHUtils.clickUsingJavaScript(getLinkAccountAddress().get(i));
			SCHUtils.waitForAjaxToComplete();
			WebElement dynamicElement = returnDynamicAccountAddressElement("Address Line 1");
			AssertUtils.assertDisplayed(dynamicElement);
			dynamicElement = returnDynamicAccountAddressElement("Address Type");
			AssertUtils.assertDisplayed(dynamicElement);
			dynamicElement = returnDynamicAccountAddressElement("City");
			AssertUtils.assertDisplayed(dynamicElement);
			dynamicElement = returnDynamicAccountAddressElement("Country");
			AssertUtils.assertDisplayed(dynamicElement);
			dynamicElement = returnDynamicAccountAddressElement("Zip/Postal Code");
			AssertUtils.assertDisplayed(dynamicElement);
			getDriver().navigate().back();
			SCHUtils.waitForAjaxToComplete();
		}
	}

	public WebElement returnDynamicAccountAddressElement(String field) {
		return SCHUtils.findElement(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), field);
	}

	public void verifyOneAddressPopulatedWithMailingAddress() {
		try {
			WaitUtils.waitForDisplayed(getLinkAccountAddress().get(0));
		} catch (Exception e) {
			Assert.fail("Address Not Found.");
		}
		int size = getLinkAccountAddress().size();
		System.out.println("Size of Address :: " + size);
		if (size != 1) {
			Assert.fail("There are more then one address.");
		}
		SCHUtils.clickUsingJavaScript(getLinkAccountAddress().get(0));
		SCHUtils.waitForAjaxToComplete();
		WebElement dynamicElement = returnDynamicAccountAddressElement("Address Line 1");
		AssertUtils.assertDisplayed(dynamicElement);
		dynamicElement = returnDynamicAccountAddressElement("Address Type");
		AssertUtils.assertDisplayed(dynamicElement);
		AssertUtils.assertTextMatches(dynamicElement, Matchers.equalTo("Mailing"));
		dynamicElement = returnDynamicAccountAddressElement("City");
		AssertUtils.assertDisplayed(dynamicElement);
		dynamicElement = returnDynamicAccountAddressElement("Country");
		AssertUtils.assertDisplayed(dynamicElement);
		dynamicElement = returnDynamicAccountAddressElement("Zip/Postal Code");
		AssertUtils.assertDisplayed(dynamicElement);
	}

	public void the_mailing_address_should_also_populate_the_mailing_address_in_the_top_bar() {
		WaitUtils.waitForDisplayed(getLabelAddressTop());
		String applicationAddress = getLabelAddressTop().getText().replace(",", "");
		assertEquals(applicationAddress, ConstantUtils.INSTITUTE_ADDRESS_ONE.toUpperCase());
		AssertUtils.assertDisplayed(getLabelAddressMunicipality());
	}

	public void mailingCountyShouldBePopulatedBasedOnMailingCountyCode() {
		try {
			WebElement countyCodeElement = SCHUtils.findElement(
					TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "Mailing County Code");
			AssertUtils.assertDisplayed(countyCodeElement);
			if (countyCodeElement.getText().equals(null) || countyCodeElement.getText().equals("")) {
				Assert.fail("Mailing County Code is not present.");
			}
		} catch (Exception e) {
			Assert.fail("Mailing County Code is not present.");
		}
		try {
			WebElement mailingCountyElement = SCHUtils.findElement(
					TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "Mailing County");
			AssertUtils.assertDisplayed(mailingCountyElement);
			if (mailingCountyElement.getText().equals(null) || mailingCountyElement.getText().equals("")) {
				Assert.fail("Mailing County is not Present.");
			}
		} catch (Exception e) {
			Assert.fail("Mailing County is not Present.");
		}
	}

	public void verifyLinkToMergedAccountFromTheSourceAccount() {
		WaitUtils.waitForDisplayed(getLinkSurvivorAccount());
		AssertUtils.assertTextMatches(getLinkSurvivorAccount(), Matchers.containsString("salesforce.com"));
	}

	public void accountInRelatedChildInstitutionField() {
		InstituteBean newParentInstituteBean = (InstituteBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.INSTITUTEDETAILS_NEW");

		String newParentAccountName = newParentInstituteBean.getInstituteName().toUpperCase();
		WebElement newParent = SCHUtils.findElement(
				TestBaseProvider.getTestBase().getString("ACCOUNT_LINK_RELATED_CHILD_INSTITUTE"), newParentAccountName);

		WaitUtils.waitForDisplayed(newParent);
		AssertUtils.assertDisplayed(newParent);

		// WebElement newChildUCN = SCHUtils
		// .waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"),
		// "UCN");
		// String newParentUCNString = (String)
		// TestBaseProvider.getTestBase().getContext()
		// .getProperty("CREATED_INSTITUTE_UCN_NEW");
		// AssertUtils.assertTextMatches(newChildUCN,
		// Matchers.equalTo(newParentUCNString));
	}

	public void newAccountRelationshipCreatedWithNewParent() {
		String ucnNumber = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_CHILD");
		System.out.println("UCN :: " + ucnNumber);
		searchForAccount(ucnNumber);
		InstituteBean instituteBean = (InstituteBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.INSTITUTEDETAILS_CHILD");
		navigateToCreatedAccount(instituteBean);

		WaitUtils.waitForDisplayed(getLinkRelated());
		getLinkRelated().click();
		SCHUtils.waitForAjaxToComplete();

		SCHUtils.scrollElementIntoView(getLabelContacts());
		SCHUtils.waitForAjaxToComplete();
		// SCHUtils.clickUsingJavaScript(newParent);
		// SCHUtils.waitForAjaxToComplete();
	}

	public void accountListedInRelatedChildInstituteFieldChangeToCurrentDate() {
		String ucnNumber = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_CHILD");
		System.out.println("UCN :: " + ucnNumber);
		searchForAccount(ucnNumber);
		InstituteBean instituteBean = (InstituteBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.INSTITUTEDETAILS_CHILD");
		navigateToCreatedAccount(instituteBean);

		WaitUtils.waitForDisplayed(getLinkRelated());
		getLinkRelated().click();
		SCHUtils.waitForAjaxToComplete();

		SCHUtils.scrollElementIntoView(getLabelContacts());

		try {
			WaitUtils.waitForDisplayed(getLinkViewAllAccountRelationship());
		} catch (Exception e) {
			Assert.fail("No new parent relation created in child UCN " + ucnNumber + " account.");
		}

		SCHUtils.clickUsingJavaScript(getLinkViewAllAccountRelationship());
		SCHUtils.waitForAjaxToComplete();

		WaitUtils.waitForDisplayed(getLabelAccountRelationshipEndDate());

		String todayDate = FieldServiceContractPage.getDate("0");
		AssertUtils.assertTextMatches(getLabelAccountRelationshipEndDate(), Matchers.equalTo(todayDate));
	}

	public void userShouldNotbeAbleToMakeUpdateIn(String arg1) {
		if (arg1.equals("Tier")) {
			the_user_attempts_to_edit_the_field(arg1);
			theyShouldNotBeAbleToMakeTheUpdate(arg1);
			// they_should_be_able_to_make_the_update(arg1);
		} else if (arg1.equals("Gold Customer")) {
			the_user_attempts_to_edit_the_field(arg1);
			// they_should_be_able_to_make_the_update(arg1);
		} else {
			Assert.fail("Wrong param " + arg1 + " passed.");
		}
	}

	public void theyShouldNotBeAbleToMakeTheUpdate(String arg1) {
		if (arg1.equals("Segmentation")) {

		} else if (arg1.equals("Gold Customer")) {
			try {
				WaitUtils.waitForDisplayed(getLabelErrorSave());
				Assert.fail("User Should be able to update " + arg1);
			} catch (Exception ex) {
			}
		} else {
			Assert.fail("Wrong param " + arg1 + " passed.");
		}
	}

	public void numberOfOpportunityExist(String arg1) {
		WaitUtils.waitForDisplayed(getLinkRelated());
		getLinkRelated().click();
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.scrollElementIntoView(getLabelContacts());
		for (int i = 0; i < Integer.parseInt(arg1); i++) {
			WaitUtils.waitForDisplayed(getLinkNewOpportunities());
			SCHUtils.clickUsingJavaScript(getLinkNewOpportunities());
			FillAndCreateOpportunity();
		}
	}

	public void validAccountExistsToReceiveAMergeNumberOfOpportunity(String arg1) {
		navigateToSecondAccount();
		WaitUtils.waitForDisplayed(getLinkRelated());
		getLinkRelated().click();
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.scrollElementIntoView(getLabelContacts());
		for (int i = 0; i < Integer.parseInt(arg1); i++) {
			WaitUtils.waitForDisplayed(getLinkNewOpportunities());
			SCHUtils.clickUsingJavaScript(getLinkNewOpportunities());
			FillAndCreateOpportunity();
		}
	}

	public void FillAndCreateOpportunity() {
		WebElement inputDynamicField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("ACCOUNT_INPUT_DYNAMIC_ACCOUNT"), "Opportunity Name");
		WaitUtils.waitForDisplayed(inputDynamicField);
		inputDynamicField.sendKeys(RandomDataUtil.getRandomOpportunityName());

		getLinkStageOpportunity().click();
		WebElement dynamicSelect = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("ACCOUNT_LINK_DYNAMIC_STAGE_OPPORTUNITY"), "Prospect");
		SCHUtils.waitForelementToBeClickable(dynamicSelect);
		SCHUtils.clickUsingJavaScript(dynamicSelect);

		String closeDate = FieldServiceContractPage.getDate("2");
		getInputCloseDateOpportunity().sendKeys(closeDate);
		getButtonAccountSave().click();
		SCHUtils.waitForAjaxToComplete();
	}

	public WebElement getOpportunityInput(String elementText) {
		WebElement element = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_INPUT_DYNAMIC_ACCOUNT"), elementText);
		return element;
	}

	public void navigateToFirstAccount() {
		String ucnNumber = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_FIRST");
		System.out.println("UCN :: " + ucnNumber);
		searchForAccount(ucnNumber);
		InstituteBean instituteBean = (InstituteBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.INSTITUTEDETAILS_FIRST");
		navigateToCreatedAccount(instituteBean);
	}

	public void navigateToSecondAccount() {
		String ucnNumber = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_SECOND");
		System.out.println("UCN :: " + ucnNumber);
		searchForAccount(ucnNumber);
		InstituteBean instituteBean = (InstituteBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.INSTITUTEDETAILS_SECOND");
		navigateToCreatedAccount(instituteBean);
	}

	public void oportunityShouldMovedToReceivingAccount() {
		HomePageStepDefs stepDef = new HomePageStepDefs();
		stepDef.userLoginInToSystem();
		navigatesToAccountsMenu();
		navigateToFirstAccount();
		WaitUtils.waitForDisplayed(getLinkRelated());
		getLinkRelated().click();
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.scrollElementIntoView(getLabelContacts());
		WaitUtils.waitForDisplayed(getLinkViewAllOpportunities());
		SCHUtils.clickUsingJavaScript(getLinkViewAllOpportunities());
		SCHUtils.waitForAjaxToComplete();
	}

	public void ReceivingAccountShouldHaveNumberOfOpportunities(String arg1) {
		String size = String.valueOf(getListOpportunity().size());
		Assert.assertEquals(size, arg1, "Receiving account not have " + arg1 + " number of Opportunities.");
	}

	public void taskShouldMovedToReceivingAccount() {
		HomePageStepDefs stepDef = new HomePageStepDefs();
		stepDef.userLoginInToSystem();
		navigatesToAccountsMenu();
		navigateToFirstAccount();
	}

	public void receivingAccountShouldHaveNumberOfTaskExists(String arg1) {
		getLinkMoreSteps().click();
		SCHUtils.pause(5);
		String size = String.valueOf(getLinkListAllNotes().size());
		Assert.assertEquals(size, arg1, "Receiving account not have " + arg1 + " number of Tasks.");
	}

	public void numberOfTasksExists(String arg1) {
		for (int i = 0; i < Integer.parseInt(arg1); i++) {
			fillAndCreateTask();
		}
	}

	public void fillAndCreateTask() {
		WaitUtils.waitForDisplayed(getLinkAddTask());
		SCHUtils.clickUsingJavaScript(getLinkAddTask());
		WaitUtils.waitForDisplayed(getInputSubjectTask());
		getInputSubjectTask().clear();
		getInputSubjectTask().sendKeys(RandomDataUtil.getRandomTask());
		getInputSubjectTask().sendKeys(Keys.TAB);
		SCHUtils.waitForelementToBeClickable(getLinkSaveTask());
		SCHUtils.clickUsingJavaScript(getLinkSaveTask());
		SCHUtils.waitForAjaxToComplete();
	}

	public void validAccountExistsToReceiveAMergeNumberOfTasks(String arg1) {
		navigatesToAccountsMenu();
		navigateToSecondAccount();
		WaitUtils.waitForDisplayed(getLinkRelated());
		for (int i = 0; i < Integer.parseInt(arg1); i++) {
			fillAndCreateTask();
		}
	}

	public void contactsShouldMovedToReceivingAccount() {
		HomePageStepDefs stepDef = new HomePageStepDefs();
		stepDef.userLoginInToSystem();
		navigatesToAccountsMenu();
		navigateToFirstAccount();
		WaitUtils.waitForDisplayed(getLinkRelated());
		SCHUtils.clickUsingJavaScript(getLinkRelated());
	}

	public void receivingAccountShouldHaveNumberOfContactsExists(String arg1) {
		SCHUtils.scrollElementIntoView(getLabelContacts());
		WaitUtils.waitForDisplayed(getLabelContactsNumber());
		String number = getLabelContactsNumber().getText().replace("(", "").replace(")", "");
		assertEquals(number, arg1, "Accout contacts is " + number + " instead of " + arg1 + ".");

		WaitUtils.waitForDisplayed(getLinkViewAllContacts());
		SCHUtils.clickUsingJavaScript(getLinkViewAllContacts());
		SCHUtils.waitForAjaxToComplete();

		String size = String.valueOf(getListOpportunity().size());
		Assert.assertEquals(size, arg1, "Receiving account not have " + arg1 + " number of Contacts.");
	}

	public void eventsShouldMovedToReceivingAccount() {
		HomePageStepDefs stepDef = new HomePageStepDefs();
		stepDef.userLoginInToSystem();
		navigatesToAccountsMenu();
		navigateToFirstAccount();
	}

	public void receivingAccountShouldHaveNumberOfEventsExists(String arg1) {
		getLinkMoreSteps().click();
		SCHUtils.pause(5);
		String size = String.valueOf(getLinkListAllNotes().size());
		Assert.assertEquals(size, arg1, "Receiving account not have " + arg1 + " number of Events.");
	}

	public void numberOfContactsExists(String arg1) {
		WaitUtils.waitForDisplayed(getLinkRelated());
		SCHUtils.clickUsingJavaScript(getLinkRelated());
		SCHUtils.waitForAjaxToComplete();
		for (int i = 0; i < Integer.parseInt(arg1); i++) {
			fillAndCreateContacts();
		}
	}

	public void fillAndCreateContacts() {
		WaitUtils.waitForElementToBeClickable(getLinkNewContacts());
		SCHUtils.clickUsingJavaScript(getLinkNewContacts());
		WebElement dynamicElement = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("ACCOUNT_INPUT_DYNAMIC_ACCOUNT"), "First Name");
		WaitUtils.waitForDisplayed(dynamicElement);
		dynamicElement.sendKeys(RandomDataUtil.getRandomFirstAndLastName());
		dynamicElement = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_INPUT_DYNAMIC_ACCOUNT"), "Last Name");
		dynamicElement.sendKeys(RandomDataUtil.getRandomFirstAndLastName());
		dynamicElement.sendKeys(Keys.TAB);
		SCHUtils.scrollElementIntoView(getButtonAccountSave());
		SCHUtils.clickUsingJavaScript(getButtonAccountSave());
		SCHUtils.waitForAjaxToComplete();
	}

	public void validAccountExistsToReceiveMergeNumberofContracts(String arg1) {
		navigatesToAccountsMenu();
		navigateToSecondAccount();
		numberOfContactsExists(arg1);
	}

	public void numberOfEventsExists(String arg1) {
		WaitUtils.waitForDisplayed(getLinkNewEvents());
		SCHUtils.clickUsingJavaScript(getLinkNewEvents());
		SCHUtils.waitForAjaxToComplete();
		for (int i = 0; i < Integer.parseInt(arg1); i++) {
			fillAndCreateEvents();
		}
	}

	public void fillAndCreateEvents() {
		getInputSubjectEvent().sendKeys(RandomDataUtil.getRandomEvent());
		getInputSubjectEvent().sendKeys(Keys.TAB);
		WaitUtils.waitForElementToBeClickable(getLinkSaveEvent());
		SCHUtils.clickUsingJavaScript(getLinkSaveEvent());
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.clickUsingJavaScript(getLinkAddEvent());
	}

	public void validAccountExistsToReceiveMergeNumberofEvents(String arg1) {
		navigatesToAccountsMenu();
		navigateToSecondAccount();
		numberOfEventsExists(arg1);
	}

	public void endUserInSalesforceShouldNotbeABletoMakeanUpdateTo(String arg1) {
		if (arg1.equals("Opportunity")) {
			verifyInOpportunityShouldNotBeAbleToMakeUpdate();
		} else if (arg1.equals("Tasks")) {
			verifyInTaskShouldNotBeAbleToMakeUpdate();
		} else if (arg1.equals("Events")) {
			verifyInEventsShouldNotBeAbleToMakeUpdate();
		} else if (arg1.equals("Note")) {
			verifyInNoteShouldNotBeAbleToMakeUpdate();
		} else if (arg1.equals("Contact")) {
			verifyInContactShouldNotBeAbleToMakeUpdate();
		} else {
			Assert.fail("Wrong " + arg1 + " Passed.");
		}
	}

	public void verifyInNoteShouldNotBeAbleToMakeUpdate() {
		WaitUtils.waitForDisplayed(getLinkRelated());
		getLinkRelated().click();
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getLinkViewAllNote());
		SCHUtils.clickUsingJavaScript(getLinkViewAllNote());
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getLinkOpportunityDropDown());
		SCHUtils.clickUsingJavaScript(getLinkOpportunityDropDown());
		try {
			WaitUtils.waitForDisplayed(getLinkEditOpportunity());
			Assert.fail("Note edit link is present");
		} catch (Exception ex) {
		}
	}

	public void verifyInEventsShouldNotBeAbleToMakeUpdate() {
		WaitUtils.waitForDisplayed(getLinkEditDropDown());
		SCHUtils.clickUsingJavaScript(getLinkEditDropDown());
		try {
			WaitUtils.waitForDisplayed(getLinkEditOpportunity());
			Assert.fail("Events edit link is present");
		} catch (Exception e) {
		}
	}

	public void verifyInContactShouldNotBeAbleToMakeUpdate() {
		WaitUtils.waitForDisplayed(getLinkRelated());
		getLinkRelated().click();
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.scrollElementIntoView(getLabelContacts());
		WaitUtils.waitForDisplayed(getLinkViewAllContacts());
		SCHUtils.clickUsingJavaScript(getLinkViewAllContacts());
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getLinkOpportunityDropDown());
		SCHUtils.clickUsingJavaScript(getLinkOpportunityDropDown());
		try {
			WaitUtils.waitForDisplayed(getLinkEditOpportunity());
			Assert.fail("Contacts edit link is present");
		} catch (Exception ex) {
		}
	}

	public void verifyInTaskShouldNotBeAbleToMakeUpdate() {

	}

	public void verifyInOpportunityShouldNotBeAbleToMakeUpdate() {
		WaitUtils.waitForDisplayed(getLinkRelated());
		getLinkRelated().click();
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.scrollElementIntoView(getLabelContacts());
		WaitUtils.waitForDisplayed(getLinkViewAllOpportunities());
		SCHUtils.clickUsingJavaScript(getLinkViewAllOpportunities());
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getLinkOpportunityDropDown());
		SCHUtils.clickUsingJavaScript(getLinkOpportunityDropDown());

		try {
			WaitUtils.waitForDisplayed(getLinkEditOpportunity());
			Assert.fail("Opportunity edit link is present");
		} catch (Exception ex) {
		}

		// WebElement dynamicElement = SCHUtils
		// .waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_INPUT_DYNAMIC_ACCOUNT"),
		// "");
		// WaitUtils.waitForDisplayed(dynamicElement);
		// dynamicElement.sendKeys("modify");

	}

	public void numberOfNotesExists(String arg1) {
		WaitUtils.waitForDisplayed(getLinkRelated());
		SCHUtils.clickUsingJavaScript(getLinkRelated());
		SCHUtils.waitForAjaxToComplete();
		for (int i = 0; i < Integer.parseInt(arg1); i++) {
			fillAndCreateNote();
		}
	}

	public void fillAndCreateNote() {
		SCHUtils.scrollElementIntoView(getLabelContacts());
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getLinkNewNotes());
		SCHUtils.clickUsingJavaScript(getLinkNewNotes());
		WaitUtils.waitForDisplayed(getInputNoteTitle());
		getInputNoteTitle().sendKeys(RandomDataUtil.getRandomNoteTitle());
		// getInputNoteDiscription().sendKeys(RandomDataUtil.getRandomNoteDiscription());
		WaitUtils.waitForDisplayed(getButtonDoneNote());
		SCHUtils.clickUsingJavaScript(getButtonDoneNote());
		SCHUtils.waitForAjaxToComplete();
	}

	public void AccountsIsUpdated() {
		setFirstTwoUpdatedRecordPropertyFromFile();
		for (int i = 0; i < 2; i++) {
			navigatesToAccountsMenu();
			String accountNameForSearch = (String) TestBaseProvider.getTestBase().getContext()
					.getProperty("UPDATED_ACCOUNT_NAME_" + i);
			String updatedSchool = (String) TestBaseProvider.getTestBase().getContext()
					.getProperty("UPDATED_NUMBER_OF_SCHOOL_" + i);
			searchForAccount(accountNameForSearch);
			navigateToAccount(accountNameForSearch);
			WebElement noOfSchoolElement = SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "# of Schools");
			AssertUtils.assertTextMatches(noOfSchoolElement, Matchers.equalTo(updatedSchool));
		}
	}

	public void navigateToAccount(String accountNameForSearch) {
		WebElement dynamicField = null;
		try {
			dynamicField = SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getString("ACCOUNT_SRP_DYNAMIC_FIELD"),
					accountNameForSearch.toUpperCase());
			WaitUtils.waitForDisplayed(dynamicField);
		} catch (Exception e) {
			Assert.fail("Account " + accountNameForSearch + " is not found.");
		}
		SCHUtils.clickUsingJavaScript(dynamicField);
		SCHUtils.waitForAjaxToComplete();
	}

	public void setFirstTwoUpdatedRecordPropertyFromFile() {
		String CSVNEW = "./src/test/resources/mdr/MDRBLDFEED1.csv";
		CSVReader reader = null;
		int i = 0;
		try {
			reader = new CSVReader(new FileReader(CSVNEW));
			String[] nextLine = null;
			while ((nextLine = reader.readNext()) != null) {
				TestBaseProvider.getTestBase().getContext().setProperty("UPDATED_ACCOUNT_NAME_" + i, nextLine[14]);
				TestBaseProvider.getTestBase().getContext().setProperty("UPDATED_NUMBER_OF_SCHOOL_" + i, nextLine[35]);
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/* verifying no duplicate records created in salesforce */
	public void the_identical_entry_should_not_create_a_duplicate_in_Salesforce() {
		setFirstTwoExistingRecordPropertyFromFile();
		for (int i = 0; i < 2; i++) {
			navigatesToAccountsMenu();
			String accountNameForSearch = (String) TestBaseProvider.getTestBase().getContext()
					.getProperty("EXISTING_ACCOUNT_NAME_" + i);
			searchForAccount(accountNameForSearch);
			List<WebElement> dynamicField = null;
			try {
				dynamicField = SCHUtils.findElements(
						TestBaseProvider.getTestBase().getString("ACCOUNT_SRP_DYNAMIC_FIELD"),
						accountNameForSearch.toUpperCase());
				WaitUtils.waitForDisplayed(dynamicField.get(0));
			} catch (Exception e) {
				Assert.fail("Account " + accountNameForSearch + " is not found.");
			}
			int size = dynamicField.size();
			if (size != 1) {
				Assert.fail("Duplicate contract found in salesforce.");
			}
		}
	}

	public void setFirstTwoExistingRecordPropertyFromFile() {
		String CSVNEW = "./src/test/resources/mdr/MDRBLDFEEDEXISTINGRECORD.csv";
		CSVReader reader = null;
		int i = 0;
		try {
			reader = new CSVReader(new FileReader(CSVNEW));
			String[] nextLine = null;
			while ((nextLine = reader.readNext()) != null) {
				TestBaseProvider.getTestBase().getContext().setProperty("EXISTING_ACCOUNT_NAME_" + i, nextLine[14]);
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void settingPreConditionForBatchProcessing() {
		navigatesToAccountsMenu();
		searchForAccount(ConstantUtils.EDIT_BATCHPROCESSING_ACCOUNT);
		navigateToAccount(ConstantUtils.EDIT_BATCHPROCESSING_ACCOUNT);
		SCHUtils.scrollElementIntoView(getLabelHeaderCustomerDetails());
		SCHUtils.waitForAjaxToComplete();

		WebElement noOfSchoolElement = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "# of Schools");
		System.out.println("Number of SCHOOL After Edit :: " + noOfSchoolElement.getText());
		if (!noOfSchoolElement.equals("1")) {
			FieldServiceContractPage.clickOnEditButton("# of Schools");
			FieldServiceContractPage fieldServicePage = new FieldServiceContractPage();
			fieldServicePage.enterUpdatedValueIntoLineQuantityAndSave("# of Schools", "1");
			noOfSchoolElement = SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "# of Schools");
			System.out.println("Number of SCHOOL After Edit :: " + noOfSchoolElement.getText());
		}
	}

	public void dataOutsideTheFirstBatchShouldNotBeProcessed() {
		navigatesToAccountsMenu();
		searchForAccount(ConstantUtils.EDIT_BATCHPROCESSING_ACCOUNT);
		navigateToAccount(ConstantUtils.EDIT_BATCHPROCESSING_ACCOUNT);
		WebElement noOfSchoolElement = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "# of Schools");
		AssertUtils.assertTextMatches(noOfSchoolElement, Matchers.equalTo("1"));
	}

	public void itIsSuccessful() {
		HomePageStepDefs homePageStepDefs = new HomePageStepDefs();
		homePageStepDefs.userLoginInToSystem();
		HomePage homePage = new HomePage();
		homePage.navigateToSetUpMenu();
		ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tabs2.get(1));
		homePage.navigateToBulkDataLoadJobs();
		homePage.verifyItIsSuccessful();
		getDriver().close();
		getDriver().switchTo().window(tabs2.get(0));
	}

	public void userShouldNotbeAbleToMakeUpdateInClosedAccount(String arg1) {
		navigatesToAccountsMenu();
		SCHUtils.waitforDataToSyncInCDB();
		InstituteBean instituteBean = (InstituteBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.INSTITUTEDETAILS_CHILD");
		String UCNNumber = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_CHILD");
		searchForAccount(UCNNumber);
		navigateToCreatedAccount(instituteBean);
		if (arg1.equals("Tier")) {
			tryToUpdateTier();
		} else if (arg1.equals("Gold Customer")) {
			tryToUpdateGoldCustomer(arg1);
		}
		try {
			WaitUtils.waitForDisplayed(getLabelErrorSave());
			AssertUtils.assertDisplayed(getLabelErrorSave());
		} catch (Exception e) {
			Assert.fail("user is able to update " + arg1 + ".");
		}
	}

	public void tryToUpdateGoldCustomer(String arg1) {
		SCHUtils.scrollElementIntoView(getSectionAdditionalInformation());
		WebElement dynamicEditButton = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_BUTTON_EDIT_DYNAMIC_FIELD"), arg1);
		WaitUtils.waitForDisplayed(dynamicEditButton);
		SCHUtils.clickUsingJavaScript(dynamicEditButton);

		WebElement dynamicEditField = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_INPUT_EDIT_DYNAMIC_FIELD"), arg1);
		WaitUtils.waitForDisplayed(dynamicEditField);
		SCHUtils.clickUsingJavaScript(dynamicEditField);

		WebElement buttonEditSave = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_BUTTON_EDIT_SAVE"), "");
		WaitUtils.waitForDisplayed(buttonEditSave);
		SCHUtils.clickUsingJavaScript(buttonEditSave);
	}

	public void tryToUpdateTier() {
		FieldServiceContractPage.clickOnEditButton("Tier");
		WaitUtils.waitForDisplayed(getLinkTierSelect());
		SCHUtils.clickUsingJavaScript(getLinkTierSelect());
		WebElement dynamicSelectTier = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LINK_DYNAMIC_TIER_VALUE"), "2");
		SCHUtils.clickUsingJavaScript(dynamicSelectTier);

		WebElement buttonEditSave = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_BUTTON_EDIT_SAVE"), "");
		WaitUtils.waitForDisplayed(buttonEditSave);
		SCHUtils.clickUsingJavaScript(buttonEditSave);
	}

	public void entryShouldBeAvailableInSFDC() {
		navigatesToAccountsMenu();
		searchForAccount(ConstantUtils.ACCOUNT_ERROR_PROCESSING_EDIT_MDR_ID);
		navigateToAccount(ConstantUtils.ACCOUNT_ERROR_PROCESSING_EDIT_MDR_ID);

		WebElement noOfSchoolElement = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "# of Schools");
		System.out.println("Number of School :: " + noOfSchoolElement.getText());
		String updatedSchool = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("UPDATED_SCHOOL_FOR_ERROR");
		AssertUtils.assertTextMatches(noOfSchoolElement, Matchers.equalTo(updatedSchool));

	}

	public void valuesForAllTheFieldsShouldBePresentInSalesForce() {
		setPropertyFromUpdatedFile();
		navigatesToAccountsMenu();
		String fieldValidationAccountName = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("FIELD_VALIDATION_ACCOUNT_NAME");
		System.out.println("Account Name :: " + fieldValidationAccountName);
		searchForAccount(fieldValidationAccountName);
		navigateToAccount(fieldValidationAccountName);
		verifyFieldValuePopulated();

		MDRPage mdrPage = new MDRPage();
		mdrPage.processedPreconditionFileForFieldService();
	}

	public void verifyFieldValuePopulated() {
		String fieldNumberOfSchool = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("FIELD_VALIDATION_NO_OF_SCHOOL");
		String fieldAFAMPercentage = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("FIELD_VALIDATION_AF_AM_PERCENTAGE");
		String fieldNTAMPercentage = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("FIELD_VALIDATION_NATIVE_AM_PERCENTAGE");
		String fieldAsianAMPercentage = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("FIELD_VALIDATION_ASIAN_AM_PERCENTAGE");
		String fieldHispanicAMPercentage = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("FIELD_VALIDATION_HIS_AM_PERCENTAGE");
		String fieldTitleOneDollar = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("FIELD_VALIDATION_TITLE_1_DOLLAR");
		String fieldStudenTitleOne = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("FIELD_VALIDATION_PERCENTAGE_STUDENT_TITLE");

		WebElement dynamicField = returnDynamicAccountField("# of Schools");
		AssertUtils.assertTextMatches(dynamicField, Matchers.equalToIgnoringCase(fieldNumberOfSchool));

		dynamicField = returnDynamicAccountField("African American %");
		AssertUtils.assertTextMatches(dynamicField, Matchers.containsString(fieldAFAMPercentage));

		dynamicField = returnDynamicAccountField("Native American %");
		AssertUtils.assertTextMatches(dynamicField, Matchers.containsString(fieldNTAMPercentage));

		dynamicField = returnDynamicAccountField("Asian American %");
		AssertUtils.assertTextMatches(dynamicField, Matchers.containsString(fieldAsianAMPercentage));

		dynamicField = returnDynamicAccountField("Hispanic American %");
		AssertUtils.assertTextMatches(dynamicField, Matchers.containsString(fieldHispanicAMPercentage));

		dynamicField = returnDynamicAccountField("Title1 Dollars");
		AssertUtils.assertTextMatches(dynamicField, Matchers.containsString(fieldTitleOneDollar));

		dynamicField = returnDynamicAccountField("% Students/Title1");
		AssertUtils.assertTextMatches(dynamicField, Matchers.containsString(fieldStudenTitleOne));
	}

	public WebElement returnDynamicAccountField(String fieldName) {
		return SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"),
				fieldName);
	}

	public void setPropertyFromUpdatedFile() {
		String CSVNEW = "./src/test/resources/mdr/MDRBLDFEEDALLFIELDUPDATEDVALUE.csv";
		CSVReader reader = null;
		try {
			reader = new CSVReader(new FileReader(CSVNEW));
			String[] nextLine = null;
			while ((nextLine = reader.readNext()) != null) {
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_ACCOUNT_NAME", nextLine[14]);
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_NO_OF_SCHOOL", nextLine[35]);
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_AF_AM_PERCENTAGE",
						nextLine[70]);
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_NATIVE_AM_PERCENTAGE",
						nextLine[71]);
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_ASIAN_AM_PERCENTAGE",
						nextLine[72]);
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_HIS_AM_PERCENTAGE",
						nextLine[73]);
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_BEFORE_AFTER", nextLine[95]);
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_YEAR_AROUND", nextLine[96]);
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_TITLE_STUDENT",
						nextLine[102]);
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_TITLE_1_DOLLAR",
						nextLine[104]);
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_COMPUTER_COUNT",
						nextLine[170]);
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_ESL", nextLine[176]);
				TestBaseProvider.getTestBase().getContext().setProperty("FIELD_VALIDATION_PERCENTAGE_STUDENT_TITLE",
						nextLine[190]);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void the_record_is_searched_in_Salesforce(String recordNumber) {
		int intRecordNumber = 0;
		if (recordNumber.equals("first")) {
			intRecordNumber = 1;
		} else if (recordNumber.equals("10th")) {
			intRecordNumber = 11;
		} else if (recordNumber.equals("50th")) {
			intRecordNumber = 51;
		} else if (recordNumber.equals("last")) {
			intRecordNumber = 100;
		} else {
			Assert.fail("Wronge record Number Passed.");
		}
		getAccountDataFromFile(intRecordNumber);

		HomePageStepDefs homePageSteps = new HomePageStepDefs();
		homePageSteps.userLoginInToSystem();
		navigatesToAccountsMenu();
		String UCNNumber = (String) TestBaseProvider.getTestBase().getContext().getProperty("ACCOUNT_UCN");
		System.out.println("UCN Number form Prop ::  " + UCNNumber);
		AccountBean accountBean = (AccountBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("ACCOUNT_DETAIL");
		System.out.println("Account Name From bean ::  " + accountBean.getAccountName());
		searchForAccount(UCNNumber);
		navigateToAccount(accountBean.getAccountName().toUpperCase());
	}

	public void getAccountDataFromFile(int recordNumber) {
		String fileToRead = "./src/test/resources/s3/" + TestBaseProvider.getTestBase().getString("FILENAME");
		File file = new File(fileToRead);
		System.out.println("isExists :: " + file.exists() + "File Path :: " + fileToRead);
		CSVReader reader = null;
		int i = 0;
		try {
			reader = new CSVReader(new FileReader(file));
			// List<String[]> totalRecord = reader.readAll();
			// System.out.println("total ::" + totalRecord.size());
			// reader = new CSVReader(new FileReader(file));
			AccountBean accountBean = new AccountBean();
			String[] nextLine = null;
			while ((nextLine = reader.readNext()) != null) {
				if (i == 0) {
					System.out.println("Skipping Header.");
				} else if (i == recordNumber) {
					TestBaseProvider.getTestBase().getContext().setProperty("ACCOUNT_UCN", nextLine[1]);
					setAccountarrayIntoBean(nextLine, accountBean);
					break;
				} else if (i == 101) {
					break;
				}
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (i == 1) {
			Assert.fail("There are no records in file.");
		}
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void setAccountarrayIntoBean(String[] nextLine, AccountBean accountBean) {
		accountBean.setAccountName(nextLine[2]);
		accountBean.setPhone(nextLine[4]);
		accountBean.setCustomerType(nextLine[8]);
		accountBean.setOrganizationType(nextLine[7]);
		accountBean.setInstitutionStatus(nextLine[6]);
		accountBean.setNumberOfSchool(nextLine[12]);
		accountBean.setAfricanAmericanPer(nextLine[15]);
		accountBean.setAsianAmericanPer(nextLine[16]);
		accountBean.setHispanicAmericanPer(nextLine[17]);
		accountBean.setNativeAmericanPer(nextLine[18]);
		accountBean.setMailingStreet(nextLine[19]);
		accountBean.setMailingCity(nextLine[20]);
		accountBean.setMailingCounty(nextLine[21]);
		accountBean.setMailingState(nextLine[22]);
		accountBean.setMailingZip(nextLine[23]);
		accountBean.setMailingCountry(nextLine[24]);
		accountBean.setFax(nextLine[5]);
		System.out.println("bean :: " + accountBean.toString());
		TestBaseProvider.getTestBase().getContext().setProperty("ACCOUNT_DETAIL", accountBean);
	}

	public void the_values_of_the_record_should_match_the_record_returned_in_the_search_results(String recordNumber,
			String type) {
		if (type.equals("Account")) {
			AccountBean accountBean = (AccountBean) TestBaseProvider.getTestBase().getContext()
					.getProperty("ACCOUNT_DETAIL");
			WebElement dynamicField = SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "Account Name");
			WaitUtils.waitForDisplayed(dynamicField);
			verifyAccountField("Account Name", accountBean.getAccountName());
			verifyAccountField("# of Schools", accountBean.getNumberOfSchool());
			verifyAccountField("African American %", accountBean.getAfricanAmericanPer());
			verifyAccountField("Native American %", accountBean.getNativeAmericanPer());
			verifyAccountField("Asian American %", accountBean.getAsianAmericanPer());
			verifyAccountField("Hispanic American %", accountBean.getHispanicAmericanPer());
			verifyAccountField("Mailing County", accountBean.getMailingCounty());
			verifyAccountField("Phone", accountBean.getPhone());
			verifyAccountField("Fax", accountBean.getFax());
		} else if (type.equals("User")) {
			HomePage homePage = new HomePage();
			homePage.navigateToSetUpMenu();
			ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(tabs2.get(1));
			homePage.navigateToUserMenu();
			getDriver().switchTo().frame(homePage.getFrameContent());
			homePage.verifyUserData();
		} else if (type.equals("District")) {
			WaitUtils.waitForDisplayed(getLinkParentAccountField());
			getLinkParentAccountField().click();
			SCHUtils.waitForAjaxToComplete();
			String parentUCN = (String) TestBaseProvider.getTestBase().getContext().getProperty("PARENT_UCN");

			WebElement ucnNumber = SCHUtils
					.findElement(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "UCN");
			AssertUtils.assertTextMatches(ucnNumber, Matchers.equalTo(parentUCN));
		} else if (type.equals("Territory")) {
			String user = null;
			String roleInTerritory = null;
			if (recordNumber.equals("first")) {
				user = ConstantUtils.FIRST_TERRITORY_VERIFY[0];
				roleInTerritory = ConstantUtils.FIRST_TERRITORY_VERIFY[1];
			} else if (recordNumber.equals("100th")) {
				user = ConstantUtils.HUNDRED_TERRITORY_VERIFY[0];
				roleInTerritory = ConstantUtils.HUNDRED_TERRITORY_VERIFY[1];
			} else if (recordNumber.equals("500th")) {
				user = ConstantUtils.FIVE_HUNDRED_TERRITORY_VERIFY[0];
				roleInTerritory = ConstantUtils.FIVE_HUNDRED_TERRITORY_VERIFY[1];
			} else if (recordNumber.equals("last")) {
				user = ConstantUtils.LAST_HUNDRED_TERRITORY_VERIFY[0];
				roleInTerritory = ConstantUtils.LAST_HUNDRED_TERRITORY_VERIFY[1];
			} else {
				Assert.fail("Wrong Param Passed.");
			}
			WebElement dynamicUserName = SCHUtils
					.findElement(TestBaseProvider.getTestBase().getString("REPORT_LINK_DYNAMIC_USERNAME"), user);
			WaitUtils.waitForDisplayed(dynamicUserName);
			WebElement roleInTerritoryElement = SCHUtils.findElement(
					TestBaseProvider.getTestBase().getString("REPORT_LABEL_DYNAMIC_RIT"), user, roleInTerritory);
			WaitUtils.waitForDisplayed(roleInTerritoryElement);

		} else {
			Assert.fail("Wrong Param Paseed.");
		}
	}

	public void verifyAccountField(String field, String beanValue) {
		if (beanValue.equals("") || beanValue.equals(null)) {
			System.out.println("No validation for " + field + " field, Empty/NULL field value");
		} else {
			WebElement dynamicField = returnDynamicAccountField(field);
			AssertUtils.assertTextMatches(dynamicField, Matchers.containsString(beanValue));
		}
	}

	public void the_district_record_is_searched_in_salesforce(String recordNumber) {
		int intRecordNumber = 0;
		if (recordNumber.equals("first")) {
			intRecordNumber = 1;
		} else if (recordNumber.equals("10th")) {
			intRecordNumber = 11;
		} else if (recordNumber.equals("50th")) {
			intRecordNumber = 51;
		} else if (recordNumber.equals("last")) {
			intRecordNumber = 100;
		} else {
			Assert.fail("Wronge record Number Passed.");
		}
		getDistrictDataFromFile(intRecordNumber);

		HomePageStepDefs homePageStep = new HomePageStepDefs();
		homePageStep.userLoginInToSystem();
		navigatesToAccountsMenu();

		String UCNNumber = (String) TestBaseProvider.getTestBase().getContext().getProperty("CHILD_UCN");
		searchForAccount(UCNNumber);
		navigateToAccountThroughUCN(UCNNumber);
	}

	public void navigateToAccountThroughUCN(String UCNNumber) {
		WebElement dynamicField = null;
		try {
			dynamicField = SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getString("ACCOUNT_LINK_FROM_UCN_NUMBER"), UCNNumber);
			WaitUtils.waitForDisplayed(dynamicField);
		} catch (Exception e) {
			Assert.fail("Account " + UCNNumber + " is not found.");
		}
		SCHUtils.clickUsingJavaScript(dynamicField);
		SCHUtils.waitForAjaxToComplete();
	}

	public void getDistrictDataFromFile(int recordNumber) {
		String fileToRead = "./src/test/resources/s3/" + TestBaseProvider.getTestBase().getString("FILENAME");
		File file = new File(fileToRead);
		CSVReader reader = null;
		int i = 0;
		try {
			reader = new CSVReader(new FileReader(file));
			String[] nextLine = null;
			while ((nextLine = reader.readNext()) != null) {
				if (i == 0) {
					System.out.println("Skipping Header.");
				} else if (i == recordNumber) {
					while (nextLine[0].equals("") || nextLine[0].equals(null)) {
						nextLine = reader.readNext();
						i++;
					}
					TestBaseProvider.getTestBase().getContext().setProperty("CHILD_UCN", nextLine[1]);
					TestBaseProvider.getTestBase().getContext().setProperty("PARENT_UCN", nextLine[0]);
					break;
				} else if (i == 101) {
					break;
				}
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (i == 1) {
			Assert.fail("There are no records in file.");
		}
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		int recordNumber = 100;
		String fileToRead = "./src/test/resources/s3/DISTRICT-09252017064525.csv";
		File file = new File(fileToRead);
		CSVReader reader = null;
		int i = 0;
		try {
			reader = new CSVReader(new FileReader(file));
			String[] nextLine = null;
			while ((nextLine = reader.readNext()) != null) {
				if (i == 0) {
					System.out.println("Skipping Header.");
				} else if (i == recordNumber) {
					while (nextLine[0].equals("") || nextLine[0].equals(null)) {
						nextLine = reader.readNext();
						i++;
					}
					System.out.println("nextline[0] ::" + nextLine[0]);
					TestBaseProvider.getTestBase().getContext().setProperty("CHILD_UCN", nextLine[1]);
					TestBaseProvider.getTestBase().getContext().setProperty("PARENT_UCN", nextLine[1]);
					break;
				} else if (i == 101) {
					break;
				}
				i++;
			}
		} catch (

		FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (i == 1) {
			Assert.fail("There are no records in file.");
		}
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
