package com.scholastic.salesforce.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.scholastic.salesforce.automation.support.ConstantUtils;
import com.scholastic.salesforce.automation.support.SCHUtils;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import junit.framework.Assert;

public class ReportPage extends BaseTestPage<HomePage> {

	@FindBy(locator = "REPORT_LINK_ALL_FOLDERS")
	private WebElement linkAllFolders;
	@FindBy(locator = "REPORT_LINK_QA_TEST_REPORT")
	private WebElement linkQATestReport;
	@FindBy(locator = "REPORT_LINK_CONTRACT_DATE_REPORT")
	private WebElement linkContractDateReport;
	@FindBy(locator = "REPORT_LINK_EDIT")
	private WebElement LinkEdit;
	@FindBy(locator = "REPORT_SERVICE_ORDER_BLOCK_HVOER")
	private WebElement blockServiceOrderHover;
	@FindBy(locator = "REPORT_INPUT_SERVICE_ORDER_NUMBER")
	private WebElement inpuptServiceOrderNumber;
	@FindBy(locator = "REPORT_BUTTON_EDIT_OK")
	private WebElement buttonEditOk;
	@FindBy(locator = "REPORT_BUTTON_RUN_REPORT")
	private WebElement buttonRunReport;
	@FindBy(locator = "REPORT_LABEL_START_DATE")
	private WebElement labelStartDate;
	@FindBy(locator = "REPORT_LABEL_END_DATE")
	private WebElement labelEndDate;
	@FindBy(locator = "REPORT_LINK_FILTER_EDIT")
	private WebElement linkFilterEdit;
	@FindBy(locator = "REPORT_UCN_BLOCK_HVOER")
	private WebElement blockUCNHover;

	public WebElement getBlockUCNHover() {
		return blockUCNHover;
	}

	public WebElement getLinkFilterEdit() {
		return linkFilterEdit;
	}

	public WebElement getLinkAllFolders() {
		return linkAllFolders;
	}

	public WebElement getLinkQATestReport() {
		return linkQATestReport;
	}

	public WebElement getLinkContractDateReport() {
		return linkContractDateReport;
	}

	public WebElement getLinkEdit() {
		return LinkEdit;
	}

	public WebElement getBlockServiceOrderHover() {
		return blockServiceOrderHover;
	}

	public WebElement getInpuptServiceOrderNumber() {
		return inpuptServiceOrderNumber;
	}

	public WebElement getButtonEditOk() {
		return buttonEditOk;
	}

	public WebElement getButtonRunReport() {
		return buttonRunReport;
	}

	public WebElement getLabelStartDate() {
		return labelStartDate;
	}

	public WebElement getLabelEndDate() {
		return labelEndDate;
	}

	@Override
	protected void openPage() {

	}

	public void the_start_dates_should_be_AM() {
		String serviceOrderNumber = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("SERVICE_ORDER_AM_PM");
		// String serviceOrderNumber = "00251935";
		System.out.println("..." + serviceOrderNumber);
		navigateToReportPage();
		navigateToContractStartAndEndDateReport();
		WaitUtils.waitForDisplayed(getLinkEdit());
		getLinkEdit().click();
		WebElement iframe = getDriver().findElement(By.className("reportsReportBuilder"));
		getDriver().switchTo().frame(iframe);
		WaitUtils.waitForDisplayed(getBlockServiceOrderHover());
		Actions action = new Actions(getDriver());
		action.moveToElement(getBlockServiceOrderHover()).build().perform();
		getLinkFilterEdit().click();
		WaitUtils.waitForDisplayed(getInpuptServiceOrderNumber());
		getInpuptServiceOrderNumber().clear();
		getInpuptServiceOrderNumber().sendKeys(serviceOrderNumber);
		getButtonEditOk().click();
		SCHUtils.pause(3);
		getButtonRunReport().click();
		SCHUtils.waitForAjaxToComplete();
		getDriver().switchTo().defaultContent();
		WaitUtils.waitForDisplayed(getLabelStartDate());
		String startDateTime = getLabelStartDate().getText();
		System.err.println("start Date :: " + startDateTime);
		AssertUtils.assertDisplayed(getLabelStartDate());

	}

	public void navigateToContractStartAndEndDateReport() {
		WaitUtils.waitForDisplayed(getLinkAllFolders());
		SCHUtils.clickUsingJavaScript(getLinkAllFolders());
		WaitUtils.waitForDisplayed(getLinkQATestReport());
		SCHUtils.clickUsingJavaScript(getLinkQATestReport());
		WaitUtils.waitForDisplayed(getLinkContractDateReport());
		SCHUtils.clickUsingJavaScript(getLinkContractDateReport());
		SCHUtils.waitForAjaxToComplete();
	}

	public void navigateToReportPage() {
		HomePage homePage = new HomePage();
		WaitUtils.waitForDisplayed(homePage.getAlMenuLink());
		homePage.getAlMenuLink().click();
		WaitUtils.waitForDisplayed(homePage.getHomeLinkReports());
		SCHUtils.clickUsingJavaScript(homePage.getHomeLinkReports());
		SCHUtils.waitForAjaxToComplete();
	}

	public void the_end_date_should_be_PM() {
		WaitUtils.waitForDisplayed(getLabelEndDate());
		AssertUtils.assertDisplayed(getLabelEndDate());
	}

	public void searchForTerritoryRecordInSalesforce(String recordNumber) {
		navigateToReportPage();
		navigateToDataToS3ForBiTerritoryCheckReport();
		String UCNNumber = null;
		if (recordNumber.equals("first")) {
			UCNNumber = ConstantUtils.FIRST_UCN_TERRITORY;
		} else if (recordNumber.equals("100th")) {
			UCNNumber = ConstantUtils.HUNDRED_UCN_TERRITORY;
		} else if (recordNumber.equals("500th")) {
			UCNNumber = ConstantUtils.FIVE_HUNDRED_UCN_TERRITORY;
		} else if (recordNumber.equals("last")) {
			UCNNumber = ConstantUtils.LAST_UCN_TERRITORY;
		} else {
			Assert.fail("Wrong record number " + recordNumber + " Passed.");
		}

		WaitUtils.waitForDisplayed(getLinkEdit());
		getLinkEdit().click();

		WebElement iframe = getDriver().findElement(By.className("reportsReportBuilder"));

		getDriver().switchTo().frame(iframe);
		WaitUtils.waitForDisplayed(getBlockUCNHover());
		Actions action = new Actions(getDriver());
		action.moveToElement(getBlockUCNHover()).build().perform();
		getLinkFilterEdit().click();
		WaitUtils.waitForDisplayed(getInpuptServiceOrderNumber());
		getInpuptServiceOrderNumber().clear();
		getInpuptServiceOrderNumber().sendKeys(UCNNumber);
		getButtonEditOk().click();
		SCHUtils.pause(3);
		getButtonRunReport().click();
		SCHUtils.waitForAjaxToComplete();
		getDriver().switchTo().defaultContent();
	}

	public void navigateToDataToS3ForBiTerritoryCheckReport() {
		HomePage homePage = new HomePage();
		WaitUtils.waitForDisplayed(homePage.getInputSearchTop());
		homePage.getInputSearchTop().sendKeys(ConstantUtils.DATA_TO_S3_TERRITORY_CHECK_REPORT);
		WebElement dynamicSuggestion = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("REPORT_LINK_SUGGESTION"),
				ConstantUtils.DATA_TO_S3_TERRITORY_CHECK_REPORT);
		WaitUtils.waitForDisplayed(dynamicSuggestion);
		SCHUtils.clickUsingJavaScript(dynamicSuggestion);
		SCHUtils.waitForAjaxToComplete();
		WebElement dynamicReportLink = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("REPORT_LINK_DYNAMIC"),
				ConstantUtils.DATA_TO_S3_TERRITORY_CHECK_REPORT);
		dynamicReportLink.click();
		SCHUtils.waitForAjaxToComplete();
	}

}
