package com.scholastic.salesforce.automation.pages;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.scholastic.salesforce.automation.steps.HomePageStepDefs;
import com.scholastic.salesforce.automation.support.ConstantUtils;
import com.scholastic.salesforce.automation.support.RandomDataUtil;
import com.scholastic.salesforce.automation.support.SCHUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;

public class MDRPage extends BaseTestPage<TestPage> {

	@Override
	protected void openPage() {
	}

	/** Object for sftp file transfer */
	public static Channel channel;
	public static JSch jsch;
	public static Session session;
	public static ChannelShell commandchannel;
	public static InputStream inputStream;
	public static PrintStream SetCommand;
	public static OutputStream outputStream;
	public static InputStream outputstream_from_the_channel = null;
	public static BufferedReader br = null;

	public static final String CHANNEL_TYPE = "shell";
	public static final String STRICT_HOSTKEY_CHECKIN_KEY = "StrictHostKeyChecking";
	public static final String STRICT_HOSTKEY_CHECKIN_VALUE = "no";
	public static final String STATUS_EXECUTE_FILE_TO_UPLOAD = "./src/test/resources/mdr/status.txt";
	public static final String MDR_DEMEGRAPHIC_PATH = "/opt/MDRSFDCDemographicLoad/execution";
	public static final String ERROR_FILE_DIRECTORY = "/opt/MDRSFDCDemographicLoad/BatchRequest";
	public static final String MDR_DEMOGRAPHIC_MONTHLY_JOB = "MDRSFDCDemographIntegrationMonthlyLoad";
	public static final String MDR_DEMOGRAPHIC_DAILY_JOB = "MDRSFDCDemographicDailyLoad";
	public static final String JBOSS_FUSE_BIN_DIR = "/opt/jboss-fuse-6.3.0.redhat-187/bin";
	public static final String CLIENT_COMMAND = "./client";
	public static final String SHELL_TYPE = "shell";
	public static final String LIST_COMMAND = "list";
	public static final String CAT_ERROR_FILEVIEW = "cat 7513D*.csv";

	public static final String SFTPHOST = "10.45.152.28";
	public static final int SFTPPORT = 22;
	public static final String SFTPUSER = "sunlee";
	public static final String SFTPPASS = "Sun@lee";
	public static ChannelSftp channelSftp = null;

	public static final String ALREADY_EXISTING_ACCOUNT_FILE = "./src/test/resources/mdr/MDRBLDFEEDEXISTINGRECORD.csv";
	public static final String BATCH_ACCOUNT_FILE = "./src/test/resources/mdr/MDRBLDFEEDBATCH.csv";
	public static final String FIELD_VALIDATION_PRE_CONDITION = "./src/test/resources/mdr/MDRBLDFEEDALLFIELDPRECONDITION.csv";
	public static final String FIELD_VALIDATION_UPDATED = "./src/test/resources/mdr/MDRBLDFEEDALLFIELDUPDATEDVALUE.csv";

	public static Session getSession() {
		JSch jsch = new JSch();
		Session session = null;
		try {
			session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
			session.setPassword(SFTPPASS);
			java.util.Properties config = new java.util.Properties();
			config.put(STRICT_HOSTKEY_CHECKIN_KEY, STRICT_HOSTKEY_CHECKIN_VALUE);
			session.setConfig(config);
			session.connect(120000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return session;
	}

	public void monthlyAccountFileIsProcessed() {
		EditCSVFileAndZIP();
		UploadCreatedZIPFile();
		UploadFileToSFTPWithExecuteStatus();
		StartCommandMonthlyDemographicJob();
	}

	public void StartCommandMonthlyDemographicJob() {
		try {
			String number = null;
			session = getSession();
			commandchannel = (ChannelShell) session.openChannel(SHELL_TYPE);
			commandchannel.setAgentForwarding(true);
			outputStream = commandchannel.getOutputStream();
			SetCommand = new PrintStream(outputStream, true);
			commandchannel.connect();
			inputStream = commandchannel.getInputStream();
			SetCommand.println("cd " + JBOSS_FUSE_BIN_DIR);
			SetCommand.println(CLIENT_COMMAND);
			SCHUtils.pause(15);
			SetCommand.println(LIST_COMMAND);
			outputstream_from_the_channel = commandchannel.getInputStream();
			br = new BufferedReader(new InputStreamReader(outputstream_from_the_channel));
			String line;
			while ((line = br.readLine()) != null) {
				if (line.contains(MDR_DEMOGRAPHIC_MONTHLY_JOB)) {
					number = line.substring(line.indexOf("[") + 1, line.indexOf("]"));
					System.out.println("Job Number :: " + number);
					break;
				} else if (line.contains("JBossFuse")) {
					break;
				}
			}
			if (number.equals("") || number.equals(null)) {
				Assert.fail("Demo Graphic Monthly Job not Found/Present.");
			} else {
				SetCommand.println("stop " + number);
				SCHUtils.pause(30);
				SCHUtils.setEstTimeForStartCommand();
				SetCommand.println("start " + number);
				SCHUtils.pause(2);
			}
			cleanUpSFTP();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		try {
			outputstream_from_the_channel.close();
			br.close();
			commandchannel.disconnect();
			SetCommand.close();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void UploadFileToSFTPWithExecuteStatus() {
		try {
			session = getSession();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(MDR_DEMEGRAPHIC_PATH);
			File f = new File(STATUS_EXECUTE_FILE_TO_UPLOAD);
			channelSftp.put(new FileInputStream(f), f.getName(), ChannelSftp.OVERWRITE);
			System.out.println("Status File Override done");
		} catch (JSchException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SftpException e) {
			e.printStackTrace();
		}
		try {
			channelSftp.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		cleanUpSFTP();
	}

	public void cleanUpSFTP() {
		if (channel != null) {
			channel.disconnect();
		}
		if (session != null) {
			session.disconnect();
		}
		channel = null;
		session = null;
	}

	public static void printDataAfterCommand(ChannelShell channelShell) throws IOException {
		InputStream outputstream_from_the_channel = channelShell.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(outputstream_from_the_channel));
		String line;
		while ((line = br.readLine()) != null)
			System.out.print(line + "\n");
	}

	public static void uploadFile(String fileName) {
		File file = new File(fileName);
		try (FileInputStream finput = new FileInputStream(file)) {
			channelSftp.put(finput, file.getName());
		} catch (IOException | SftpException e) {
			e.printStackTrace();
		}
	}

	public static void UploadCreatedZIPFile() {
		String server = "egcrmwebdev1.corp.scholasticinc.local";
		int port = 21;
		String user = "egcrmappftp";
		String pass = "Temp_123";
		String ZIPFILELocation = "./src/test/resources/mdr/MDRBLDFEED.zip";
		String firstRemoteFile = "/opt/ftpdepot/MDRFEED/MDRBLDFEED.zip";

		FTPClient ftpClient = new FTPClient();
		try {
			ftpClient.connect(server, port);
			ftpClient.login(user, pass);
			ftpClient.enterLocalPassiveMode();

			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			File firstLocalFile = new File(ZIPFILELocation);

			InputStream inputStream = new FileInputStream(firstLocalFile);

			System.out.println("Start uploading ZIP file");
			boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
			inputStream.close();
			if (done) {
				System.out.println("The zip file is uploaded successfully.");
			} else {
				Assert.fail("Zip file is not uploaded successfully on FTP Server.");
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public static void createZIPFile(String filePath) {
		byte[] buffer = new byte[1024];
		String ZIPFile = "./src/test/resources/mdr/MDRBLDFEED.zip";
		try {
			FileOutputStream fos = new FileOutputStream(ZIPFile);
			ZipOutputStream zos = new ZipOutputStream(fos);
			ZipEntry ze = new ZipEntry("MDRBLDFEED.csv");
			zos.putNextEntry(ze);
			FileInputStream in = new FileInputStream(filePath);

			int len;
			while ((len = in.read(buffer)) > 0) {
				zos.write(buffer, 0, len);
			}

			in.close();
			zos.closeEntry();
			zos.close();
			System.out.println("ZIP File Created");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void EditCSVFileAndZIP() {
		String CSVUPDATE = "./src/test/resources/mdr/MDRBLDFEED.csv";
		String CSVNEW = "./src/test/resources/mdr/MDRBLDFEED1.csv";
		CSVReader reader = null;
		CSVWriter writer = null;
		try {
			reader = new CSVReader(new FileReader(CSVUPDATE));
			writer = new CSVWriter(new FileWriter(CSVNEW));
			String[] nextLine = null;
			while ((nextLine = reader.readNext()) != null) {
				nextLine[35] = String.valueOf(generateRandomSchool(nextLine[35]));
				writer.writeNext(nextLine);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			reader.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		createZIPFile(CSVNEW);
	}

	public static int generateRandomSchool(String string) {
		int Min = 0;
		int Max = 99;
		int randomNum = 0;
		do {
			randomNum = (int) (Math.random() * (Max - Min));
		} while (randomNum == Integer.parseInt(string));
		System.out.println("Random Updated School Number :: " + randomNum);
		return randomNum;
	}

	public void existingMonthlyAccountFileIsProcessed() {
		createZIPFile(ALREADY_EXISTING_ACCOUNT_FILE);
		UploadCreatedZIPFile();
		UploadFileToSFTPWithExecuteStatus();
		StartCommandMonthlyDemographicJob();
	}

	public void fileContainsAnEntryThatAlreadyExissInSalesforce() {
		File file = new File(ALREADY_EXISTING_ACCOUNT_FILE);
		if (!file.exists()) {
			Assert.fail("Existing account file is not present.");
		}
	}

	public void a_MDR_Monthly_Account_file_is_received_that_contains_more_records_than_the_batch_size() {
		File file = new File(BATCH_ACCOUNT_FILE);
		if (!file.exists()) {
			Assert.fail("MDR Monthly file for Batch processing is not present.");
		}

		// setting pre condition for test case in SFDC
		HomePageStepDefs homeStep = new HomePageStepDefs();
		homeStep.settingPreConditionForBatchProcessing();
	}

	public void theMonthlyRoleFileIsProcessed() {
		createZIPFile(BATCH_ACCOUNT_FILE);
		UploadCreatedZIPFile();
		UploadFileToSFTPWithExecuteStatus();
		StartCommandMonthlyDemographicJob();
	}

	public void monthlyAccountErrorIsLoggedInSeparateFile() {
		EditMDRIDCSVAndZipForGeneratingError();
		UploadCreatedZIPFile();
		UploadFileToSFTPWithExecuteStatus();
		StartCommandMonthlyDemographicJob();

		verifyScenarioProcessingFailed();
		updateMDRIDinSFDC();
	}

	public void verifyScenarioProcessingFailed() {
		HomePageStepDefs homePageStepDefs = new HomePageStepDefs();
		homePageStepDefs.userLoginInToSystem();
		HomePage homePage = new HomePage();
		homePage.navigateToSetUpMenu();
		ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tabs2.get(1));
		homePage.navigateToBulkDataLoadJobs();
		homePage.verifyProcessingFailed();
		getDriver().close();
		getDriver().switchTo().window(tabs2.get(0));
	}

	public void updateMDRIDinSFDC() {
		AccountsPage accountsPage = new AccountsPage();
		accountsPage.navigatesToAccountsMenu();
		accountsPage.searchForAccount(ConstantUtils.ACCOUNT_ERROR_PROCESSING_EDIT_MDR_ID);
		accountsPage.navigateToAccount(ConstantUtils.ACCOUNT_ERROR_PROCESSING_EDIT_MDR_ID);
		String errorMDRID = (String) TestBaseProvider.getTestBase().getContext().getProperty("RANDOM_MDR_ID");
		FieldServiceContractPage.clickOnEditButton("MDR ID");
		FieldServiceContractPage fieldServicePage = new FieldServiceContractPage();
		fieldServicePage.enterUpdatedValueIntoLineQuantityAndSave("MDR ID", errorMDRID);
		WebElement MDRIDElement = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "MDR ID");
		System.out.println("MDR ID After Edit :: " + MDRIDElement.getText());
	}

	public void EditMDRIDCSVAndZipForGeneratingError() {
		String CSVERROR = "./src/test/resources/mdr/MDRBLDFEEDERROR.csv";
		String CSVNEW = "./src/test/resources/mdr/MDRBLDFEED1.csv";
		CSVReader reader = null;
		CSVWriter writer = null;
		try {
			reader = new CSVReader(new FileReader(CSVERROR));
			writer = new CSVWriter(new FileWriter(CSVNEW));
			String[] nextLine = null;
			String randomMDRID = RandomDataUtil.getRandomMDRID();
			System.out.println("Random MDR ID Generate :: " + randomMDRID);
			TestBaseProvider.getTestBase().getContext().setProperty("RANDOM_MDR_ID", randomMDRID);
			while ((nextLine = reader.readNext()) != null) {
				nextLine[1] = randomMDRID;
				nextLine[35] = String.valueOf(generateRandomSchool(nextLine[35]));
				TestBaseProvider.getTestBase().getContext().setProperty("UPDATED_SCHOOL_FOR_ERROR", nextLine[35]);
				writer.writeNext(nextLine);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			reader.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		createZIPFile(CSVNEW);
	}

	public void theDailyErrorAccountFileIsProcessed() {
		try {
			String number = null;
			Session session = getSession();
			commandchannel = (ChannelShell) session.openChannel(SHELL_TYPE);
			commandchannel.setAgentForwarding(true);
			outputStream = commandchannel.getOutputStream();
			SetCommand = new PrintStream(outputStream, true);
			commandchannel.connect();
			inputStream = commandchannel.getInputStream();
			SetCommand.println("cd " + JBOSS_FUSE_BIN_DIR);
			SetCommand.println(CLIENT_COMMAND);
			SCHUtils.pause(15);
			SetCommand.println(LIST_COMMAND);
			outputstream_from_the_channel = commandchannel.getInputStream();
			br = new BufferedReader(new InputStreamReader(outputstream_from_the_channel));
			String line;
			while ((line = br.readLine()) != null) {
				if (line.contains(MDR_DEMOGRAPHIC_DAILY_JOB)) {
					number = line.substring(line.indexOf("[") + 1, line.indexOf("]"));
					System.out.println("Daily Job Number :: " + number);
					break;
				} else if (line.contains("JBossFuse")) {
					break;
				}
			}
			if (number.equals("") || number.equals(null)) {
				Assert.fail("Demo Graphic Monthly Job not Present.");
			} else {
				SetCommand.println("stop " + number);
				SCHUtils.pause(30);
				SCHUtils.setEstTimeForStartCommand();
				SetCommand.println("start " + number);
				SCHUtils.pause(2);
			}
			cleanUpSFTP();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			outputstream_from_the_channel.close();
			br.close();
			commandchannel.disconnect();
			SetCommand.close();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void a_MDR_Monthly_Account_error_file_exist_with_failed_error_entries() {
		EditMDRIDCSVAndZipForGeneratingError();
		UploadCreatedZIPFile();
		UploadFileToSFTPWithExecuteStatus();
		StartCommandMonthlyDemographicJob();
	}

	public void a_new_MDR_Monthly_Account_file_is_processed() {
		EditMDRIDCSVAndZipForGeneratingError();
		UploadCreatedZIPFile();
		UploadFileToSFTPWithExecuteStatus();
		StartCommandMonthlyDemographicJob();
	}

	public void it_contains_an_error() {
		StartCommandMonthlyDemographicJob();
	}

	public void the_new_error_should_be_the_only_error_in_the_error_file() {
		ErrorFileContainNewErrorInCSVFile();
	}

	public void ErrorFileContainNewErrorInCSVFile() {
		try {
			boolean bool = false;
			session = getSession();
			commandchannel = (ChannelShell) session.openChannel(SHELL_TYPE);
			commandchannel.setAgentForwarding(true);
			outputStream = commandchannel.getOutputStream();
			SetCommand = new PrintStream(outputStream, true);
			commandchannel.connect();
			inputStream = commandchannel.getInputStream();
			SetCommand.println("cd " + ERROR_FILE_DIRECTORY);
			SCHUtils.pause(5);
			SetCommand.println(CAT_ERROR_FILEVIEW);
			outputstream_from_the_channel = commandchannel.getInputStream();
			br = new BufferedReader(new InputStreamReader(outputstream_from_the_channel));
			String line;
			String latestErrorMDRID = (String) TestBaseProvider.getTestBase().getContext().getProperty("RANDOM_MDR_ID");
			while ((line = br.readLine()) != null) {
				if (line.contains(latestErrorMDRID)) {
					bool = true;
					break;
				} else if (line.contains("BatchRequest")) {
					break;
				}
			}
			if (bool) {
				System.out.println("Pass");
			} else {
				Assert.fail("New Error is not replaced in error csv file.");
			}
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		try {
			if (channel != null) {
				channel.disconnect();
			}
			if (session != null) {
				session.disconnect();
			}
			channel = null;
			session = null;
			outputstream_from_the_channel.close();
			br.close();
			commandchannel.disconnect();
			SetCommand.close();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void a_MDR_Monthly_Account_File_is_contains_all_the_fields() {
		File file = new File(FIELD_VALIDATION_UPDATED);
		if (!file.exists()) {
			Assert.fail("Updated field validation file is not present.");
		}
	}

	public void the_MDR_Monthly_Account_file_is_processed_with_all_thes_fields() {
		createZIPFile(FIELD_VALIDATION_UPDATED);
		UploadCreatedZIPFile();
		UploadFileToSFTPWithExecuteStatus();
		StartCommandMonthlyDemographicJob();
	}

	public void processedPreconditionFileForFieldService() {
		createZIPFile(FIELD_VALIDATION_PRE_CONDITION);
		UploadCreatedZIPFile();
		UploadFileToSFTPWithExecuteStatus();
		StartCommandMonthlyDemographicJob();

	}
}
