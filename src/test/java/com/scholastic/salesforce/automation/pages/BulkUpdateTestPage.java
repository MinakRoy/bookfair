package com.scholastic.salesforce.automation.pages;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.scholastic.salesforce.automation.support.ConstantUtils;
import com.scholastic.salesforce.automation.support.RandomDataUtil;
import com.scholastic.salesforce.automation.support.SCHUtils;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

public class BulkUpdateTestPage extends BaseTestPage<HomePage> {

	@FindBy(locator = "BUP_DD_ALL_STATUS")
	private WebElement ddAllStatus;
	@FindBy(locator = "BUP_INPUT_ACCOUNT")
	private WebElement inputAccount;
	@FindBy(locator = "BUP_BUTTON_SEARCH")
	private WebElement buttonSearch;
	@FindBy(locator = "BUP_INPUT_SEARCH_CONSULTANT")
	private WebElement inputSearchConsultant;
	@FindBy(locator = "BUP_LINK_LIST_SUGGESTED_CONSULTANT")
	private List<WebElement> linkSuggestedConsultatnt;
	@FindBy(locator = "BUP_LINK_LIST_SERVICE_NUMBER")
	private List<WebElement> linkSearviceNumber;
	@FindBy(locator = "BUP_LINK_LIST_STATUS")
	private List<WebElement> linkStatus;
	@FindBy(locator = "BUP_LINK_LIST_SEARCH_CONSULTANT")
	private List<WebElement> linkSearchConsultant;
	@FindBy(locator = "BUP_SELECT_STATUS_DYNAMIC")
	private WebElement selectStatusDynamic;
	@FindBy(locator = "BUP_INPUT_LIST_SERVICE_DATE")
	private List<WebElement> inputListServiceDate;
	@FindBy(locator = "BUP_BUTTON_SAVE")
	private WebElement buttonSave;
	@FindBy(locator = "BUP_LABLE_SUCCESSFULLY_UPDATE")
	private WebElement labelSuccessfullyUpdated;
	@FindBy(locator = "BUL_LABEL_SELECTED_STATUS")
	private List<WebElement> labelSelectedStatus;
	@FindBy(locator = "BUL_LABLE_LIST_ERROR")
	private List<WebElement> labelError;
	@FindBy(locator = "BUP_INPUT_LIST_CONSULTANT")
	private List<WebElement> inputListConsultant;
	@FindBy(locator = "BUP_LINK_PROVIDER_GROUP")
	private List<WebElement> linkProviderGroup;
	@FindBy(locator = "BUP_OPTION_STATUS")
	private List<WebElement> listOptionStatus;

	public List<WebElement> getListOptionStatus() {
		return listOptionStatus;
	}

	public List<WebElement> getLinkProviderGroup() {
		return linkProviderGroup;
	}

	public List<WebElement> getInputListConsultant() {
		return inputListConsultant;
	}

	public List<WebElement> getLabelError() {
		return labelError;
	}

	public List<WebElement> getLabelSelectedStatus() {
		return labelSelectedStatus;
	}

	public WebElement getLabelSuccessfullyUpdated() {
		return labelSuccessfullyUpdated;
	}

	public WebElement getButtonSave() {
		return buttonSave;
	}

	public List<WebElement> getInputListServiceDate() {
		return inputListServiceDate;
	}

	public WebElement getSelectStatusDynamic() {
		return selectStatusDynamic;
	}

	public WebElement getDdAllStatus() {
		return ddAllStatus;
	}

	public WebElement getInputAccount() {
		return inputAccount;
	}

	public WebElement getButtonSearch() {
		return buttonSearch;
	}

	public WebElement getInputSearchConsultant() {
		return inputSearchConsultant;
	}

	public List<WebElement> getLinkSuggestedConsultatnt() {
		return linkSuggestedConsultatnt;
	}

	public List<WebElement> getLinkSearviceNumber() {
		return linkSearviceNumber;
	}

	public List<WebElement> getLinkStatus() {
		return linkStatus;
	}

	public List<WebElement> getLinkSearchConsultant() {
		return linkSearchConsultant;
	}

	@Override
	protected void openPage() {
	}

	/**
	 * 
	 * @param assignCount
	 * 
	 */

	public void schedulerAssignFieldServicesToAConsultant(String assignCount) {
		if (assignCount.equals("1") || assignCount.equals("2") || assignCount.equals("6") || assignCount.equals("10")) {
			FieldServiceContractPage fieldServiceContractPage = new FieldServiceContractPage();
			fieldServiceContractPage.createContractWith("bulk update");
		}
		navigateToBulkUpdateTestPage();
		WaitUtils.waitForDisplayed(getSelectStatusDynamic());
		getSelectStatusDynamic().sendKeys(ConstantUtils.TEXT_OPEN_ORDER);
		getInputAccount().sendKeys(ConstantUtils.BULKUPDATE_ACCOUNT_NAME);
		SCHUtils.clickUsingJavaScript(getButtonSearch());
		SCHUtils.waitForAjaxToComplete();
		int size = Integer.parseInt(assignCount);
		for (int i = 0; i < size; i++) {
			String dateToSent = FieldServiceContractPage.getDate(String.valueOf(0 - i));
			getLinkStatus().get(i).sendKeys(ConstantUtils.ASSIGNED_STATUS);
			SCHUtils.clickUsingJavaScript(getLinkSearchConsultant().get(i));
			getInputSearchConsultant().sendKeys("a");
			SCHUtils.pause(7);
			int sizeOfSuggestedConsultant = getLinkSuggestedConsultatnt().size();
			int randomConsultantFromSuggestion = RandomDataUtil.getRandomSuggestedConsultant(sizeOfSuggestedConsultant);
			SCHUtils.clickUsingJavaScript(getLinkSuggestedConsultatnt().get(randomConsultantFromSuggestion));
			getInputListServiceDate().get(i).sendKeys(dateToSent);
			getInputListServiceDate().get(i).sendKeys(Keys.TAB);
			SCHUtils.pause(8);

			boolean isAlertPresent = isAlertPresent();
			if (isAlertPresent) {
				getDriver().switchTo().alert().accept();
				System.out.println("Alert Handled.");
			}
		}
		SCHUtils.clickUsingJavaScript(getButtonSave());
	}

	/**
	 * 
	 * @return alert is present or not
	 */

	public boolean isAlertPresent() {
		try {
			getDriver().switchTo().alert();
			return true;
		} catch (NoAlertPresentException ex) {
			return false;
		}
	}

	/**
	 * method to navigate to Bulk Update Test Page
	 */

	public void navigateToBulkUpdateTestPage() {
		HomePage homePage = new HomePage();
		WaitUtils.waitForDisplayed(homePage.getAlMenuLink());
		homePage.getAlMenuLink().click();
		WaitUtils.waitForDisplayed(homePage.getHomeLinkBulkUpdate());
		SCHUtils.clickUsingJavaScript(homePage.getHomeLinkBulkUpdate());
		SCHUtils.waitForAjaxToComplete();
	}

	/**
	 * 
	 * @param assignCount
	 */

	public void consultantShouldBeBookForAssignedServices(String assignCount) {
		WaitUtils.waitForDisplayed(getLabelSuccessfullyUpdated());
		AssertUtils.assertTextMatches(getLabelSuccessfullyUpdated(),
				Matchers.equalTo(ConstantUtils.SUCCESS_UPDATED_MESSAGE));
		int size = Integer.parseInt(assignCount);
		for (int i = 0; i < size; i++) {
			SCHUtils.clickUsingJavaScript(getLinkSearviceNumber().get(i));
			ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(tabs2.get(1));
			WebElement statusElement = SCHUtils
					.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "Status");
			WaitUtils.waitForDisplayed(statusElement);
			AssertUtils.assertTextMatches(statusElement, Matchers.equalTo(ConstantUtils.ASSIGNED_STATUS));
			getDriver().close();
			getDriver().switchTo().window(tabs2.get(0));
		}
	}

	/**
	 * 
	 * @param deliverCount
	 */

	public void schdulerDeliversFieldServicesForConsultant(String deliverCount) {
		navigateToBulkUpdateTestPage();
		WaitUtils.waitForDisplayed(getSelectStatusDynamic());
		getSelectStatusDynamic().sendKeys(ConstantUtils.ASSIGNED_STATUS);
		getInputAccount().sendKeys(ConstantUtils.BULKUPDATE_ACCOUNT_NAME);
		SCHUtils.clickUsingJavaScript(getButtonSearch());
		SCHUtils.waitForAjaxToComplete();
		int size = Integer.parseInt(deliverCount);

		int assignedSize = getLinkSearviceNumber().size();
		System.out.println("size :: " + size + "  ..assiigned Size :: " + assignedSize);
		if (size > assignedSize) {
			System.out.println("Assigned Order Are less so assigning order first");
			schedulerAssignFieldServicesToAConsultant(String.valueOf(size));
		}

		for (int i = 0; i < size; i++) {
			String dateToSent = FieldServiceContractPage.getDate(String.valueOf("0"));
			System.out.println("Today Date :: " + dateToSent);
			getLinkStatus().get(i).sendKeys(ConstantUtils.DELIVERED_STATUS);
			getInputListServiceDate().get(i).clear();
			getInputListServiceDate().get(i).sendKeys(dateToSent);
			getInputListServiceDate().get(i).sendKeys(Keys.TAB);
			SCHUtils.pause(8);

			boolean isAlertPresent = isAlertPresent();
			if (isAlertPresent) {
				getDriver().switchTo().alert().accept();
				System.out.println("Alert Handled.");
			}
		}
		SCHUtils.clickUsingJavaScript(getButtonSave());
	}

	/**
	 * 
	 * @param deliverCount
	 */

	public void fieldServicesShouldBeMarkedAsDelivered(String deliverCount) {
		WaitUtils.waitForDisplayed(getLabelSuccessfullyUpdated());
		AssertUtils.assertTextMatches(getLabelSuccessfullyUpdated(),
				Matchers.equalTo(ConstantUtils.SUCCESS_UPDATED_MESSAGE));
		int size = Integer.parseInt(deliverCount);
		for (int i = 0; i < size; i++) {
			SCHUtils.clickUsingJavaScript(getLinkSearviceNumber().get(i));
			ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(tabs2.get(1));
			WebElement statusElement = SCHUtils
					.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "Status");
			WaitUtils.waitForDisplayed(statusElement);
			AssertUtils.assertTextMatches(statusElement, Matchers.equalTo(ConstantUtils.DELIVERED_STATUS));
			getDriver().close();
			getDriver().switchTo().window(tabs2.get(0));
		}
	}

	/**
	 * 
	 * @param deliverCount
	 */

	public void the_scheduler_delivers_field_services_for_the_consultant_bulk_update(String deliverCount) {
		int size = Integer.parseInt(deliverCount);
		for (int i = 0; i < size; i++) {
			String dateToSent = FieldServiceContractPage.getDate(String.valueOf("0"));
			getLinkStatus().get(i).sendKeys("Delivered");
			getInputListServiceDate().get(i).clear();
			getInputListServiceDate().get(i).sendKeys(dateToSent);
			getInputListServiceDate().get(i).sendKeys(Keys.TAB);
			SCHUtils.pause(8);
			boolean isAlertPresent = isAlertPresent();
			if (isAlertPresent) {
				getDriver().switchTo().alert().accept();
				System.out.println("Alert Handled.");
			}
		}
		SCHUtils.clickUsingJavaScript(getButtonSave());
	}

	/**
	 * 
	 * @param assignCount
	 */

	public void the_scheduler_assigns_field_services_to_a_consultant_bulk_update(String assignCount) {
		WaitUtils.waitForDisplayed(getLabelSuccessfullyUpdated());
		AssertUtils.assertTextMatches(getLabelSuccessfullyUpdated(),
				Matchers.equalTo(ConstantUtils.SUCCESS_UPDATED_MESSAGE));
		int startCountForAssigned = 0;
		int serviceCount = getLabelSelectedStatus().size();
		for (int i = 0; i < serviceCount; i++) {
			System.out.println("getText :: " + getLabelSelectedStatus().get(i).getText());
			if (!getLabelSelectedStatus().get(i).getText().equals(ConstantUtils.TEXT_OPEN_ORDER)) {
				startCountForAssigned++;
			} else {
				break;
			}
		}
		int actualAssignedCount = 0;
		for (int i = 0; i < startCountForAssigned; i++) {
			SCHUtils.clickUsingJavaScript(getLinkSearviceNumber().get(i));
			ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(tabs2.get(1));
			WebElement statusElement = SCHUtils
					.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "Status");
			WaitUtils.waitForDisplayed(statusElement);
			String status = statusElement.getText();
			if (status.equals(ConstantUtils.ASSIGNED_STATUS)) {
				actualAssignedCount++;
			}
			getDriver().close();
			getDriver().switchTo().window(tabs2.get(0));
		}
		Assert.assertEquals(actualAssignedCount, Integer.parseInt(assignCount));
	}

	public void a_consultant_is_already_booked_for_a_specific_date() {
		navigateToBulkUpdateTestPage();
		WaitUtils.waitForDisplayed(getSelectStatusDynamic());
		getSelectStatusDynamic().sendKeys(ConstantUtils.TEXT_OPEN_ORDER);
		getInputAccount().sendKeys(ConstantUtils.BULKUPDATE_ACCOUNT_NAME);
		SCHUtils.clickUsingJavaScript(getButtonSearch());
		SCHUtils.waitForAjaxToComplete();
		for (int i = 0; i < 1; i++) {
			String dateToSent = FieldServiceContractPage.getDate("0");
			getLinkStatus().get(i).sendKeys(ConstantUtils.ASSIGNED_STATUS);
			SCHUtils.clickUsingJavaScript(getLinkSearchConsultant().get(i));
			getInputSearchConsultant().sendKeys(ConstantUtils.ASSIGNED_CONSULTANT);
			SCHUtils.pause(7);
			SCHUtils.clickUsingJavaScript(getLinkSuggestedConsultatnt().get(0));
			getInputListServiceDate().get(i).sendKeys(dateToSent);
			getInputListServiceDate().get(i).sendKeys(Keys.TAB);
			SCHUtils.pause(8);
			boolean isAlertPresent = isAlertPresent();
			if (isAlertPresent) {
				getDriver().switchTo().alert().accept();
				System.out.println("Alert Handled.");
			}
		}
		SCHUtils.clickUsingJavaScript(getButtonSave());
	}

	public void the_scheduler_schedules_the_same_consultant_for_another_booking_for_the_same_date_by_bulk_update() {
		navigateToBulkUpdateTestPage();
		WaitUtils.waitForDisplayed(getSelectStatusDynamic());
		getSelectStatusDynamic().sendKeys(ConstantUtils.TEXT_OPEN_ORDER);
		getInputAccount().sendKeys(ConstantUtils.BULKUPDATE_ACCOUNT_NAME);
		SCHUtils.clickUsingJavaScript(getButtonSearch());
		SCHUtils.waitForAjaxToComplete();
		for (int i = 0; i < 1; i++) {
			String dateToSent = FieldServiceContractPage.getDate("0");
			getLinkStatus().get(i).sendKeys(ConstantUtils.ASSIGNED_STATUS);
			SCHUtils.clickUsingJavaScript(getLinkSearchConsultant().get(i));
			getInputSearchConsultant().sendKeys(ConstantUtils.ASSIGNED_CONSULTANT);
			SCHUtils.pause(7);
			SCHUtils.clickUsingJavaScript(getLinkSuggestedConsultatnt().get(0));
			getInputListServiceDate().get(i).sendKeys(dateToSent);
			getInputListServiceDate().get(i).sendKeys(Keys.TAB);
			SCHUtils.pause(8);
		}
	}

	public void the_scheduler_should_be_prompted_that_the_consultant_is_over_booked() {
		boolean isAlertPresent = isAlertPresent();
		Assert.assertEquals(true, isAlertPresent, "Scheduler Should be Prompt with Alert for Over Booked.");
	}

	public void the_scheduler_should_be_able_to_bypass_the_prompt() {
		boolean isAlertPresent = isAlertPresent();
		if (isAlertPresent) {
			getDriver().switchTo().alert().accept();
			isAlertPresent = isAlertPresent();
			Assert.assertEquals(false, isAlertPresent, "Scheduler should be able to bypass the prompt.");
		}
		SCHUtils.clickUsingJavaScript(getButtonSave());
	}

	public void the_schedule_should_be_able_to_book_the_service_to_the_consultant() {
		WaitUtils.waitForDisplayed(getLabelSuccessfullyUpdated());
		AssertUtils.assertTextMatches(getLabelSuccessfullyUpdated(),
				Matchers.equalTo(ConstantUtils.SUCCESS_UPDATED_MESSAGE));
		SCHUtils.clickUsingJavaScript(getLinkSearviceNumber().get(0));
		ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tabs2.get(1));
		WebElement statusElement = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "Status");
		WaitUtils.waitForDisplayed(statusElement);
		AssertUtils.assertTextMatches(statusElement, Matchers.equalTo(ConstantUtils.ASSIGNED_STATUS));
		getDriver().close();
		getDriver().switchTo().window(tabs2.get(0));
	}

	/**
	 * method is create two different field service contract with same account
	 * as pre condition
	 */

	public void the_scheduler_schedules_fields_services_from_different_contracts_to_consultants() {

		FieldServiceContractPage fieldServiceContractPage = new FieldServiceContractPage();
		fieldServiceContractPage.createContractWith("bulk update with multiple account");
		fieldServiceContractPage.createContractWith("bulk update with multiple account");

		// search for open and account
		navigateToBulkUpdateTestPage();
		WaitUtils.waitForDisplayed(getSelectStatusDynamic());
		getSelectStatusDynamic().sendKeys(ConstantUtils.TEXT_OPEN_ORDER);
		getInputAccount().sendKeys(ConstantUtils.BULKUPDATE_MULTIPLE_ACCOUNT_NAME);
		SCHUtils.clickUsingJavaScript(getButtonSearch());
		SCHUtils.waitForAjaxToComplete();
		int size = getLinkSearviceNumber().size();
		for (int i = 0; i < size; i++) {
			String dateToSent = FieldServiceContractPage.getDate(String.valueOf(0 - i));
			getLinkStatus().get(i).sendKeys(ConstantUtils.ASSIGNED_STATUS);
			SCHUtils.clickUsingJavaScript(getLinkSearchConsultant().get(i));
			getInputSearchConsultant().sendKeys("a");
			SCHUtils.pause(7);
			int sizeOfSuggestedConsultant = getLinkSuggestedConsultatnt().size();
			int randomConsultantFromSuggestion = RandomDataUtil.getRandomSuggestedConsultant(sizeOfSuggestedConsultant);
			SCHUtils.clickUsingJavaScript(getLinkSuggestedConsultatnt().get(randomConsultantFromSuggestion));
			getInputListServiceDate().get(i).sendKeys(dateToSent);
			getInputListServiceDate().get(i).sendKeys(Keys.TAB);
			SCHUtils.pause(8);

			boolean isAlertPresent = isAlertPresent();
			if (isAlertPresent) {
				getDriver().switchTo().alert().accept();
				System.out.println("Alert Handled.");
			}
		}
		SCHUtils.clickUsingJavaScript(getButtonSave());
	}

	public void the_consultant_should_be_booked_for_the_assigned_services() {
		WaitUtils.waitForDisplayed(getLabelSuccessfullyUpdated());
		AssertUtils.assertTextMatches(getLabelSuccessfullyUpdated(),
				Matchers.equalTo(ConstantUtils.SUCCESS_UPDATED_MESSAGE));
		int size = getLinkSearviceNumber().size();
		for (int i = 0; i < size; i++) {
			SCHUtils.clickUsingJavaScript(getLinkSearviceNumber().get(i));
			ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(tabs2.get(1));
			WebElement statusElement = SCHUtils
					.waitForPresent(TestBaseProvider.getTestBase().getString("ACCOUNT_LABEL_DYNAMIC_FIELD"), "Status");
			WaitUtils.waitForDisplayed(statusElement);
			AssertUtils.assertTextMatches(statusElement, Matchers.equalTo(ConstantUtils.ASSIGNED_STATUS));
			getDriver().close();
			getDriver().switchTo().window(tabs2.get(0));
		}
	}

	/**
	 * method trying to assign multiple services without required details
	 */

	public void the_scheduler_schedules_multiple_field_services_without_all_requirements() {
		navigateToBulkUpdateTestPage();
		WaitUtils.waitForDisplayed(getSelectStatusDynamic());
		getSelectStatusDynamic().sendKeys(ConstantUtils.TEXT_OPEN_ORDER);
		getInputAccount().sendKeys(ConstantUtils.BULKUPDATE_ACCOUNT_NAME);
		SCHUtils.clickUsingJavaScript(getButtonSearch());
		SCHUtils.waitForAjaxToComplete();
		for (int i = 0; i < 3; i++) {
			getLinkStatus().get(i).sendKeys(ConstantUtils.ASSIGNED_STATUS);
		}
		SCHUtils.clickUsingJavaScript(getButtonSave());
	}

	/**
	 * verification step is pending to verify as multiple error not visible,
	 * because of future enhancement
	 */

	public void the_individual_error_should_be_viewable_by_the_user() {
		int size = getLabelError().size();
		Assert.assertEquals(size, "3", "Individual Error is not viewed.");
	}

	public void the_user_navigates_to_a_service_that_has_a_status(String serviceStatus) {
		navigateToBulkUpdateTestPage();
		WaitUtils.waitForDisplayed(getSelectStatusDynamic());
		getSelectStatusDynamic().click();
		SCHUtils.pause(3);
		int dropdownSize = getListOptionStatus().size();
		for (int i = 0; i < dropdownSize; i++) {
			String dropdownText = getListOptionStatus().get(i).getText();
			if (dropdownText.equals(serviceStatus)) {
				getListOptionStatus().get(i).click();
				break;
			}
		}
		getInputAccount().click();
		SCHUtils.clickUsingJavaScript(getButtonSearch());
		SCHUtils.waitForAjaxToComplete();
	}

	public void the_user_should_not_be_able_to_edit_the_field_on_the_bulk_update_page(String serviceStatus) {
		int serviceStatusSize = getLinkSearviceNumber().size();
		int size = (serviceStatusSize > 3) ? 3 : serviceStatusSize;
		String disabledText = null;
		if (size <= 0) {
			Assert.fail("There are no services in " + serviceStatus + ".");
		} else {
			for (int i = 0; i < size; i++) {
				if (serviceStatus.equals("Status")) {
					disabledText = getLinkStatus().get(i).getAttribute("disabled");
					Assert.assertEquals(disabledText, "true", "Status is not disabled.");
				} else if (serviceStatus.equals("Service Date")) {
					disabledText = getInputListServiceDate().get(i).getAttribute("disabled");
					Assert.assertEquals(disabledText, "true", "Service date is not disabled.");
				} else if (serviceStatus.equals("Consultant")) {
					disabledText = getInputListConsultant().get(i).getAttribute("disabled");
					Assert.assertEquals(disabledText, "true", "Consultant is not disabled.");
				} else if (serviceStatus.equals("Provider Group")) {
					disabledText = getLinkProviderGroup().get(i).getAttribute("disabled");
					Assert.assertEquals(disabledText, "true", "Provider Group is not disabled.");
				} else {
					Assert.fail("Wrong " + serviceStatus + " Param Passed.");
				}
			}
		}
	}

	public void the_service_date_is_entered_for_a_field_service_in_bulk_update() {
		schdulerDeliversFieldServicesForConsultant("1");
		fieldServicesShouldBeMarkedAsDelivered("1");
		String deliveredWordOrderNumber = getLinkSearviceNumber().get(0).getText();
		System.out.println("WO Text :: " + deliveredWordOrderNumber);
		TestBaseProvider.getTestBase().getContext().setProperty("SERVICE_ORDER_AM_PM", deliveredWordOrderNumber);
	}

	public void the_start_dates_should_be_AM() {
		ReportPage reportPage = new ReportPage();
		reportPage.the_start_dates_should_be_AM();
	}

	public void the_end_date_should_be_PM() {
		ReportPage reportPage = new ReportPage();
		reportPage.the_end_date_should_be_PM();
	}
}
