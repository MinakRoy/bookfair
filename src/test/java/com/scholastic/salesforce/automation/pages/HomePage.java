package com.scholastic.salesforce.automation.pages;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.opencsv.CSVReader;
import com.scholastic.salesforce.automation.beans.InstituteBean;
import com.scholastic.salesforce.automation.beans.OTCXMLBean;
import com.scholastic.salesforce.automation.steps.HomePageStepDefs;
import com.scholastic.salesforce.automation.support.ConstantUtils;
import com.scholastic.salesforce.automation.support.SCHUtils;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;

public class HomePage extends BaseTestPage<TestPage> {

	@FindBy(locator = "INPUT_USERNAME")
	private WebElement inputUsername;
	@FindBy(locator = "INPUT_PASSWORD")
	private WebElement inputPassword;
	@FindBy(locator = "BUTTON_LOG_IN")
	private WebElement buttonLogin;
	@FindBy(locator = "INPUT_SEARCH_TOP")
	private WebElement inputSearchTop;
	@FindBy(locator = "LINK_SIDE_MENU_LOGOUT")
	private WebElement linkSideMenuLogout;
	@FindBy(locator = "LINK_LOGOUT")
	private WebElement linkLogout;
	@FindBy(locator = "LINK_MORE")
	private WebElement linkMore;
	@FindBy(locator = "AL_MENU_LINK")
	private WebElement alMenuLink;
	@FindBy(locator = "AL_LINK_FIELD_SERVICE_CONTRACT")
	private WebElement linkFieldServiceContract;
	@FindBy(locator = "AL_LINK_PRODUCTS")
	private WebElement linkProducts;
	@FindBy(locator = "HOME_LINK_MENU_TRIGGER")
	private WebElement linkMenuTrigger;
	@FindBy(locator = "HOME_LINK_SETUP")
	private WebElement linkSetup;
	@FindBy(locator = "HOME_LINK_ENVIRONMENT")
	private WebElement linkEnvironment;
	@FindBy(locator = "HOME_LINK_JOBS")
	private WebElement linkJobs;
	@FindBy(locator = "HOME_LINK_BULK_DATA_LOAD_JOB")
	private WebElement linkBulkDataLoadJob;
	@FindBy(locator = "HOME_LABEL_COMPLETED_PROCESS_TIME_LIST")
	private List<WebElement> labelCompletedProcessTimeList;
	@FindBy(locator = "HOME_FRAME_CONTENT")
	private WebElement frameContent;
	@FindBy(locator = "HOME_LABEL_RECORD_PROCESSED_LIST")
	private List<WebElement> labelRecordProcessedList;
	@FindBy(locator = "HOME_LABEL_INPROGRESS_START_TIME")
	private List<WebElement> labelInporgressStatTime;
	@FindBy(locator = "HOME_LABEL_RECORD_PROCESSED")
	private List<WebElement> labelRecordProcessed;
	@FindBy(locator = "HOME_LABEL_RECORD_FAILED")
	private List<WebElement> labelRecordFailed;
	@FindBy(locator = "HOME_LINK_BULK_UPDATE")
	private WebElement homeLinkBulkUpdate;
	@FindBy(locator = "HOME_LINK_REPORTS")
	private WebElement homeLinkReports;
	@FindBy(locator = "HOME_LINK_USERS_MENU")
	private WebElement homeLinkUsersMenu;
	@FindBy(locator = "HOME_LINK_USERS_SUBMENU")
	private WebElement homeLinkUsersSubmenu;

	public WebElement getHomeLinkUsersMenu() {
		return homeLinkUsersMenu;
	}

	public WebElement getHomeLinkUsersSubmenu() {
		return homeLinkUsersSubmenu;
	}

	public WebElement getHomeLinkReports() {
		return homeLinkReports;
	}

	public WebElement getHomeLinkBulkUpdate() {
		return homeLinkBulkUpdate;
	}

	public List<WebElement> getLabelRecordProcessed() {
		return labelRecordProcessed;
	}

	public List<WebElement> getLabelRecordFailed() {
		return labelRecordFailed;
	}

	public List<WebElement> getLabelInporgressStatTime() {
		return labelInporgressStatTime;
	}

	public List<WebElement> getLabelRecordProcessedList() {
		return labelRecordProcessedList;
	}

	public WebElement getFrameContent() {
		return frameContent;
	}

	public List<WebElement> getLabelCompletedProcessTimeList() {
		return labelCompletedProcessTimeList;
	}

	public WebElement getLinkEnvironment() {
		return linkEnvironment;
	}

	public WebElement getLinkJobs() {
		return linkJobs;
	}

	public WebElement getLinkBulkDataLoadJob() {
		return linkBulkDataLoadJob;
	}

	public WebElement getLinkMenuTrigger() {
		return linkMenuTrigger;
	}

	public WebElement getLinkSetup() {
		return linkSetup;
	}

	public WebElement getLinkProducts() {
		return linkProducts;
	}

	public WebElement getAlMenuLink() {
		return alMenuLink;
	}

	public WebElement getLinkFieldServiceContract() {
		return linkFieldServiceContract;
	}

	public WebElement getLinkMore() {
		return linkMore;
	}

	public WebElement getLinkSideMenuLogout() {
		return linkSideMenuLogout;
	}

	public WebElement getLinkLogout() {
		return linkLogout;
	}

	public WebElement getInputSearchTop() {
		return inputSearchTop;
	}

	public WebElement getInputUsername() {
		return inputUsername;
	}

	public WebElement getInputPassword() {
		return inputPassword;
	}

	public WebElement getButtonLogin() {
		return buttonLogin;
	}

	enum HeaderMenu {
		Home, Chatter, Opportunities, Leads, Accounts, Contacts, Campaigns, Dashboards, Tasks, Notes, Calendar, Groups, Reports, Files, People, Cases, Products, Account_Requests(
				"Account Requests"), Field_Service_Contracts("Field Service Contracts");
		public String menu;

		HeaderMenu() {
			menu = toString();
		}

		HeaderMenu(String menu) {
			this.menu = menu;
		}

		public WebElement getElement() {
			return SCHUtils.findElement(TestBaseProvider.getTestBase().getString("LINK_DYNAMIC_MENU"), menu);
		}

		public boolean equals(String menu) {
			if (this.menu.toUpperCase().contains(menu.toUpperCase())) {
				return true;
			} else {
				return false;
			}
		}
	}

	@Override
	protected void openPage() {
	}

	public void doLogin(String username, String password) {
		if (getTestBase().getString("salesfoce", "true").equals("true")) {
			getDriver().get(getTestBase().getString("salesforce.base.url"));
			WaitUtils.waitForDisplayed(getInputUsername());
			getInputUsername().sendKeys(username);
			getInputPassword().sendKeys(password);
			getButtonLogin().click();
			WaitUtils.waitForDisplayed(getInputSearchTop());
		} else {
			// api login
			Assert.fail("Not Implemented");
		}
	}

	public void navigateToMenu(String menu) {
		for (HeaderMenu hmenu : HeaderMenu.values()) {
			if (hmenu.equals(menu)) {
				if (menu.equals("Account Requests")) {
					SCHUtils.clickUsingJavaScript(getLinkMore());
				} else if (menu.equals("Field Service Contracts")) {
					getAlMenuLink().click();
					SCHUtils.clickUsingJavaScript(getLinkFieldServiceContract());
				} else if (menu.equals("Products")) {
					getAlMenuLink().click();
					SCHUtils.clickUsingJavaScript(getLinkProducts());
				} else {
					WaitUtils.waitForDisplayed(hmenu.getElement());
					SCHUtils.clickUsingJavaScript(hmenu.getElement());
					SCHUtils.waitForAjaxToComplete();
				}
				break;
			}
		}
	}

	public void doLogOut(String application) {
		WaitUtils.waitForDisplayed(getLinkSideMenuLogout());
		getLinkSideMenuLogout().click();
		WaitUtils.waitForDisplayed(getLinkLogout());
		getLinkLogout().click();
		WaitUtils.waitForDisplayed(getInputUsername());
	}

	public void addressesArePopulatedWithAddressTypeDetails() {
		// HomePageStepDefs homeSteps = new HomePageStepDefs();
		// homeSteps.userLoginInToSystem();
		// HomePage homePage = new HomePage();
		// homePage.navigateToMenu("Accounts");
		AccountsPage accountPage = new AccountsPage();
		// String ucnNumber = (String)
		// TestBaseProvider.getTestBase().getContext()
		// .getProperty("CREATED_INSTITUTE_UCN_ADDRESS");
		// System.out.println("UCN :: " + ucnNumber);
		// accountPage.searchForAccount(ucnNumber);
		// InstituteBean instituteBean = (InstituteBean)
		// TestBaseProvider.getTestBase().getContext()
		// .getProperty("testexecution.INSTITUTEDETAILS_ADDRESS");
		// accountPage.navigateToCreatedAccount(instituteBean);
		SCHUtils.clickUsingJavaScript(accountPage.getLinkRelated());
		SCHUtils.waitForAjaxToComplete();
		accountPage.verifyAddressArePopulatedWithAddressTypeDetails();
	}

	public void oneAddressPopulatedShouldMailingAddress() {
		AccountsPage accountPage = new AccountsPage();
		WaitUtils.waitForDisplayed(accountPage.getLinkRelated());
		SCHUtils.clickUsingJavaScript(accountPage.getLinkRelated());
		SCHUtils.waitForAjaxToComplete();
		((JavascriptExecutor) getDriver()).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		SCHUtils.waitForAjaxToComplete();
		((JavascriptExecutor) getDriver()).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		SCHUtils.waitForAjaxToComplete();
		accountPage.verifyOneAddressPopulatedWithMailingAddress();
	}

	public void thereShouldBeLinkToMergedAccountFromTheSourceAccount() {
		HomePageStepDefs homeSteps = new HomePageStepDefs();
		homeSteps.userLoginInToSystem();
		HomePage homePage = new HomePage();
		SCHUtils.waitforDataToSyncInCDB();
		homePage.navigateToMenu("Accounts");
		AccountsPage accountPage = new AccountsPage();
		String ucnNumber = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_INSTITUTE_UCN_SECOND");
		System.out.println("UCN :: " + ucnNumber);
		accountPage.searchForAccount(ucnNumber);
		InstituteBean instituteBean = (InstituteBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.INSTITUTEDETAILS_SECOND");
		accountPage.navigateToCreatedAccount(instituteBean);
		WaitUtils.waitForDisplayed(accountPage.getLinkDetails());
		accountPage.getLinkDetails().click();
		SCHUtils.pause(2);
		((JavascriptExecutor) getDriver()).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		SCHUtils.waitForAjaxToComplete();
		accountPage.verifyLinkToMergedAccountFromTheSourceAccount();
	}

	public void accountListedInRelatedChildInstitutionFieldChangeToCurrentDate() {
		HomePageStepDefs stepDef = new HomePageStepDefs();
		stepDef.userLoginInToSystem();
		// waiting 5 min to sync data from cdb to SFDC
		for (int i = 0; i < 5; i++) {
			TestBaseProvider.getTestBase().getDriver().getTitle();
			SCHUtils.pause(60);
		}

		navigateToMenu("Account");
		AccountsPage accountPage = new AccountsPage();

		accountPage.accountListedInRelatedChildInstituteFieldChangeToCurrentDate();
	}

	public void newAccountRelationshipCreatedWithTheNewParentAccount() {
		HomePageStepDefs stepDef = new HomePageStepDefs();
		stepDef.userLoginInToSystem();
		SCHUtils.waitforDataToSyncInCDB();
		navigateToMenu("Account");
		AccountsPage accountPage = new AccountsPage();
		accountPage.newAccountRelationshipCreatedWithNewParent();
	}

	public void userShouldNotbeAbleToMakeUpdateIn(String arg1) {
		navigateToMenu("Account");
		AccountsPage accountPage = new AccountsPage();
		accountPage.userShouldNotbeAbleToMakeUpdateInClosedAccount(arg1);
	}

	public void numberOfOpportunitiesExist(String arg1) {
		HomePageStepDefs stepDef = new HomePageStepDefs();
		stepDef.userLoginInToSystem();
		navigateToMenu("Account");
		AccountsPage accountPage = new AccountsPage();
		accountPage.navigateToFirstAccount();
		accountPage.numberOfOpportunityExist(arg1);
	}

	public void validAccountExistsToReceiveAMergeNumberOfOpportunities(String arg1) {
		navigateToMenu("Account");
		AccountsPage accountPage = new AccountsPage();
		accountPage.validAccountExistsToReceiveAMergeNumberOfOpportunity(arg1);
	}

	public void numberOfTasksExists(String arg1) {
		AccountsPage accountPage = new AccountsPage();
		accountPage.navigateToFirstAccount();
		accountPage.numberOfTasksExists(arg1);
	}

	public void validAccountExistsToReceiveMergeNumberofTasks(String arg1) {
		AccountsPage accountPage = new AccountsPage();
		accountPage.validAccountExistsToReceiveAMergeNumberOfTasks(arg1);
	}

	public void numberOfContactsExists(String arg1) {
		AccountsPage accountPage = new AccountsPage();
		accountPage.navigateToFirstAccount();
		accountPage.numberOfContactsExists(arg1);
	}

	public void validAccountExistsToReceiveMergeNumberofContacts(String arg1) {
		AccountsPage accountPage = new AccountsPage();
		accountPage.validAccountExistsToReceiveMergeNumberofContracts(arg1);
	}

	public void numberOfEventsExists(String arg1) {
		AccountsPage accountPage = new AccountsPage();
		accountPage.navigateToFirstAccount();
		accountPage.numberOfEventsExists(arg1);
	}

	public void validAccountExistsToReceiveMergeNumberofEvents(String arg1) {
		AccountsPage accountPage = new AccountsPage();
		accountPage.validAccountExistsToReceiveMergeNumberofEvents(arg1);
	}

	public void anAccountExistWithInSalesForce(String arg1) {
		TMSCRMHomePage tmcHomePage = new TMSCRMHomePage();
		tmcHomePage.createFirstAccount();
		SCHUtils.waitforDataToSyncInCDB();
		HomePageStepDefs homePageStepDefs = new HomePageStepDefs();
		homePageStepDefs.userLoginInToSystem();
		HomePage homePage = new HomePage();
		homePage.navigateToMenu("Accounts");
		AccountsPage accountPage = new AccountsPage();
		accountPage.navigateToFirstAccount();
		if (arg1.equals("Opportunity")) {
			accountPage.numberOfOpportunityExist("1");
		} else if (arg1.equals("Tasks")) {
			accountPage.numberOfTasksExists("1");
		} else if (arg1.equals("Events")) {
			accountPage.numberOfEventsExists("1");
		} else if (arg1.equals("Note")) {
			accountPage.numberOfNotesExists("1");
		} else if (arg1.equals("Contact")) {
			accountPage.numberOfContactsExists("1");
		} else {
			Assert.fail("Wrong " + arg1 + " Passed.");
		}
	}

	public void endUserInSalesforceShouldNotbeABletoMakeanUpdateTo(String arg1) {
		navigateToMenu("Accounts");
		AccountsPage accountPage = new AccountsPage();
		accountPage.navigateToFirstAccount();
		accountPage.endUserInSalesforceShouldNotbeABletoMakeanUpdateTo(arg1);
	}

	public void dataShouldProcessSuccessfully() {
		navigateToSetUpMenu();
		ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tabs2.get(1));
		navigateToBulkDataLoadJobs();
		verifyJobIsProcessSuccessfully();
		getDriver().close();
		getDriver().switchTo().window(tabs2.get(0));
	}

	/**
	 * verify first completed job start time
	 */
	public void verifyJobIsProcessSuccessfully() {
		getDriver().switchTo().frame(getFrameContent());
		String StartTime = (String) TestBaseProvider.getTestBase().getContext().getProperty("COMMAND_START_TIME");
		String StartTimeWithPlusOneMinute = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("COMMAND_START_TIME_PLUS_ONE_MINUTE");
		String StartTimeWithPlusTwoMinute = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("COMMAND_START_TIME_PLUS_TWO_MINUTE");

		String StartTimeWithMinusTwoMinute = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("COMMAND_START_TIME_MINUS_ONE_MINUTE");
		String elementText = getLabelCompletedProcessTimeList().get(0).getText();
		System.out.println("element Time :: " + elementText);
		boolean flag = false;
		if (elementText.equals(StartTime) || elementText.equals(StartTimeWithPlusOneMinute)
				|| elementText.equals(StartTimeWithPlusTwoMinute) || elementText.equals(StartTimeWithMinusTwoMinute)) {
			flag = true;
		}
		Assert.assertEquals(flag, true, "Job is Not Processed Successfully");
	}

	public void navigateToBulkDataLoadJobs() {
		WaitUtils.waitForDisplayed(getLinkEnvironment());
		SCHUtils.clickUsingJavaScript(getLinkEnvironment());
		WaitUtils.waitForDisplayed(getLinkJobs());
		SCHUtils.clickUsingJavaScript(getLinkJobs());
		WaitUtils.waitForDisplayed(getLinkBulkDataLoadJob());
		SCHUtils.clickUsingJavaScript(getLinkBulkDataLoadJob());
		SCHUtils.waitForAjaxToComplete();
	}

	public void navigateToSetUpMenu() {
		WaitUtils.waitForDisplayed(getLinkMenuTrigger());
		SCHUtils.clickUsingJavaScript(getLinkMenuTrigger());
		WaitUtils.waitForDisplayed(getLinkSetup());
		SCHUtils.clickUsingJavaScript(getLinkSetup());
		// SCHUtils.waitforDataToSyncInCDB();
	}

	public void accountFromTheFileisUpdatedInAccount() {
		AccountsPage accountPage = new AccountsPage();
		accountPage.AccountsIsUpdated();
	}

	public void the_identical_entry_should_not_create_a_duplicate_in_Salesforce() {
		AccountsPage accountsPage = new AccountsPage();
		accountsPage.the_identical_entry_should_not_create_a_duplicate_in_Salesforce();
	}

	public void the_data_from_the_first_batch_should_process_successfully() {
		navigateToSetUpMenu();
		ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tabs2.get(1));
		navigateToBulkDataLoadJobs();
		verifyInProgressJob();
		getDriver().close();
		getDriver().switchTo().window(tabs2.get(0));
	}

	private void verifyInProgressJob() {
		getDriver().switchTo().frame(getFrameContent());
		String StartTime = (String) TestBaseProvider.getTestBase().getContext().getProperty("COMMAND_START_TIME");
		String StartTimeWithPlusOneMinute = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("COMMAND_START_TIME_PLUS_ONE_MINUTE");
		String StartTimeWithPlusTwoMinute = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("COMMAND_START_TIME_PLUS_TWO_MINUTE");
		System.out.println(
				"Start Time ::" + StartTime + " +1" + StartTimeWithPlusOneMinute + " +2 " + StartTimeWithPlusTwoMinute);

		String elementText = getLabelInporgressStatTime().get(0).getText();
		boolean flag = false;
		if (elementText.equals(StartTime) || elementText.equals(StartTimeWithPlusOneMinute)
				|| elementText.equals(StartTimeWithPlusTwoMinute)) {
			flag = true;
		}
		Assert.assertEquals(flag, true, "Job is Not Processed Successfully");
	}

	public void the_data_that_is_outside_the_first_batch_of_records_should_not_be_processed() {
		AccountsPage accountPage = new AccountsPage();
		accountPage.dataOutsideTheFirstBatchShouldNotBeProcessed();
	}

	public void verifyProcessingFailed() {
		getDriver().switchTo().frame(getFrameContent());
		WaitUtils.waitForDisplayed(getLabelRecordProcessed().get(0));
		AssertUtils.assertTextMatches(getLabelRecordProcessed().get(0),
				Matchers.equalTo(ConstantUtils.PROCESSED_RECORD));
		AssertUtils.assertTextMatches(getLabelRecordFailed().get(0),
				Matchers.equalTo(ConstantUtils.PROCESSED_RECORD_FAIL));

	}

	public void verifyItIsSuccessful() {
		getDriver().switchTo().frame(getFrameContent());
		WaitUtils.waitForDisplayed(getLabelRecordProcessed().get(0));
		AssertUtils.assertTextMatches(getLabelRecordProcessed().get(0),
				Matchers.equalTo(ConstantUtils.PROCESSED_RECORD));
		AssertUtils.assertTextMatches(getLabelRecordFailed().get(0),
				Matchers.equalTo(ConstantUtils.PROCESSED_RECORD_FAIL_TO_PAAS));
	}

	public void the_user_record_is_Searched_in_Salesforce(String recordNumber) {
		int intRecordNumber = 0;
		if (recordNumber.equals("first")) {
			intRecordNumber = 1;
		} else if (recordNumber.equals("10th")) {
			intRecordNumber = 11;
		} else if (recordNumber.equals("50th")) {
			intRecordNumber = 51;
		} else if (recordNumber.equals("last")) {
			intRecordNumber = 100;
		} else {
			Assert.fail("Wronge record Number Passed.");
		}
		getUserDataFromFile(intRecordNumber);
		//
		// navigateToSetUpMenu();
		// ArrayList<String> tabs2 = new
		// ArrayList<String>(getDriver().getWindowHandles());
		// getDriver().switchTo().window(tabs2.get(1));
		// navigateToUserMenu();
		//
		// getDriver().switchTo().frame(getFrameContent());
		// verifyUserData();
	}

	/**
	 * email validation is remain, as in latest file we are getting blank and
	 * other file have .dev emails
	 */

	public void verifyUserData() {
		String username = (String) TestBaseProvider.getTestBase().getContext().getProperty("USER_NAME");
		String email = (String) TestBaseProvider.getTestBase().getContext().getProperty("USERNAME_EMAIL");
		String firstStr[] = username.split(" ");
		WebElement dynamicElement = SCHUtils.findElement(
				TestBaseProvider.getTestBase().getString("HOME_LINK_DYNAMIC_USER_FILTER"),
				String.valueOf(firstStr[1].charAt(0)));
		SCHUtils.clickUsingJavaScript(dynamicElement);
		SCHUtils.waitForAjaxToComplete();
		getDriver().switchTo().frame(getFrameContent());
		String formattedUsernameAsPerSFDC = firstStr[1] + ", " + firstStr[0];

		WebElement usernameElement = null;
		try {
			usernameElement = SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("HOME_LINK_USER_NAME"),
					formattedUsernameAsPerSFDC);
			AssertUtils.assertDisplayed(usernameElement);
		} catch (Exception e) {
			Assert.fail("Username not found " + formattedUsernameAsPerSFDC);
		}

		System.out.println("Email :: " + email);

	}

	public void navigateToUserMenu() {
		WaitUtils.waitForDisplayed(getHomeLinkUsersMenu());
		SCHUtils.clickUsingJavaScript(getHomeLinkUsersMenu());
		WaitUtils.waitForDisplayed(getHomeLinkUsersSubmenu());
		SCHUtils.clickUsingJavaScript(getHomeLinkUsersSubmenu());
		SCHUtils.waitForAjaxToComplete();
	}

	/**
	 * 
	 * @param recordNumber
	 */

	public void getUserDataFromFile(int recordNumber) {
		String fileToRead = "./src/test/resources/s3/" + TestBaseProvider.getTestBase().getString("FILENAME");
		File file = new File(fileToRead);
		CSVReader reader = null;
		int i = 0;
		try {
			reader = new CSVReader(new FileReader(file));
			String[] nextLine = null;
			while ((nextLine = reader.readNext()) != null) {
				if (i == 0) {
					System.out.println("Skipping Header.");
				} else if (i == recordNumber) {
					TestBaseProvider.getTestBase().getContext().setProperty("USERNAME_EMAIL", nextLine[1]);
					TestBaseProvider.getTestBase().getContext().setProperty("USER_NAME", nextLine[2]);
					break;
				} else if (i == 101) {
					break;
				}
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (i == 1) {
			// TestBaseProvider.getTestBase().getContext().setProperty("USERNAME_EMAIL",
			// "jelkhoury@scholastic.com.dev");
			// TestBaseProvider.getTestBase().getContext().setProperty("USER_NAME",
			// "Joey Elkhoury");
			Assert.fail("There are no records in file.");
		}
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void the_contract_should_be_created_with_each_of_the_values() {
		OTCXMLBean otcXMLBean = (OTCXMLBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("OTC_CONTRACT_CREATION_VALUE");
		String contractNumber = otcXMLBean.getCorpIDAndContractNumber();
		System.out.println("Contract Created From OTC :: " + contractNumber);
		searchforContract(contractNumber);
		verifyContractPresent(contractNumber);
		verifyContractDetails(otcXMLBean);
	}

	public static String returnSFDCFormattedDate(String contractDate) {
		String formattedSD[] = contractDate.split("-");
		contractDate = formattedSD[1] + "/" + formattedSD[2] + "/" + formattedSD[0];
		return contractDate;
	}

	public void verifyContractDetails(OTCXMLBean otcXMLBean) {
		String contractNumber = otcXMLBean.getCorpIDAndContractNumber();
		String contractLinePrice = otcXMLBean.getContractLinePrice();
		String lineQTY = otcXMLBean.getOTCLineQuantity();
		String contractStartDate = otcXMLBean.getStartDate();
		String contractEndDate = otcXMLBean.getEndDate();
		contractStartDate = returnSFDCFormattedDate(contractStartDate);
		contractEndDate = returnSFDCFormattedDate(contractEndDate);
		String lineStatus = otcXMLBean.getLineStatus();
		String salesOrderLineNumber = otcXMLBean.getSalesOrderLineNumber();
		String salesOrderNumber = otcXMLBean.getSalesOrderNumber();

		Assert.assertEquals(getDynamicElementString("Contract Number"), contractNumber);
		Assert.assertEquals(getDynamicElementString("Contract Line Price"), "$" + contractLinePrice);
		Assert.assertEquals(getDynamicElementString("Line Quantity"), lineQTY);
		Assert.assertEquals(getDynamicElementString("Start Date"), contractStartDate);
		Assert.assertEquals(getDynamicElementString("End Date"), contractEndDate);
		Assert.assertEquals(getDynamicElementString("Line Status").toLowerCase(), lineStatus);
		Assert.assertEquals(getDynamicElementString("Sales Order Number"), salesOrderNumber);
		Assert.assertEquals(getDynamicElementString("Sales Order Line Number"), salesOrderLineNumber);
		Assert.assertEquals(getDynamicElementString("Title"), otcXMLBean.getTitle());
		Assert.assertEquals(getDynamicElementString("Install To UCN"), otcXMLBean.getInstallToUCN());

	}

	public String getDynamicElementString(String elementText) {
		WebElement dynamicElement = SCHUtils.findElement(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_FIELD_SERVICE_CONTRACT"), elementText);
		return dynamicElement.getText();
	}

	public void verifyContractPresent(String contractNumber) {
		try {
			WebElement contractLink = SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getString("HOME_LINK_DYNAMIC_FSC_OTC"), contractNumber);
			WaitUtils.waitForDisplayed(contractLink);
			SCHUtils.clickUsingJavaScript(contractLink);
			SCHUtils.waitForAjaxToComplete();
		} catch (Exception e) {
			Assert.fail("Contract " + contractNumber + " Not Received From OTC.");
		}
	}

	public void searchforContract(String contractNumber) {
		WaitUtils.waitForDisplayed(getInputSearchTop());
		getInputSearchTop().sendKeys(contractNumber);
		getInputSearchTop().sendKeys(Keys.ENTER);
		SCHUtils.waitForAjaxToComplete();
	}
}
