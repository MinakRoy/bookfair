package com.scholastic.salesforce.automation.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.scholastic.salesforce.automation.beans.FieldServiceContractBean;
import com.scholastic.salesforce.automation.support.RandomDataUtil;
import com.scholastic.salesforce.automation.support.SCHUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import junit.framework.Assert;

public class ProductsPage extends BaseTestPage<HomePage> {

	@FindBy(locator = "PRODUCTS_LINK_NEW")
	private WebElement linkNew;
	@FindBy(locator = "PRODUCTS_BUTTON_PRODUCT_SAVE")
	private WebElement buttonProductSave;
	@FindBy(locator = "PRODUCT_LINK_PRODUCT_FAMILY")
	private WebElement productLinkProductFamily;
	@FindBy(locator = "PRODUCT_TITLE_LABEL")
	private WebElement productTitleLabel;
	@FindBy(locator = "PRODUCT_LINK_EDIT")
	private WebElement productLinkEdit;

	public WebElement getProductLinkEdit() {
		return productLinkEdit;
	}

	public WebElement getProductTitleLabel() {
		return productTitleLabel;
	}

	public WebElement getProductLinkProductFamily() {
		return productLinkProductFamily;
	}

	public WebElement getButtonProductSave() {
		return buttonProductSave;
	}

	public WebElement getLinkNew() {
		return linkNew;
	}

	@Override
	protected void openPage() {
	}

	public void productIsCreatedWithTheInvalidProductNumber() {
		navigateToProductPage();
		WaitUtils.waitForDisplayed(getLinkNew());
		getLinkNew().click();
		createProductWithInvalidProductNumber();
	}

	private void navigateToProductPage() {
		HomePage homePage = new HomePage();
		homePage.navigateToMenu("Products");
		SCHUtils.waitForAjaxToComplete();
	}

	private void createProductWithInvalidProductNumber() {
		FieldServiceContractBean fsBean = (FieldServiceContractBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.FSCONTRACTDETAILS");
		String inValidItemNumber = fsBean.getItemNumber();
		WebElement inputDynamicField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("PRODUCTS_INPUT_DYNAMIC_PRODUCT"), "Product Name");
		String productName = RandomDataUtil.getRandomProductName();
		WaitUtils.waitForDisplayed(inputDynamicField);
		inputDynamicField.sendKeys(productName);
		inputDynamicField = SCHUtils
				.findElement(TestBaseProvider.getTestBase().getString("PRODUCTS_INPUT_DYNAMIC_PRODUCT"), "Item Number");
		inputDynamicField.sendKeys(inValidItemNumber);
		TestBaseProvider.getTestBase().getContext().setProperty("CREATED_PRODUCT_FOR_INVALID_PRODUCT", productName);
		getButtonProductSave().click();
		SCHUtils.waitForAjaxToComplete();
	}

	public void createProduct(String string) {
		navigateToProductPage();
		WaitUtils.waitForDisplayed(getLinkNew());
		getLinkNew().click();
		if (string.equals("Not a Service Product")) {
			createProductWithNotServiceProduct();
		} else if (string.equals("an inactive Product Number")) {
			createInactiveProduct();
		} else {
			Assert.fail("Invalid Param Passed.");
		}
	}

	private void createInactiveProduct() {
		String itemNumber = RandomDataUtil.getRandomNumber(6);
		WebElement inputDynamicField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("PRODUCTS_INPUT_DYNAMIC_PRODUCT"), "Product Name");
		String productName = RandomDataUtil.getRandomProductName();
		WaitUtils.waitForDisplayed(inputDynamicField);
		inputDynamicField.sendKeys(productName);
		inputDynamicField = SCHUtils
				.findElement(TestBaseProvider.getTestBase().getString("PRODUCTS_INPUT_DYNAMIC_PRODUCT"), "Item Number");
		inputDynamicField.sendKeys(itemNumber);
		WebElement activeCheckBox = SCHUtils
				.findElement(TestBaseProvider.getTestBase().getString("PRODUCTS_INPUT_DYNAMIC_PRODUCT"), "Active");
		if (activeCheckBox.isSelected()) {
			activeCheckBox.click();
		}
		TestBaseProvider.getTestBase().getContext().setProperty("CREATED_PRODUCT_NAME_FOR_INACTIVE_PRODUCT",
				productName);
		TestBaseProvider.getTestBase().getContext().setProperty("CREATED_PRODUCT_ITEM_NUMBER_FOR_INACTIVE_PRODUCT",
				itemNumber);
		getButtonProductSave().click();
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getProductTitleLabel());
		TestBaseProvider.getTestBase().getContext().setProperty("INACTIVE_PRODUCT_NAME",
				getProductTitleLabel().getText());
	}

	private void createProductWithNotServiceProduct() {
		String itemNumber = RandomDataUtil.getRandomNumber(6);
		WebElement inputDynamicField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("PRODUCTS_INPUT_DYNAMIC_PRODUCT"), "Product Name");
		String productName = RandomDataUtil.getRandomProductName();
		WaitUtils.waitForDisplayed(inputDynamicField);
		inputDynamicField.sendKeys(productName);
		inputDynamicField = SCHUtils
				.findElement(TestBaseProvider.getTestBase().getString("PRODUCTS_INPUT_DYNAMIC_PRODUCT"), "Item Number");
		inputDynamicField.sendKeys(itemNumber);
		WebElement activeCheckBox = SCHUtils
				.findElement(TestBaseProvider.getTestBase().getString("PRODUCTS_INPUT_DYNAMIC_PRODUCT"), "Active");
		if (!activeCheckBox.isSelected()) {
			activeCheckBox.click();
		}
		selectProductFamily("Digital Product");
		TestBaseProvider.getTestBase().getContext().setProperty("CREATED_PRODUCT_NAME_FOR_NOT_SERVICE_PRODUCT",
				productName);
		TestBaseProvider.getTestBase().getContext().setProperty("CREATED_PRODUCT_ITEM_NUMBER_FOR_NOT_SERVICE_PRODUCT",
				itemNumber);
		getButtonProductSave().click();
		SCHUtils.waitForAjaxToComplete();
	}

	private void selectProductFamily(String string) {
		if (string.equals("Digital Product")) {
			getProductLinkProductFamily().sendKeys("Digital Products");
		} else {
			Assert.fail("Invalid Param Passed.");
		}
	}

	public void reactiveInactiveProductNumber() {
		navigateToProductPage();
		navigateToCreatedInactiveProduct();
		madeInactiveProductActive();
	}

	private void madeInactiveProductActive() {
		WaitUtils.waitForDisplayed(getProductLinkEdit());
		getProductLinkEdit().click();
		WebElement activeCheckbox = SCHUtils
				.findElement(TestBaseProvider.getTestBase().getString("PRODUCTS_INPUT_DYNAMIC_PRODUCT"), "Active");
		if (activeCheckbox.isSelected()) {
			Assert.fail("Product is already activated.");
		} else {
			activeCheckbox.click();
		}
		getButtonProductSave().click();
	}

	private void navigateToCreatedInactiveProduct() {
		WebElement inactiveProduct = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("PRODUCT_LINK_DYNAMIC"),
				(String) TestBaseProvider.getTestBase().getContext().getProperty("INACTIVE_PRODUCT_NAME"));
		inactiveProduct.click();
	}

}
