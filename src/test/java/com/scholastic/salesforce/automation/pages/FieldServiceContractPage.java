package com.scholastic.salesforce.automation.pages;

import static org.testng.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.scholastic.salesforce.automation.beans.FieldServiceContractBean;
import com.scholastic.salesforce.automation.beans.OTCXMLBean;
import com.scholastic.salesforce.automation.support.ConstantUtils;
import com.scholastic.salesforce.automation.support.OTCXMLGenerate;
import com.scholastic.salesforce.automation.support.RandomDataUtil;
import com.scholastic.salesforce.automation.support.SCHUtils;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

public class FieldServiceContractPage extends BaseTestPage<HomePage> {

	@FindBy(locator = "FSC_LINK_NEW")
	private WebElement linkNew;
	@FindBy(locator = "FSC_BUTTON_FIELD_CONTRACT_SAVE")
	private WebElement buttonFieldContractSave;
	@FindBy(locator = "FSC_LABEL_FIELD_SERVICE_CONTRACT")
	private WebElement labelFieldServiceContract;
	@FindBy(locator = "FSC_LINK_RECENTLY_VIEWED")
	private WebElement linkRecentlyViewed;
	@FindBy(locator = "FSC_LINK_ALL_VIEWED")
	private WebElement linkAllViewed;
	@FindBy(locator = "FSC_LINK_FIELD_SERVIC_CONTRACT_LIST")
	private List<WebElement> linkFieldServiceContractList;
	@FindBy(locator = "FSC_LINK_WORK_ORDER_LIST")
	private List<WebElement> linkWorkOrderList;
	@FindBy(locator = "FSC_LABLE_WORK_ORDER_NUMBER")
	private WebElement lableWorkOrderNumber;
	@FindBy(locator = "FSC_LINK_WORK_ORDER_VIEW_ALL")
	private WebElement linkWorkOrdereViewAll;
	@FindBy(locator = "FSC_LINK_TEXT_WORK_ORDER")
	private WebElement linkTextWorkOrder;
	@FindBy(locator = "FSC_BUTTON_EDIT_SAVE")
	private WebElement buttonEditSave;
	@FindBy(locator = "FSC_LABLE_FIELD_SERVICE_CREATED_CONTRACT_NUMBER")
	private WebElement lableFieldServiceCreatedContractNumber;
	@FindBy(locator = "FSC_LIST_SERVICE_NUMBER_IN_WORK_ORDER")
	private List<WebElement> listServiceNumberInWorkOrder;
	@FindBy(locator = "FSC_LABEL_DYNAMIC_FIELD_SERVICE_CONTRACT")
	private WebElement labelDynamicFieldServiceContract;
	@FindBy(locator = "FSC_LINK_LIST_WORK_ORDER")
	private List<WebElement> listWorkOrder;
	@FindBy(locator = "FSC_LINK_WO_EDIT")
	private WebElement linkWoEdit;
	@FindBy(locator = "FSC_WO_DROP_DOWN")
	private List<WebElement> woDropDown;
	@FindBy(locator = "FSC_WO_OPEN_DROP_DOWN")
	private List<WebElement> woOpenDropDown;
	@FindBy(locator = "FSC_LINK_WO_STATUS")
	private WebElement linkWoStatus;
	@FindBy(locator = "FSC_LINK_LINE_STATUS")
	private WebElement linkLineStatus;
	@FindBy(locator = "FSC_LIST_WO_DELIVERED")
	private List<WebElement> listWoDelivered;
	@FindBy(locator = "FSC_LIST_WO_CANCELLED")
	private List<WebElement> listWoCancelled;
	@FindBy(locator = "FSC_LIST_WO_CANCELLED_DELIVERED")
	private List<WebElement> listWoCancelledDelivered;
	@FindBy(locator = "FSC_LIST_WO_OPEN")
	private List<WebElement> listWoOpen;
	@FindBy(locator = "FSC_LIST_WO_ASSIGNED")
	private List<WebElement> listWoAssigned;
	@FindBy(locator = "FSC_WO_LINK_WORK_ORDER_LIST")
	private List<WebElement> woListLinkWorkOrder;

	public List<WebElement> getWoOpenDropDown() {
		return woOpenDropDown;
	}

	public List<WebElement> getWoListLinkWorkOrder() {
		return woListLinkWorkOrder;
	}

	public List<WebElement> getListWoOpen() {
		return listWoOpen;
	}

	public List<WebElement> getListWoAssigned() {
		return listWoAssigned;
	}

	public List<WebElement> getListWoCancelledDelivered() {
		return listWoCancelledDelivered;
	}

	public List<WebElement> getListWoDelivered() {
		return listWoDelivered;
	}

	public List<WebElement> getListWoCancelled() {
		return listWoCancelled;
	}

	public static int getWorkOrderNumber() {
		return workOrderNumber;
	}

	public static int workOrderNumber = 0;

	public WebElement getLinkLineStatus() {
		return linkLineStatus;
	}

	public WebElement getLinkWoStatus() {
		return linkWoStatus;
	}

	public List<WebElement> getWoDropDown() {
		return woDropDown;
	}

	public WebElement getLabelDynamicFieldServiceContract() {
		return labelDynamicFieldServiceContract;
	}

	public List<WebElement> getListWorkOrder() {
		return listWorkOrder;
	}

	public WebElement getLinkWoEdit() {
		return linkWoEdit;
	}

	public List<WebElement> getListServiceNumberInWorkOrder() {
		return listServiceNumberInWorkOrder;
	}

	public WebElement getLableFieldServiceCreatedContractNumber() {
		return lableFieldServiceCreatedContractNumber;
	}

	public WebElement getButtonEditSave() {
		return buttonEditSave;
	}

	public WebElement getLinkTextWorkOrder() {
		return linkTextWorkOrder;
	}

	public WebElement getLinkRecentlyViewed() {
		return linkRecentlyViewed;
	}

	public WebElement getLinkAllViewed() {
		return linkAllViewed;
	}

	public List<WebElement> getLinkFieldServiceContractList() {
		return linkFieldServiceContractList;
	}

	public List<WebElement> getLinkWorkOrderList() {
		return linkWorkOrderList;
	}

	public WebElement getLableWorkOrderNumber() {
		return lableWorkOrderNumber;
	}

	public WebElement getLinkWorkOrdereViewAll() {
		return linkWorkOrdereViewAll;
	}

	public WebElement getLabelFieldServiceContract() {
		return labelFieldServiceContract;
	}

	public WebElement getButtonFieldContractSave() {
		return buttonFieldContractSave;
	}

	public WebElement getLinkNew() {
		return linkNew;
	}

	@Override
	protected void openPage() {
	}

	public void createContractWith(String param) {
		if (param.equals("an Product Number which is not a Service Product")) {
			ProductsPage productsPage = new ProductsPage();
			productsPage.createProduct("Not a Service Product");
		}
		if (param.equals("an inactive Product Number")) {
			ProductsPage productsPage = new ProductsPage();
			productsPage.createProduct(param);
		}
		navigateToFieldServiceContractPage();
		getLinkNew().click();
		switch (param) {
		case "an invalid Install to UCN":
		case "an invalid Product Number":
		case "blank start-end dates":
		case "an Product Number which is not a Service Product":
		case "an inactive Product Number":
		case "Line Quantity":
		case "bulk update":
		case "bulk update with multiple account":
			createFieldServiceContract(param);
			break;
		default:
			Assert.fail("Invalid Param " + param + " Passed For Creating Contract");
			break;
		}
	}

	public void navigateToFieldServiceContractPage() {
		HomePage homePage = new HomePage();
		homePage.getAlMenuLink().click();
		SCHUtils.clickUsingJavaScript(homePage.getLinkFieldServiceContract());
		// homePage.navigateToMenu("Field Service Contracts");
		WaitUtils.waitForDisplayed(getLinkNew());
		SCHUtils.waitForAjaxToComplete();
	}

	public void createFieldServiceContract(String param) {
		WebElement inputDynamicField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_FIELD_SERVICE_CONTRACT"),
				"Contract Number");
		WaitUtils.waitForDisplayed(inputDynamicField);
		inputDynamicField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_FIELD_SERVICE_CONTRACT"),
				"Contract Line Number");
		WaitUtils.waitForDisplayed(inputDynamicField);
		inputDynamicField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_FIELD_SERVICE_CONTRACT"), "Install To UCN");
		WaitUtils.waitForDisplayed(inputDynamicField);
		FieldServiceContractBean fsBean = FieldServiceContractBean.getRandomFieldServiceContractBean();

		switch (param) {
		case "an invalid Install to UCN":
			break;
		case "an invalid Product Number":
			break;
		case "blank start-end dates":
			getDateFieldServiceInput("Start Date").clear();
			getDateFieldServiceInput("End Date").clear();
			break;
		case "an Product Number which is not a Service Product":
			fsBean.setItemNumber(
					TestBaseProvider.getTestBase().getString("CREATED_PRODUCT_ITEM_NUMBER_FOR_NOT_SERVICE_PRODUCT"));
			break;
		case "an inactive Product Number":
			fsBean.setItemNumber(
					TestBaseProvider.getTestBase().getString("CREATED_PRODUCT_ITEM_NUMBER_FOR_INACTIVE_PRODUCT"));
			break;
		case "Line Quantity":
			fsBean.setInstallToUCN("39832");
			fsBean.setLineQuantity(
					(String) TestBaseProvider.getTestBase().getContext().getProperty("FSC_LINE_QUANTITY"));
			String startDate = getDate("0");
			String endDate = getDate("30");
			getDateFieldServiceInput("Start Date").sendKeys(startDate);
			getDateFieldServiceInput("End Date").sendKeys(endDate);
			getDateFieldServiceInput("Sales Order Number").sendKeys(fsBean.getSalesOrderNumber());
			getDateFieldServiceInput("Sales Order Line Number").sendKeys(fsBean.getSalesOrderLineNumber());
			getDateFieldServiceInput("Assignment ID").sendKeys(fsBean.getAssignmentID());
			getDateFieldServiceInput("ISBN").sendKeys(fsBean.getISBN());
			getDateFieldServiceInput("Title").sendKeys("Test Title");
			break;
		case "bulk update":
			fsBean.setLineQuantity("10");
			fsBean.setInstallToUCN(ConstantUtils.BULKUPDATE_ACCOUNT_UCN);
			String startDateBulkUpdate = getDate("0");
			String endDateBulkUpdate = getDate("30");
			getDateFieldServiceInput("Start Date").sendKeys(startDateBulkUpdate);
			getDateFieldServiceInput("End Date").sendKeys(endDateBulkUpdate);
			getDateFieldServiceInput("Sales Order Number").sendKeys(fsBean.getSalesOrderNumber());
			getDateFieldServiceInput("Sales Order Line Number").sendKeys(fsBean.getSalesOrderLineNumber());
			getDateFieldServiceInput("Assignment ID").sendKeys(fsBean.getAssignmentID());
			getDateFieldServiceInput("ISBN").sendKeys(fsBean.getISBN());
			getDateFieldServiceInput("Title").sendKeys("Bulk Update Title");
			break;
		case "bulk update with multiple account":
			fsBean.setLineQuantity("2");
			fsBean.setInstallToUCN(ConstantUtils.BULKUPDATE_MULTIPLE_ACCOUNT_UCN);
			startDateBulkUpdate = getDate("0");
			endDateBulkUpdate = getDate("30");
			getDateFieldServiceInput("Start Date").sendKeys(startDateBulkUpdate);
			getDateFieldServiceInput("End Date").sendKeys(endDateBulkUpdate);
			getDateFieldServiceInput("Sales Order Number").sendKeys(fsBean.getSalesOrderNumber());
			getDateFieldServiceInput("Sales Order Line Number").sendKeys(fsBean.getSalesOrderLineNumber());
			getDateFieldServiceInput("Assignment ID").sendKeys(fsBean.getAssignmentID());
			getDateFieldServiceInput("ISBN").sendKeys(fsBean.getISBN());
			getDateFieldServiceInput("Title").sendKeys("Bulk Update Title");
			break;
		default:
			Assert.fail("Need to Implement for Valid Data");
			break;
		}

		TestBaseProvider.getTestBase().getContext().setProperty("testexecution.FSCONTRACTDETAILS", fsBean);

		getFieldServiceInput("Contract Number").sendKeys(fsBean.getContractnumber());
		getFieldServiceInput("Install To UCN").sendKeys(fsBean.getInstallToUCN());
		getFieldServiceInput("Contract Line Number").sendKeys(fsBean.getContractLineNumber());
		getFieldServiceInput("Item Number").sendKeys(fsBean.getItemNumber());
		getFieldServiceInput("Line Quantity").sendKeys(fsBean.getLineQuantity());
		getFieldServiceInput("External Contract ID").sendKeys(fsBean.getExternalContractID());

		getButtonFieldContractSave().click();
		SCHUtils.waitForAjaxToComplete();
		WaitUtils.waitForDisplayed(getLableFieldServiceCreatedContractNumber());

		System.out.println("Created Contract Number :: " + getLableFieldServiceCreatedContractNumber().getText());
		TestBaseProvider.getTestBase().getContext().setProperty("CONTRACT_CREATED_NUMBER",
				getLableFieldServiceCreatedContractNumber().getText());

		WebElement contractNumber = SCHUtils.findElement(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_FIELD_SERVICE_CONTRACT"),
				"Contract Number");
		TestBaseProvider.getTestBase().getContext().setProperty("CONTRACT_NUMBER_OTC_UPDATE", contractNumber.getText());
	}

	public WebElement getDateFieldServiceInput(String elementText) {
		WebElement element = SCHUtils.findElement(
				TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_DATE_FIELD_SERVICE_CONTRACT"), elementText);
		return element;
	}

	public WebElement getFieldServiceInput(String elementText) {
		WebElement element = SCHUtils.findElement(
				TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_FIELD_SERVICE_CONTRACT"), elementText);
		return element;
	}

	public WebElement getLabelFieldServiceContract(String elementText) {
		WebElement element = SCHUtils.findElement(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_FIELD_SERVICE_CONTRACT"), elementText);
		return element;
	}

	public WebElement getLabelFieldService(String elementText) {
		WebElement element = SCHUtils.findElement(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_FIELD_SERVICE_WORK_ORDER"), elementText);
		return element;
	}

	public void contractCreatedWith(String param) {
		FieldServiceContractBean fsBean = (FieldServiceContractBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.FSCONTRACTDETAILS");
		if (param.equals("an invalid Install to UCN")) {
			AssertUtils.assertTextMatches(getLabelFieldServiceContract(fsBean.getInstallToUCN()),
					Matchers.equalTo(fsBean.getInstallToUCN()));
		} else if (param.equals("an invalid Product Number") || param.equals("an inactive Product Number")
				|| param.equals("Product Number which is not a Service Product")) {
			System.out.println("FSBEAN Item Number :: " + fsBean.getItemNumber());
			System.out.println("Element GetText :: " + getLabelFieldServiceContract(fsBean.getItemNumber()).getText());
			Assert.assertTrue(
					getLabelFieldServiceContract(fsBean.getItemNumber()).getText().equals(fsBean.getItemNumber()));
		}
		AssertUtils.assertTextMatches(getLabelFieldServiceContract(fsBean.getContractnumber()),
				Matchers.equalTo(fsBean.getContractnumber()));
		AssertUtils.assertTextMatches(getLabelFieldServiceContract(fsBean.getContractLineNumber()),
				Matchers.equalTo(fsBean.getContractLineNumber()));
		AssertUtils.assertTextMatches(getLabelFieldServiceContract(fsBean.getExternalContractID()),
				Matchers.equalTo(fsBean.getExternalContractID()));
	}

	public void fieldServiceShouldBeCreatedWithTheInvalidInstallToUCN() {
		SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_LABLE_WORK_ORDER_NUMBER"), "");
		WaitUtils.waitForDisplayed(getLableWorkOrderNumber());
		// SCHUtils.scrollElementIntoView(getLableWorkOrderNumber());
		navigateToWorkOrder();
		verifyFieldServiceShouldBeCreatedWithInvalidInstallToUCN();
	}

	private void verifyFieldServiceShouldBeCreatedWithInvalidInstallToUCN() {
		FieldServiceContractBean fsBean = (FieldServiceContractBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.FSCONTRACTDETAILS");
		WebElement dynamicLableFieldService = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_FIELD_SERVICE_CONTRACT"),
				"Contract Number");

		WaitUtils.waitForDisplayed(dynamicLableFieldService);
		AssertUtils.assertTextMatches(dynamicLableFieldService, Matchers.equalTo(fsBean.getContractnumber()));

		dynamicLableFieldService = getLabelFieldServiceContract("External Contract ID");
		AssertUtils.assertTextMatches(dynamicLableFieldService, Matchers.equalTo(fsBean.getExternalContractID()));
	}

	public void contractWasCreatedWithInvalidInstallToUCN() {
		createContractWith("an invalid Install to UCN");
	}

	public void fieldServiceWereCreatedWithInvalidInstallToUCN() {
		contractCreatedWith("an invalid Install to UCN");
		fieldServiceShouldBeCreatedWithTheInvalidInstallToUCN();
	}

	public void contractIsUpdated() {
		getDriver().navigate().back();
		clickOnEditButton("Line Quantity");
		enterUpdatedValueIntoLineQuantityAndSave("Line Quantity", "default");
	}

	public void updateTheLineQuantity(String value) {
		if (value.equals("1")) {
			getDriver().navigate().back();
		}
		clickOnEditButton("Line Quantity");
		enterUpdatedValueIntoLineQuantityAndSave("Line Quantity", value);
	}

	public void enterUpdatedValueIntoLineQuantityAndSave(String elementText, String lineQuantity) {
		WebElement inputEditField = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_INPUT_EDIT_DYNAMIC_FIELD"), elementText);
		WaitUtils.waitForDisplayed(inputEditField);
		String updatedLineQtyValue = null;
		if (lineQuantity.equals("default")) {
			updatedLineQtyValue = ConstantUtils.UPDATE_LINE_QTY_VALUE;
		} else {
			updatedLineQtyValue = lineQuantity;
		}
		TestBaseProvider.getTestBase().getContext().setProperty("UPDATED_LINE_QTY_VALUE", updatedLineQtyValue);
		WaitUtils.waitForDisplayed(inputEditField);
		inputEditField.clear();
		inputEditField.sendKeys(updatedLineQtyValue);
		SCHUtils.pause(2);
		SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_BUTTON_EDIT_SAVE"), "");
		WaitUtils.waitForDisplayed(getButtonEditSave());
		getButtonEditSave().click();
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.waitForLoaderToDismiss();
	}

	public static void clickOnEditButton(String elementText) {
		WebElement editButton = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_BUTTON_EDIT_DYNAMIC_FIELD"), elementText);
		WaitUtils.waitForDisplayed(editButton);
		SCHUtils.clickUsingJavaScript(editButton);
	}

	public void contractShouldContainTheUpdatedValuesWithInvalidInstallToUCN() {
		String updatedValue = TestBaseProvider.getTestBase().getContext().getString("UPDATED_LINE_QTY_VALUE");
		WebElement editedField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getContext().getString("FSC_LABEL_EDITED_NUMBER_FIELD"),
				"Line Quantity");
		WaitUtils.waitForDisplayed(editedField);
		AssertUtils.assertTextMatches(editedField, Matchers.equalTo(updatedValue));
	}

	public void fieldServiceShouldContainTheUpdatedValues() {
		WaitUtils.waitForDisplayed(getLinkWorkOrdereViewAll());
		getLinkWorkOrdereViewAll().click();
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.waitForLoaderToDismiss();
		int size = getWoListLinkWorkOrder().size();
		System.out.println("size :: " + size + " TestBase Provider Value ::  "
				+ TestBaseProvider.getTestBase().getString("UPDATED_LINE_QTY_VALUE"));
		assertEquals(size, Integer.parseInt(TestBaseProvider.getTestBase().getString("UPDATED_LINE_QTY_VALUE")),
				"Field Services Update Fail");
	}

	public void contractShouldBeCreatedWithInvalidProductNumberThatIsNotMapped() {
		contractCreatedWith("an invalid Install to UCN");
	}

	public void fieldServicesShouldBeCreatedWithTheInvalidProductNumberThatIsNotMapped() {
		fieldServiceShouldBeCreatedWithTheInvalidInstallToUCN();
	}

	public void contractShouldBeFlaggedAsPending() {

	}

	public void fieldServiceShouldBeFlaggedAsPending() {

	}

	public void contractWasCreatedWithAnInvalildProductNumber() {
		createContractWith("an invalid Product Number");
		contractCreatedWith("an invalid Product Number");
		fieldServiceShouldBeCreatedWithTheInvalidInstallToUCN();
	}

	public void contractShouldContainUpdatedValueWithInvalidProductNumberThatIsNotMapped() {
		contractShouldContainTheUpdatedValuesWithInvalidInstallToUCN();
	}

	public void fieldServiceShouldContainTheUpdatedValuesWithTheInvalidProductNumberThatIsNotMapped() {
		fieldServiceShouldBeCreatedWithTheInvalidInstallToUCN();
	}

	public void productNumberInTheContractShouldBeMappedToTheCreatedProductNumber() {
		navigateToFieldServiceContractPage();
		navigateToPreviouslyCreatedFieldServiceContract();
		verifyProductNumberIsMapped();
	}

	private void verifyProductNumberIsMapped() {
		SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_LABLE_WORK_ORDER_NUMBER"), "");
		WaitUtils.waitForDisplayed(getLableWorkOrderNumber());
		SCHUtils.scrollElementIntoView(getLableWorkOrderNumber());
		SCHUtils.waitForAjaxToComplete();
		String productName = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_PRODUCT_FOR_INVALID_PRODUCT");
		List<WebElement> productNumberList = SCHUtils
				.findElements(TestBaseProvider.getTestBase().getString("FSC_LINK_DYNAMIC_SERVICE_LIST"), productName);
		int size = productNumberList.size();
		int minVal = (size > 3) ? 3 : size;
		System.out.println("size ::" + size);
		if (size == 0) {
			Assert.fail(productName + " is not mapped.");
		}
		for (int i = 0; i < minVal; i++) {
			AssertUtils.assertTextMatches(productNumberList.get(i), Matchers.equalTo(productName));
		}
	}

	private void navigateToPreviouslyCreatedFieldServiceContract() {
		String fieldServiceContractNumber = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CONTRACT_CREATED_NUMBER");
		WebElement dynamicFieldServiceContractLink = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getContext().getString("FSC_LINK_DYANAMIC_FIELD_SERVICE_CONTRACT"),
				fieldServiceContractNumber);
		dynamicFieldServiceContractLink.click();
		SCHUtils.waitForAjaxToComplete();
	}

	public void contractAlreadyExistedWithAnInvalidProductNumber() {
		createContractWith("an invalid Product Number");
		contractCreatedWith("an invalid Product Number");
	}

	public void productnumberInTheFieldServiceShouldBeMappedToCreatedProductNumber() {
		navigateToFirstServiceContract();
		verifyFieldServiceShouldBeMapped();
	}

	private void verifyFieldServiceShouldBeMapped() {
		String productName = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_PRODUCT_FOR_INVALID_PRODUCT");
		WebElement serviceName = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getContext().getString("FSC_LINK_DYNAMIC_SERVICE_NAME"), productName);
		WaitUtils.waitForDisplayed(serviceName);
		AssertUtils.assertTextMatches(serviceName, Matchers.equalTo(productName));
	}

	private void navigateToFirstServiceContract() {
		SCHUtils.waitForAjaxToComplete();
		int size = getLinkWorkOrderList().size();
		if (size > 0) {
			WaitUtils.waitForDisplayed(getLinkWorkOrderList().get(0));
			SCHUtils.clickUsingJavaScript(getLinkWorkOrderList().get(0));
			SCHUtils.waitForAjaxToComplete();
		} else {
			Assert.fail("No Work Order is here");
		}
	}

	public void contractAlreadyExistedWithAnInvalidInstallToUCN() {
		createContractWith("an invalid Install to UCN");
	}

	public void theInstallToUCNIntheContractShouldBeMappedToCreatedClient() {
		navigateToFieldServiceContractPage();
		navigateToPreviouslyCreatedFieldServiceContract();
		verifyContractMappedToCreatedClient();
	}

	private void verifyContractMappedToCreatedClient() {
		SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_LABLE_WORK_ORDER_NUMBER"), "");
		WaitUtils.waitForDisplayed(getLableWorkOrderNumber());
		SCHUtils.scrollElementIntoView(getLableWorkOrderNumber());
		SCHUtils.waitForAjaxToComplete();
		String clientName = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_CLIENT_ACCOUNT_FOR_INVALID_UCN");
		System.out.println("Client Name :: " + clientName);
		List<WebElement> productNumberList = SCHUtils
				.findElements(TestBaseProvider.getTestBase().getString("FSC_LINK_DYNAMIC_SERVICE_LIST"), clientName);
		int size = productNumberList.size();
		int minVal = (size > 3) ? 3 : size;
		System.out.println("size of Created Client ::" + size + " Min Value :: " + minVal);
		if (minVal <= 0) {
			Assert.fail("Client Contract is Not Mapped");
		} else {
			for (int i = 0; i < minVal; i++) {
				AssertUtils.assertTextMatches(productNumberList.get(i), Matchers.equalTo(clientName));
			}
		}
	}

	public void installToUCNInFieldServicesShouldBeMappedToTheCreatedClient() {
		navigateToFirstServiceContract();
		verifyFieldServiceMappedWithCreatedClient();
		verifyFieldServiceShouldBeMappedToInvalidInstallToUCN();
	}

	public void verifyFieldServiceShouldBeMappedToInvalidInstallToUCN() {
		String accountName = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_CLIENT_ACCOUNT_FOR_INVALID_UCN");
		WebElement serviceName = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getContext().getString("FSC_LINK_DYNAMIC_ACCOUNT_NAME"), accountName);
		WaitUtils.waitForDisplayed(serviceName);
		AssertUtils.assertTextMatches(serviceName, Matchers.equalTo(accountName));
	}

	private void verifyFieldServiceMappedWithCreatedClient() {
		String productName = (String) TestBaseProvider.getTestBase().getContext()
				.getProperty("CREATED_CLIENT_ACCOUNT_FOR_INVALID_UCN");
		WebElement serviceName = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getContext().getString("FSC_LINK_DYNAMIC_ACCOUNT_NAME"), productName);
		WaitUtils.waitForDisplayed(serviceName);
		AssertUtils.assertTextMatches(serviceName, Matchers.equalTo(productName));
	}

	public void fieldServicesShouldBeCreatedWithTheProductNumberWhichIsNotServiceProduct() {
		SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_LABLE_WORK_ORDER_NUMBER"), "");
		WaitUtils.waitForDisplayed(getLableWorkOrderNumber());
		SCHUtils.scrollElementIntoView(getLableWorkOrderNumber());

		navigateToWorkOrder();

		FieldServiceContractBean fsBean = (FieldServiceContractBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.FSCONTRACTDETAILS");
		WebElement dynamicLableFieldService = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_FIELD_SERVICE_WORK_ORDER"),
				fsBean.getContractnumber());
		WaitUtils.waitForDisplayed(dynamicLableFieldService);
		AssertUtils.assertTextMatches(dynamicLableFieldService, Matchers.equalTo(fsBean.getContractnumber()));

		dynamicLableFieldService = getLabelFieldService(fsBean.getItemNumber());
		AssertUtils.assertTextMatches(dynamicLableFieldService, Matchers.equalTo(fsBean.getItemNumber()));

		dynamicLableFieldService = getLabelFieldService(fsBean.getExternalContractID());
		AssertUtils.assertTextMatches(dynamicLableFieldService, Matchers.equalTo(fsBean.getExternalContractID()));
	}

	public void contractShouldBeCreatedWithoutDates() {
		WebElement startDate = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_CONTRACT_FROM_LABEL"), "Start Date");
		WebElement endDate = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_CONTRACT_FROM_LABEL"), "End Date");
		AssertUtils.assertNotDisplayed(startDate);
		AssertUtils.assertNotDisplayed(endDate);
	}

	public void fieldServicesShouldBeCreatedWithoutDates() {
		navigateToWorkOrder();
		verifyFieldServicesShouldBeCreatedWithoutDates();
	}

	private void verifyFieldServicesShouldBeCreatedWithoutDates() {
		WebElement startDate = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_CONTRACT_FROM_LABEL"),
				"Contract Start Date");
		WebElement endDate = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_CONTRACT_FROM_LABEL"), "Contract End Date");
		AssertUtils.assertNotDisplayed(startDate);
		AssertUtils.assertNotDisplayed(endDate);
	}

	private void navigateToWorkOrder() {
		SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_LABLE_WORK_ORDER_NUMBER"), "");
		WaitUtils.waitForDisplayed(getLableWorkOrderNumber());
		SCHUtils.scrollElementIntoView(getLableWorkOrderNumber());
		SCHUtils.waitForAjaxToComplete();
		try {
			WaitUtils.waitForDisplayed(getLinkWorkOrderList().get(0));
			SCHUtils.clickUsingJavaScript(getLinkWorkOrderList().get(0));
			SCHUtils.waitForAjaxToComplete();
			SCHUtils.pause(5);
		} catch (Exception e) {
			Assert.fail("There is no work order.");
		}
	}

	public void contractAlreadyExistedWithBlankstartEndDate() {
		createContractWith("blank start-end dates");
	}

	public void contractIsUpdatedWithValidStartEndDates() {
		updateValidDate("Start Date");
		updateValidDate("End Date");
	}

	private void updateValidDate(String string) {
		String date = null;
		if (string.equals("Start Date")) {
			clickOnEditButton(string);
			date = getDate("1");
		} else if (string.equals("End Date")) {
			clickOnEditButton(string);
			date = getDate("2");
		} else {
			Assert.fail("Invalid Param Passed.");
		}
		WebElement inputEditField = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_INPUT_EDIT_DYNAMIC_DATE_FIELD"), string);
		WaitUtils.waitForDisplayed(inputEditField);
		TestBaseProvider.getTestBase().getContext()
				.setProperty("UPDATED_" + string.replace(" ", "_").toUpperCase() + "_VALUE", date);
		WaitUtils.waitForDisplayed(inputEditField);
		inputEditField.clear();
		inputEditField.sendKeys(date);
		SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_BUTTON_EDIT_SAVE"), "");
		WaitUtils.waitForDisplayed(getButtonEditSave());
		getButtonEditSave().click();
		SCHUtils.pause(2);
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.waitForLoaderToDismiss();
	}

	public static String getDate(String string) {
		DateFormat dateFormat = new SimpleDateFormat("M/d/YYYY");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, Integer.parseInt(string));
		String day = dateFormat.format(cal.getTime());
		return day;
	}

	public void startEndDatesIntheContractShouldBePopulated() {
		WebElement startDate = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_CONTRACT_FROM_LABEL"), "Start Date");
		WaitUtils.waitForDisplayed(startDate);
		WebElement endDate = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_CONTRACT_FROM_LABEL"), "End Date");
		AssertUtils.assertTextMatches(startDate,
				Matchers.equalTo(TestBaseProvider.getTestBase().getString("UPDATED_START_DATE_VALUE")));
		AssertUtils.assertTextMatches(endDate,
				Matchers.equalTo(TestBaseProvider.getTestBase().getString("UPDATED_END_DATE_VALUE")));
	}

	public void startEndDatesInFieldServicesShouldBePopulated() {
		navigateToWorkOrder();
		startEndDateInFieldServiceShouldBePopulated();
	}

	private void startEndDateInFieldServiceShouldBePopulated() {
		WebElement startDate = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_CONTRACT_FROM_LABEL"),
				"Contract Start Date");
		WaitUtils.waitForDisplayed(startDate);
		WebElement endDate = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_CONTRACT_FROM_LABEL"), "Contract End Date");
		AssertUtils.assertTextMatches(startDate,
				Matchers.equalTo(TestBaseProvider.getTestBase().getString("UPDATED_START_DATE_VALUE")));
		AssertUtils.assertTextMatches(endDate,
				Matchers.equalTo(TestBaseProvider.getTestBase().getString("UPDATED_END_DATE_VALUE")));
	}

	public void contractWasCreatedWithProductNumberIsNotServiceProduct() {
		createContractWith("an Product Number which is not a Service Product");
		contractCreatedWith("an Product Number which is not a Service Product");
		fieldServicesShouldBeCreatedWithTheProductNumberWhichIsNotServiceProduct();

	}

	public void contractShouldContainTheUpdatedValuesWithTheProductNumberWhichIsNotServiceProduct() {
		String updatedValue = TestBaseProvider.getTestBase().getContext().getString("UPDATED_LINE_QTY_VALUE");
		WebElement editedField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getContext().getString("FSC_LABEL_EDITED_NUMBER_FIELD"),
				"Line Quantity");
		WaitUtils.waitForDisplayed(editedField);
		AssertUtils.assertTextMatches(editedField, Matchers.equalTo(updatedValue));
	}

	public void fieldServiceShouldContainTheUpdatedValuesWithTheProductNumberWhichIsNotServiceProduct() {
		fieldServiceShouldContainTheUpdatedValues();
	}

	public void fieldServiceShouldBecreatedWithTheInactiveProductNumber() {
		SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_LABLE_WORK_ORDER_NUMBER"), "");
		WaitUtils.waitForDisplayed(getLableWorkOrderNumber());
		SCHUtils.scrollElementIntoView(getLableWorkOrderNumber());
		navigateToWorkOrder();
		verifyFieldServiceCreatedWithTheInactiveProductNumber();
	}

	private void verifyFieldServiceCreatedWithTheInactiveProductNumber() {
		FieldServiceContractBean fsBean = (FieldServiceContractBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("testexecution.FSCONTRACTDETAILS");
		WebElement dynamicLableFieldService = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_FIELD_SERVICE_WORK_ORDER"),
				fsBean.getContractnumber());
		WaitUtils.waitForDisplayed(dynamicLableFieldService);
		AssertUtils.assertTextMatches(dynamicLableFieldService, Matchers.equalTo(fsBean.getContractnumber()));

		dynamicLableFieldService = getLabelFieldService(fsBean.getItemNumber());
		AssertUtils.assertTextMatches(dynamicLableFieldService, Matchers.equalTo(fsBean.getItemNumber()));

		dynamicLableFieldService = getLabelFieldService(fsBean.getExternalContractID());
		AssertUtils.assertTextMatches(dynamicLableFieldService, Matchers.equalTo(fsBean.getExternalContractID()));
	}

	public void contractShouldContainTheUpdatedValuesWithInactiveProductNumber() {
		String updatedValue = TestBaseProvider.getTestBase().getContext().getString("UPDATED_LINE_QTY_VALUE");
		WebElement editedField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getContext().getString("FSC_LABEL_EDITED_NUMBER_FIELD"),
				"Line Quantity");
		WaitUtils.waitForDisplayed(editedField);
		AssertUtils.assertTextMatches(editedField, Matchers.equalTo(updatedValue));
	}

	public void fieldserviceShouldContainTheUpdatedValuesWithInactiveProductNumber() {
		fieldServiceShouldContainTheUpdatedValues();
	}

	public void contractAlreadyExisted() {
		createContractWith("an invalid Install to UCN");
	}

	public void installtoUCNIsUpdated() {
		clickOnEditButton("Install To UCN");
		enterUpdatedValueAndSave("Install To UCN");
	}

	private void enterUpdatedValueAndSave(String elementText) {
		WebElement inputEditField = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_INPUT_EDIT_DYNAMIC_FIELD"), elementText);
		WaitUtils.waitForDisplayed(inputEditField);
		String updatedValue = RandomDataUtil.getRandomNumber(5);
		TestBaseProvider.getTestBase().getContext()
				.setProperty("UPDATED_" + elementText.replace(" ", "_").toUpperCase() + "_VALUE", updatedValue);
		WaitUtils.waitForDisplayed(inputEditField);
		inputEditField.clear();
		inputEditField.sendKeys(updatedValue);
		SCHUtils.pause(2);
		SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_BUTTON_EDIT_SAVE"), "");
		WaitUtils.waitForDisplayed(getButtonEditSave());
		getButtonEditSave().click();
		SCHUtils.waitForAjaxToComplete();
		SCHUtils.waitForLoaderToDismiss();
	}

	private void verifyUpdatedValue(String elementText) {
		String updatedValue = TestBaseProvider.getTestBase().getContext()
				.getString("UPDATED_" + elementText.replace(" ", "_").toUpperCase() + "_VALUE");
		WebElement editedField = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getContext().getString("FSC_LABEL_EDITED_FIELD"), elementText);
		WaitUtils.waitForDisplayed(editedField);
		AssertUtils.assertTextMatches(editedField, Matchers.equalTo(updatedValue));
	}

	public void intallToUCNInContractShouldBeChanged() {
		verifyUpdatedValue("Install To UCN");
	}

	public void updateField(String field) {
		if (field.equals("Line Quantity")) {
			clickOnEditButton("Line Quantity");
			enterUpdatedValueIntoLineQuantityAndSave("Line Quantity", "default");
		} else if (field.equals("Contract Dates")) {
			contractIsUpdatedWithValidStartEndDates();
		} else if (field.equals("Install to UCN")) {
			clickOnEditButton("Install To UCN");
			enterUpdatedValueAndSave("Install To UCN");
		} else {
			Assert.fail("Wrong field passed for updatation.");
		}
	}

	public void verifyUpdatedField(String field) {
		if (field.equals("Line Quantity")) {
			String updatedValue = TestBaseProvider.getTestBase().getContext().getString("UPDATED_LINE_QTY_VALUE");
			WebElement editedField = SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getContext().getString("FSC_LABEL_EDITED_NUMBER_FIELD"),
					"Line Quantity");
			WaitUtils.waitForDisplayed(editedField);
			AssertUtils.assertTextMatches(editedField, Matchers.equalTo(updatedValue));
		} else if (field.equals("Contract Dates")) {
			startEndDatesIntheContractShouldBePopulated();
		} else if (field.equals("Install to UCN")) {
			verifyUpdatedValue("Install To UCN");
		} else {
			Assert.fail("Wrong field passed for verification.");
		}
	}

	public void contractContainsDeliveredFieldServices(String arg1) {
		scrollToWorkOrder();
		WaitUtils.waitForDisplayed(getLinkAllViewed());
		getLinkAllViewed().click();
		for (int i = 0; i < Integer.parseInt(arg1); i++) {
			SCHUtils.waitForAjaxToComplete();
			// SCHUtils.clickUsingJavaScript(getListServiceNumberInWorkOrder().get(i));
			// clickOnEditButton("Status");
			getDynamicDropDown("Status").sendKeys("Delivered");
		}
	}

	public WebElement getDynamicDropDown(String elementText) {
		WebElement element = SCHUtils
				.findElement(TestBaseProvider.getTestBase().getString("FSC_WO_INPUT_DYNAMIC_DROP_DOWN"), elementText);
		return element;
	}

	private void scrollToWorkOrder() {
		SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_LABLE_WORK_ORDER_NUMBER"), "");
		WaitUtils.waitForDisplayed(getLableWorkOrderNumber());
		SCHUtils.scrollElementIntoView(getLableWorkOrderNumber());
		SCHUtils.waitForAjaxToComplete();
	}

	public void contractContrainsOpenStatusFieldServices(String arg1) {
		updateWorkOrder(Integer.parseInt(arg1), "Open");
	}

	public void updateWorkOrder(int numberOFWorkOrder, String orderType) {
		for (int i = 0; i < numberOFWorkOrder; i++) {
			System.out.println("i :: " + i);
			if (orderType.equals("Assigned")) {
				SCHUtils.clickUsingJavaScript(getWoOpenDropDown().get(i));
			} else {
				SCHUtils.clickUsingJavaScript(getWoDropDown().get(i));
			}
			SCHUtils.pause(2);
			SCHUtils.clickUsingJavaScript(getLinkWoEdit());
			SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_FIELD_SERVICE_CONTRACT"),
					"Sales Order Number");
			if (orderType.equals("Assigned")) {
				WebElement consultantElement = SCHUtils.waitForPresent(
						TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_DATE_FIELD_SERVICE_CONTRACT"),
						"Consultant");
				consultantElement.sendKeys(ConstantUtils.ASSIGNED_CONSULTANT);
				SCHUtils.pause(3);
				WebElement suggestionConsultant = SCHUtils.waitForPresent(
						TestBaseProvider.getTestBase().getString("FSC_LINK_SUGGESTION_CONSULTANT"),
						ConstantUtils.ASSIGNED_CONSULTANT);
				WaitUtils.waitForDisplayed(suggestionConsultant);
				SCHUtils.clickUsingJavaScript(suggestionConsultant);
				String serviceDate = getDate("0");
				System.out.println("Service Date ::  " + serviceDate);
				WebElement serviceDateElement = SCHUtils.findElement(
						TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_DATE_FIELD_SERVICE_CONTRACT"),
						"Service Date");
				serviceDateElement.sendKeys(serviceDate);
			} else if (orderType.equals("Delivered")) {
				System.out.println("in Develivered");
				assignedFirst();
				SCHUtils.clickUsingJavaScript(getWoDropDown().get(i));
				SCHUtils.pause(2);
				SCHUtils.clickUsingJavaScript(getLinkWoEdit());
				SCHUtils.waitForPresent(
						TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_FIELD_SERVICE_CONTRACT"),
						"Sales Order Number");
			}
			getLinkWoStatus().click();
			WebElement dynamicSelect = SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getString("FSC_LINK_DYNAMIC_SERVICE_LIST"), orderType);
			SCHUtils.waitForelementToBeClickable(dynamicSelect);
			SCHUtils.scrollElementIntoView(dynamicSelect);
			SCHUtils.clickUsingJavaScript(dynamicSelect);
			getButtonFieldContractSave().click();
			SCHUtils.waitForAjaxToComplete();
		}
		// workOrderNumber += numberOFWorkOrder;
		// System.out.println("Work Order Number :: " + workOrderNumber + "
		// Order Type Param Passed " + orderType);
	}

	public void assignedFirst() {
		WebElement consultantElement = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_DATE_FIELD_SERVICE_CONTRACT"),
				"Consultant");
		consultantElement.sendKeys(ConstantUtils.ASSIGNED_CONSULTANT);
		WebElement suggestionConsultant = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LINK_SUGGESTION_CONSULTANT"),
				ConstantUtils.ASSIGNED_CONSULTANT);
		SCHUtils.clickUsingJavaScript(suggestionConsultant);
		String serviceDate = getDate("0");
		System.out.println("Service Date ::  " + serviceDate);
		WebElement serviceDateElement = SCHUtils.findElement(
				TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_DATE_FIELD_SERVICE_CONTRACT"),
				"Service Date");
		serviceDateElement.sendKeys(serviceDate);
		getLinkWoStatus().click();
		WebElement dynamicSelect = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_LINK_DYNAMIC_SERVICE_LIST"), "Assigned");
		SCHUtils.waitForelementToBeClickable(dynamicSelect);
		SCHUtils.scrollElementIntoView(dynamicSelect);
		SCHUtils.clickUsingJavaScript(dynamicSelect);
		getButtonFieldContractSave().click();
		SCHUtils.waitForAjaxToComplete();
	}

	public void contractIs(String contractStatus) {
		navigateToFieldServiceContractPage();
		navigateToPreviouslyCreatedFieldServiceContract();
		clickOnEditButton("Line Status");
		SCHUtils.waitForAjaxToComplete();
		getLinkLineStatus().click();
		WebElement dynamicSelect = null;
		if (contractStatus.equals("terminated")) {
			dynamicSelect = SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getString("FSC_LINK_DYNAMIC_SERVICE_LIST"), "Terminated");
		} else if (contractStatus.equals("expired")) {
			dynamicSelect = SCHUtils.waitForPresent(
					TestBaseProvider.getTestBase().getString("FSC_LINK_DYNAMIC_SERVICE_LIST"), "Expired");
		} else {
			Assert.fail("Wrong Param Passed.");
		}
		SCHUtils.waitForelementToBeClickable(dynamicSelect);
		SCHUtils.scrollElementIntoView(dynamicSelect);
		SCHUtils.clickUsingJavaScript(dynamicSelect);
		SCHUtils.clickUsingJavaScript(getButtonEditSave());
		SCHUtils.waitForAjaxToComplete();
	}

	public void contractContainsAssignedStatusFieldServices(String arg1) {
		updateWorkOrder(Integer.parseInt(arg1), "Assigned");
	}

	public void contractContainsDeliveredStatusFieldServices(String arg1) {
		updateWorkOrder(Integer.parseInt(arg1), "Delivered");
	}

	public void contractContainsCancelledStatusFieldServices(String arg1) {
		updateWorkOrder(Integer.parseInt(arg1), "Cancelled");
	}

	public void contractContainsCancelledDeliveredStatusFieldServices(String arg1) {
		updateWorkOrder(Integer.parseInt(arg1), "Cancelled/Delivered");
		workOrderNumber = 0;
	}

	public void clickOnViewAllWOrkOrders() {
		// navigateToFieldServiceContractPage();
		// String fieldServiceContractNumber = "FSC-2293";
		// WebElement dynamicFieldServiceContractLink = SCHUtils.waitForPresent(
		// TestBaseProvider.getTestBase().getContext().getString("FSC_LINK_DYANAMIC_FIELD_SERVICE_CONTRACT"),
		// fieldServiceContractNumber);
		// dynamicFieldServiceContractLink.click();
		// SCHUtils.waitForAjaxToComplete();

		scrollToWorkOrder();
		getLinkWorkOrdereViewAll().click();
		SCHUtils.waitForAjaxToComplete();
	}

	public void contractShouldContainCancelledStatusFieldServices(String arg1) {
		int size = getListWoCancelled().size();
		Assert.assertEquals(size, Integer.parseInt(arg1),
				"Contract not contain " + arg1 + "  cancelled field services.");
	}

	public void contractShouldContainCancelledDeliveredStatusFieldDelivered(String arg1) {
		int size = getListWoCancelledDelivered().size();
		Assert.assertEquals(size, Integer.parseInt(arg1),
				"Contract not contain " + arg1 + " cancelled/delivered field services.");
	}

	public void contractShouldContainCancelledDeliveredStatusField(String arg1) {

	}

	public void the_Line_Item_Quantity_is_updated_to(String arg1) {
		navigateToFieldServiceContractPage();
		navigateToPreviouslyCreatedFieldServiceContract();
		clickOnEditButton("Line Quantity");
		enterUpdatedValueIntoLineQuantityAndSave("Line Quantity", arg1);
	}

	public void the_contract_should_contain_open_status_field_services(String arg1) {
		int size = getListWoOpen().size();
		Assert.assertEquals(size, Integer.parseInt(arg1), "Contract not contain " + arg1 + " Open field services.");
	}

	public void the_contract_should_contain_assigned_status_field_services(String arg1) {
		int size = getListWoAssigned().size();
		Assert.assertEquals(size, Integer.parseInt(arg1), "Contract not contain " + arg1 + " Assigned field services.");
	}

	public void the_contract_should_contain_delivered_status_field_services(String arg1) {
		int size = getListWoDelivered().size();
		Assert.assertEquals(size, Integer.parseInt(arg1),
				"Contract not contain " + arg1 + " Delivered field services.");
	}

	public void the_contract_should_contain_cancelled_delivered_status_field_services(String arg1) {
		int size = getListWoCancelledDelivered().size();
		Assert.assertEquals(size, Integer.parseInt(arg1),
				"Contract not contain " + arg1 + " Cancelled/Delivered field services.");
	}

	public void a_contract_is_created() {
		TestBaseProvider.getTestBase().getContext().setProperty("FSC_LINE_QUANTITY", "1");
		navigateToFieldServiceContractPage();
		getLinkNew().click();
		createFieldServiceContract("Line Quantity");
	}

	public void one_of_the_services_is_assigned() {
		scrollToWorkOrder();
		getLinkWorkOrdereViewAll().click();
		SCHUtils.waitForAjaxToComplete();
		updateWorkOrder(1, "Assigned");
		FieldServiceContractPage.workOrderNumber = 0;
	}

	public void the_same_service_is_delivered_with_the_service_date_that_is(String date) {
		SCHUtils.clickUsingJavaScript(getWoDropDown().get(0));
		SCHUtils.pause(2);
		SCHUtils.clickUsingJavaScript(getLinkWoEdit());
		SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_FIELD_SERVICE_CONTRACT"),
				"Sales Order Number");
		getLinkWoStatus().click();
		WebElement dynamicSelect = SCHUtils
				.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_LINK_DYNAMIC_SERVICE_LIST"), "Delivered");
		SCHUtils.waitForelementToBeClickable(dynamicSelect);
		SCHUtils.scrollElementIntoView(dynamicSelect);
		SCHUtils.clickUsingJavaScript(dynamicSelect);
		String serviceDate = null;
		if (date.equals("todays date")) {
			serviceDate = getDate("0");
		} else {
			serviceDate = getDate("-5");
		}
		WebElement serviceDateElement = SCHUtils.findElement(
				TestBaseProvider.getTestBase().getString("FSC_INPUT_DYNAMIC_DATE_FIELD_SERVICE_CONTRACT"),
				"Service Date");
		serviceDateElement.clear();
		serviceDateElement.sendKeys(serviceDate);
		getButtonFieldContractSave().click();
		SCHUtils.waitForAjaxToComplete();
	}

	public void the_Revenue_Message_field_should_be_populated_with_todays_date() {
		SCHUtils.clickUsingJavaScript(getListServiceNumberInWorkOrder().get(0));
		SCHUtils.waitForAjaxToComplete();
		WebElement revenueMessageElement = SCHUtils.findElement(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_FIELD_SERVICE_CONTRACT"),
				"Revenue Message");
		String todaysDate = getDate("0");
		AssertUtils.assertTextMatches(revenueMessageElement, Matchers.equalTo(todaysDate));
	}

	public void contractShouldBeCreatedWithProductNumberWhichIsNotServiceProductFromOTC() {
		OTCXMLBean otcXMLBean = (OTCXMLBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("OTC_CONTRACT_CREATION_VALUE");
		String contractNumber = otcXMLBean.getCorpIDAndContractNumber();
		HomePage homepage = new HomePage();
		homepage.searchforContract(contractNumber);
		homepage.verifyContractPresent(contractNumber);
		homepage.verifyContractDetails(otcXMLBean);
	}

	public void fieldServicesShouldBeCreatedWithProductNumberWhichIsNotServiceProductFromOTC() {
		SCHUtils.waitForPresent(TestBaseProvider.getTestBase().getString("FSC_LABLE_WORK_ORDER_NUMBER"), "");
		WaitUtils.waitForDisplayed(getLableWorkOrderNumber());
		SCHUtils.scrollElementIntoView(getLableWorkOrderNumber());

		navigateToWorkOrder();

		OTCXMLBean otcXMLBean = (OTCXMLBean) TestBaseProvider.getTestBase().getContext()
				.getProperty("OTC_CONTRACT_CREATION_VALUE");

		WebElement dynamicLableFieldService = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_FIELD_SERVICE_WORK_ORDER"),
				otcXMLBean.getCorpIDAndContractNumber());
		WaitUtils.waitForDisplayed(dynamicLableFieldService);
		AssertUtils.assertTextMatches(dynamicLableFieldService,
				Matchers.equalTo(otcXMLBean.getCorpIDAndContractNumber()));

		dynamicLableFieldService = getLabelFieldService(otcXMLBean.getItemNumber());
		AssertUtils.assertTextMatches(dynamicLableFieldService, Matchers.equalTo(otcXMLBean.getItemNumber()));

		dynamicLableFieldService = getLabelFieldService(otcXMLBean.getCorpIDAndContractNumber() + "~2");
		AssertUtils.assertTextMatches(dynamicLableFieldService,
				Matchers.equalTo(otcXMLBean.getCorpIDAndContractNumber() + "~2"));

		String contractStartDate = HomePage.returnSFDCFormattedDate(otcXMLBean.getStartDate());
		String contractEndDate = HomePage.returnSFDCFormattedDate(otcXMLBean.getEndDate());

		WebElement startDate = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_CONTRACT_FROM_LABEL"),
				"Contract Start Date");
		WaitUtils.waitForDisplayed(startDate);
		WebElement endDate = SCHUtils.waitForPresent(
				TestBaseProvider.getTestBase().getString("FSC_LABEL_DYNAMIC_CONTRACT_FROM_LABEL"), "Contract End Date");

		AssertUtils.assertTextMatches(startDate, Matchers.equalTo(contractStartDate));
		AssertUtils.assertTextMatches(endDate, Matchers.equalTo(contractEndDate));

	}

	public void contractShouldBeCreatedWithInactiveProductNumberFromOTC() {
		contractShouldBeCreatedWithProductNumberWhichIsNotServiceProductFromOTC();
	}

	public void fieldServiceShouldbeCreatedWithInactiveProductNumberFromOTC() {
		fieldServicesShouldBeCreatedWithProductNumberWhichIsNotServiceProductFromOTC();
	}

	public void updateContractStatusFromOTC(String contractStatus) {
		OTCXMLGenerate OTCXML = new OTCXMLGenerate();
		OTCXML.updateContractNumberInOTCXML(contractStatus);
		OTCXML.placeContractStatusOTCXMLIntoActiveMQPath(contractStatus);
	}

	public void updateLineItemQuantityThroughOTC(String updatedValue) {
		OTCXMLGenerate OTCXML = new OTCXMLGenerate();
		OTCXML.updateTheLineQuantityInOTCXML(updatedValue);
		OTCXML.placeUpdatedLineQuantityOTCXMLIntoActiveMQPath();
	}

}
