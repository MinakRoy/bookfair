package com.scholastic.salesforce.automation.support;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.scholastic.salesforce.automation.beans.InstituteBean;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.TestBaseProvider;

public class SCHUtils {
	static WebDriverWait wait = null;

	public static void pause(long seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
		}
	}

	public static void waitForAjaxToComplete() {
		try {
			(new WebDriverWait(TestBaseProvider.getTestBase().getDriver(),
					Integer.parseInt(TestBaseProvider.getTestBase().getContext().getString("wait.timeout.sec"))))
							.until(new ExpectedCondition<Boolean>() {
								@Override
								public Boolean apply(WebDriver d) {
									JavascriptExecutor js = (JavascriptExecutor) TestBaseProvider.getTestBase()
											.getDriver();
									return (Boolean) js
											.executeScript("return !!window.jQuery && window.jQuery.active == 0");

								}
							});
		} catch (Exception e) {
		}
	}

	public static void waitForLoaderToDismiss() {
		wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(),
				Integer.parseInt(TestBaseProvider.getTestBase().getContext().getString("wait.timeout.sec")));
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver wdriver) {
					return ((JavascriptExecutor) TestBaseProvider.getTestBase().getDriver())
							.executeScript("return document.readyState").equals("complete");
				}
			});
		} catch (Exception e) {
			System.out.println("Waiting for Loader to dismiss timed out");
		}
	}

	public static void waitForelementToBeClickable(final WebElement webElement) {
		wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 30);
		wait.until(ExpectedConditions.elementToBeClickable(webElement));
	}

	public static WebElement waitForElementToBeClickable(WebElement element, long timeOut) {
		try {
			WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), timeOut);
			element = wait.until(ExpectedConditions.elementToBeClickable(element));
		} catch (Exception e) {
		}
		return element;
	}

	/**
	 * this method will return WebElement for dynamic locators which needs
	 * String format
	 *
	 * @param loc
	 *            -Locator
	 */
	public static WebElement findElement(String loc, String... value) {
		switch (value.length) {
		case 0:
			return TestBaseProvider.getTestBase().getDriver().findElement(LocatorUtils.getBy(loc));
		case 1:
			try {
				return TestBaseProvider.getTestBase().getDriver()
						.findElement(LocatorUtils.getBy(String.format(loc, value[0])));

			} catch (Exception e) {
				return TestBaseProvider.getTestBase().getDriver()
						.findElement(LocatorUtils.getBy(String.format(loc, value[0].toUpperCase(Locale.ENGLISH))));
			}

		case 2:
			return TestBaseProvider.getTestBase().getDriver()
					.findElement(LocatorUtils.getBy(String.format(loc, value[0], value[1])));

		case 3:
			return TestBaseProvider.getTestBase().getDriver()
					.findElement(LocatorUtils.getBy(String.format(loc, value[0], value[1], value[2])));

		default:
			return null;
		}

	}

	public static List<WebElement> findElements(String loc, String... value) {
		switch (value.length) {
		case 0:
			return TestBaseProvider.getTestBase().getDriver().findElements(LocatorUtils.getBy(loc));
		case 1:
			try {
				return TestBaseProvider.getTestBase().getDriver()
						.findElements(LocatorUtils.getBy(String.format(loc, value[0])));
			} catch (Exception e) {
			}

		case 2:
			return TestBaseProvider.getTestBase().getDriver()
					.findElements(LocatorUtils.getBy(String.format(loc, value[0], value[1])));

		case 3:
			return TestBaseProvider.getTestBase().getDriver()
					.findElements(LocatorUtils.getBy(String.format(loc, value[0], value[1], value[2])));

		default:
			return null;
		}

	}

	public static void clickUsingActions(WebElement ele) {
		Actions actions = new Actions(TestBaseProvider.getTestBase().getDriver());
		actions.moveToElement(ele).click().build().perform();
	}

	public static void clickUsingJavaScript(WebElement ele) {
		((JavascriptExecutor) TestBaseProvider.getTestBase().getDriver()).executeScript("arguments[0].click()", ele);
	}

	public static WebElement waitForPresent(String loc, String... value) {
		WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 30);
		switch (value.length) {
		case 0:
			return wait.until(ExpectedConditions.presenceOfElementLocated(LocatorUtils.getBy(loc)));
		case 1:
			return wait.until(
					ExpectedConditions.presenceOfElementLocated(LocatorUtils.getBy(String.format(loc, value[0]))));
		case 2:
			return wait.until(ExpectedConditions
					.presenceOfElementLocated(LocatorUtils.getBy(String.format(loc, value[0], value[1]))));
		case 3:
			return wait.until(ExpectedConditions
					.presenceOfElementLocated(LocatorUtils.getBy(String.format(loc, value[0], value[1], value[2]))));
		default:
			return null;
		}
	}

	public static void scrollElementIntoView(WebElement element) {
		((JavascriptExecutor) TestBaseProvider.getTestBase().getDriver())
				.executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public static void setChildDummyProperty() {
		TestBaseProvider.getTestBase().getContext().setProperty("CREATED_INSTITUTE_UCN_CHILD", "641888712");
		InstituteBean insBean = InstituteBean.getInstituteDataBean();
		insBean.setInstituteName("TEST ZULEKYZS INSTITUTE");
		TestBaseProvider.getTestBase().getContext().setProperty("testexecution.INSTITUTEDETAILS_CHILD", insBean);
	}

	public static void setFirstDummyProperty() {
		TestBaseProvider.getTestBase().getContext().setProperty("CREATED_INSTITUTE_UCN_FIRST", "641888711");
		InstituteBean insBean = InstituteBean.getInstituteDataBean();
		insBean.setInstituteName("TEST CEFVITHA INSTITUTE");
		TestBaseProvider.getTestBase().getContext().setProperty("testexecution.INSTITUTEDETAILS_FIRST", insBean);
	}

	public static void setSecondDummyProperty() {
		TestBaseProvider.getTestBase().getContext().setProperty("CREATED_INSTITUTE_UCN_SECOND", "641882486");
		InstituteBean insBean = InstituteBean.getInstituteDataBean();
		insBean.setInstituteName("TEST COZWAPGW INSTITUTE");
		TestBaseProvider.getTestBase().getContext().setProperty("testexecution.INSTITUTEDETAILS_SECOND", insBean);
	}

	/**
	 * usage : to wait till data sync CDB to SFDC
	 */

	public static void waitforDataToSyncInCDB() {
		for (int i = 0; i < 18; i++) {
			TestBaseProvider.getTestBase().getDriver().getTitle();
			SCHUtils.pause(50);
		}
	}

	/**
	 * usage : setEST Time to verify in Bulk Job in SFDC
	 */

	public static void setEstTimeForStartCommand() {
		Date date = new Date();
		DateFormat formatter = new SimpleDateFormat("M/d/yyyy H:mm a");
		Calendar cal = Calendar.getInstance();
		formatter.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
		String DayTime = formatter.format(date);
		TestBaseProvider.getTestBase().getContext().setProperty("COMMAND_START_TIME", DayTime);
		System.out.println("Start Command Start Time In Shell :: " + DayTime);
		cal.setTime(date);
		cal.add(Calendar.MINUTE, -1);
		DayTime = formatter.format(cal.getTime());
		TestBaseProvider.getTestBase().getContext().setProperty("COMMAND_START_TIME_MINUS_ONE_MINUTE", DayTime);

		cal.add(Calendar.MINUTE, 1);
		DayTime = formatter.format(cal.getTime());
		TestBaseProvider.getTestBase().getContext().setProperty("COMMAND_START_TIME_PLUS_ONE_MINUTE", DayTime);
		cal.add(Calendar.MINUTE, 1);
		DayTime = formatter.format(cal.getTime());
		TestBaseProvider.getTestBase().getContext().setProperty("COMMAND_START_TIME_PLUS_TWO_MINUTE", DayTime);
	}

	public static void waitforContractToSyncInSFDCFromOTC() {
		for (int i = 0; i < 3; i++) {
			TestBaseProvider.getTestBase().getDriver().getTitle();
			SCHUtils.pause(50);
		}
	}
}
