package com.scholastic.salesforce.automation.support;

public class ConstantUtils {
	public static final int TEMP = 8;
	public static final String LINE_QTY = "3";
	public static final String UPDATE_LINE_QTY_VALUE = "6";
	public static final String INSTITUTE_ADDRESS_ONE = "1601 cottman ave";
	public static final String INSTITUTE_PIN = "19111";
	public static final String INSTITUTE_ZIP4 = "3430";
	public static final String INSTITUTE_ENROLLMENT = "200";
	public static final String INSTITUTE_PHONE = "9898789889";
	public static final String INSTITUTE_CUSTOMER_GROUP = "COLLEGES";
	public static final String INSTITUTE_CUSTOMER_TYPE = "TWO YEAR COLLEGE DEPARTMENT OR SATELLITE OFFICE";
	public static final String INSTITUTE_DETAILS_LIST = "testexecution.INSDETAILS";
	public static final String EDIT_ACCOUNT_NAME = "Updated Account Name";
	public static final String UCN_SEARCH_FOR_ADDRESS_UPDATE = "641870305";
	public static final String UCN_SEARCH_FOR_RELATIONSHIP_UPDATE = "641859675";
	public static final String ENTERPRISE_STATUS_TERMINATED = "TERMINATED";
	public static final String LOC_ADD_POSTAL = "19111";
	public static final String LOC_ADD_ADDRESS1 = "1602 location add";
	public static final String LOC_ADD_ZIP4 = "3480";
	public static final String APPROVE_END_REQUEST = "Approve End Req.";
	public static final String APPROVE_NEW_REQUEST = "Approve New Req.";
	public static final String ACCOUNT_MERGE = "Merge";
	public static final String ACCOUNT_CLOSE = "Close";
	public static final String ACCOUNT_CLOSE_REASON = "School Closed";
	public static final String EDIT_BATCHPROCESSING_ACCOUNT = "CLANTON ELEMENTARY SCHOOL";
	public static final String ACCOUNT_ERROR_PROCESSING_EDIT_MDR_ID = "TEST GXXGCKAH INSTITUTE";
	public static final String USERTYPE_TONI_USERNAME = "slee5-consultant@scholastic.com.chfull.toni";
	public static final String USERTYPE_TONI_PASSWORD = "Welcome1";
	public static final String USERTYPE_CSR_USERNAME = "slee5-consultant@scholastic.com.chfull.csr";
	public static final String USERTYPE_CSR_PASSWORD = "Welcome1";
	public static final String ACCOUNT_TO_EDIT = "TEST DZBYOXAA INSTITUTE";
	public static final String ASSIGNED_CONSULTANT = "TERRAL CESAK";
	public static final String PROCESSED_RECORD = "1";
	public static final String PROCESSED_RECORD_FAIL = "1";
	public static final String PROCESSED_RECORD_FAIL_TO_PAAS = "0";
	public static final String BULKUPDATE_ACCOUNT_UCN = "54405";
	public static final String BULKUPDATE_ACCOUNT_NAME = "Testing bhszisbl Accounts";
	public static final String BULKUPDATE_MULTIPLE_ACCOUNT_UCN = "80494";
	public static final String BULKUPDATE_MULTIPLE_ACCOUNT_NAME = "Testing yeututwk Accounts";
	public static final String SUCCESS_UPDATED_MESSAGE = "Successfully Updated";
	public static final String ASSIGNED_STATUS = "Assigned";
	public static final String DELIVERED_STATUS = "Delivered";
	public static final String TEXT_OPEN_ORDER = "Open";
	public static final String DATA_TO_S3_TERRITORY_CHECK_REPORT = "Data to s3 for Bi - Territory Check";
	public static final String FIRST_UCN_TERRITORY = "600201824";
	public static final String FIRST_TERRITORY_VERIFY[] = { "Randee Salisbury", "Account Executive" };
	public static final String HUNDRED_UCN_TERRITORY = "600382524";
	public static final String HUNDRED_TERRITORY_VERIFY[] = { "Stacy Muir", "Mideast RD" };
	public static final String FIVE_HUNDRED_UCN_TERRITORY = "600230277";
	public static final String FIVE_HUNDRED_TERRITORY_VERIFY[] = { "Ben Woodworth", "Northeast RD" };
	public static final String LAST_UCN_TERRITORY = "600225473";
	public static final String LAST_HUNDRED_TERRITORY_VERIFY[] = { "David Smith", "East Region VP" };
	public static final String[] ACCOUNT_FILE_FIELD = { "ID", "UCN", "AccountName", "Segmentation", "Phone", "Fax",
			"InstitutionStatus", "District", "TypeCustomer", "TypeEnrollment", "Title1Dollars", "SpecialEdFund$",
			"#Schools", "LEPStudent%", "IEPStudent%", "AfricanAmerican %", "AsianAmerican %", "Hispanic American %",
			"NativeAmerican %", "MailingStreet", "MailingCity", "Mailing County", "MailingState/Province",
			"MailingZip/PostalCode", "MailingCountry", "MailingStateDescription" };
	public static final String DISTRICT_FILE_FIELD[] = { "Parent UCN", "Child UCN" };
	public static final String[] USER_FILE_FIELD = { "UserId", "Email", "Name", "Region", "Sub-Region" };
	public static final String[] TERRITORY_FILE_FIELD = { "AccountID", "Territory", "UserID", "RoleInTerritory" };
	public static final String OTC_LINE_QTY = "1";
	public static final String OTC_ACTION_CREATE = "CREATE";
	public static final String OTC_ACTION_UPDATE = "UPDATE";
	public static final String CONTRACT_EXPIRED = "expired";
	public static final String CONTRACT_TERMINATED = "terminated";

}
