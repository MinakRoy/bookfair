package com.scholastic.salesforce.automation.support;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.scholastic.salesforce.automation.beans.OTCXMLBean;
import com.scholastic.torque.common.TestBaseProvider;

public class OTCXMLGenerate {

	public static final String sourcePath = "./src/test/resources/xml/latestcodem.xml";
	public static final String destinationPath = "./src/test/resources/queue/latestcodem.xml";
	public static final String sourcePathNotServiceProduct = "./src/test/resources/xml/NotServiceProduct.xml";
	public static final String sourcePathInvalidProduct = "./src/test/resources/xml/InvalidProduct.xml";
	public static final String sourcePathContractExpired = "./src/test/resources/xml/ContractExpired.xml";
	public static final String sourcePathContractTerminated = "./src/test/resources/xml/ContractTerminated.xml";
	public static final String sourcePathContractLineQuantity = "./src/test/resources/xml/ContractLineQuantity.xml";

	public void generateOTCXMLWithAllValues() {
		String filepath = "./src/test/resources/xml/latestcodem.xml";
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			String corpIDAndContractNumber = RandomDataUtil.getRandomNumber(8);

			Node corpInfo = doc.getElementsByTagName("CORP_INFO").item(0);
			NodeList corpList = corpInfo.getChildNodes();
			for (int i = 0; i < corpList.getLength(); i++) {
				Node node = corpList.item(i);
				if ("CORP_ID_VALUE".equals(node.getNodeName())) {
					node.setTextContent(corpIDAndContractNumber);
				}
			}

			Node contractInfo = doc.getElementsByTagName("CONTRACT_V2_INFO").item(0);

			String contractLinePrice = null;
			String startDate = null;
			String endDate = null;

			OTCXMLBean otcBean = new OTCXMLBean();

			NodeList list = contractInfo.getChildNodes();
			for (int i = 0; i < list.getLength(); i++) {
				Node node = list.item(i);

				if ("CONTRACT_NUMBER".equals(node.getNodeName())) {
					node.setTextContent(corpIDAndContractNumber);
				} else if ("START_DATE".equals(node.getNodeName())) {
					startDate = getDate("0");
					node.setTextContent(startDate);
				} else if ("END_DATE".equals(node.getNodeName())) {
					endDate = getDate("2");
					node.setTextContent(endDate);
				} else if ("LINE_QUANTITY".equals(node.getNodeName())) {
					node.setTextContent(ConstantUtils.OTC_LINE_QTY);
				} else if ("TOTAL_LINE_PRICE".equals(node.getNodeName())) {
					contractLinePrice = RandomDataUtil.getRandomNumber(3);
					node.setTextContent(contractLinePrice);
				} else if ("ACTION".equals(node.getNodeName())) {
					node.setTextContent(ConstantUtils.OTC_ACTION_CREATE);
				} else if ("INSTALL_TO_UCN".equals(node.getNodeName())) {
					otcBean.setInstallToUCN(node.getTextContent());
				} else if ("ISBN".equals(node.getNodeName())) {
					otcBean.setISBN(node.getTextContent());
				} else if ("ITEM_DESCRIPTION".equals(node.getNodeName())) {
					otcBean.setTitle(node.getTextContent());
				} else if ("SALES_ORDER_LINE_NUMBER".equals(node.getNodeName())) {
					otcBean.setSalesOrderLineNumber(node.getTextContent());
				} else if ("SALES_ORDER_NUMBER".equals(node.getNodeName())) {
					otcBean.setSalesOrderNumber(node.getTextContent());
				} else if ("LINE_STATUS".equals(node.getNodeName())) {
					otcBean.setLineStatus(node.getTextContent().toLowerCase());
				} else if ("ITEM_NUMBER".equals(node.getNodeName())) {
					otcBean.setItemNumber(node.getTextContent());
				}

			}

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);

			setOTCBeanValue(otcBean, contractLinePrice, startDate, endDate, corpIDAndContractNumber,
					ConstantUtils.OTC_LINE_QTY, ConstantUtils.OTC_ACTION_CREATE);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	private static void setOTCBeanValue(OTCXMLBean otcBean, String contractLinePrice, String startDate, String endDate,
			String corpIDAndContractNumber, String otcLineQty, String otcActionCreate) {
		otcBean.setContractLinePrice(contractLinePrice);
		otcBean.setStartDate(startDate);
		otcBean.setEndDate(endDate);
		otcBean.setCorpIDAndContractNumber(corpIDAndContractNumber);
		otcBean.setOTCLineQuantity(otcLineQty);
		otcBean.setOTCAction(otcActionCreate);
		// System.out.println("xml generate toString :: " + otcBean.toString());
		TestBaseProvider.getTestBase().getContext().setProperty("OTC_CONTRACT_CREATION_VALUE", otcBean);
	}

	public static String getDate(String string) {
		DateFormat dateFormat = new SimpleDateFormat("YYYY-M-d");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, Integer.parseInt(string));
		String day = dateFormat.format(cal.getTime());
		return day;
	}

	public void placeGeneratedOTCXMLIntoActiveMQPath() {
		copyFile(sourcePath, destinationPath);
	}

	public void copyFile(String sourcePath, String destinationPath) {
		File sourceFile = new File(sourcePath);
		File destinationFile = new File(destinationPath);
		try {
			FileUtils.copyFile(sourceFile, destinationFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void generateOTCXMLWithNotServiceProductNumber() {
		String filepath = "./src/test/resources/xml/NotServiceProduct.xml";
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			String corpIDAndContractNumber = RandomDataUtil.getRandomNumber(8);

			Node corpInfo = doc.getElementsByTagName("CORP_INFO").item(0);
			NodeList corpList = corpInfo.getChildNodes();
			for (int i = 0; i < corpList.getLength(); i++) {
				Node node = corpList.item(i);
				if ("CORP_ID_VALUE".equals(node.getNodeName())) {
					node.setTextContent(corpIDAndContractNumber);
				}
			}

			Node contractInfo = doc.getElementsByTagName("CONTRACT_V2_INFO").item(0);

			String contractLinePrice = null;
			String startDate = null;
			String endDate = null;

			OTCXMLBean otcBean = new OTCXMLBean();

			NodeList list = contractInfo.getChildNodes();
			for (int i = 0; i < list.getLength(); i++) {
				Node node = list.item(i);
				if ("CONTRACT_NUMBER".equals(node.getNodeName())) {
					node.setTextContent(corpIDAndContractNumber);
				} else if ("START_DATE".equals(node.getNodeName())) {
					startDate = getDate("0");
					node.setTextContent(startDate);
				} else if ("END_DATE".equals(node.getNodeName())) {
					endDate = getDate("2");
					node.setTextContent(endDate);
				} else if ("LINE_QUANTITY".equals(node.getNodeName())) {
					node.setTextContent(ConstantUtils.OTC_LINE_QTY);
				} else if ("TOTAL_LINE_PRICE".equals(node.getNodeName())) {
					contractLinePrice = RandomDataUtil.getRandomNumber(3);
					node.setTextContent(contractLinePrice);
				} else if ("ACTION".equals(node.getNodeName())) {
					node.setTextContent(ConstantUtils.OTC_ACTION_CREATE);
				} else if ("INSTALL_TO_UCN".equals(node.getNodeName())) {
					otcBean.setInstallToUCN(node.getTextContent());
				} else if ("ISBN".equals(node.getNodeName())) {
					otcBean.setISBN(node.getTextContent());
				} else if ("ITEM_DESCRIPTION".equals(node.getNodeName())) {
					otcBean.setTitle(node.getTextContent());
				} else if ("SALES_ORDER_LINE_NUMBER".equals(node.getNodeName())) {
					otcBean.setSalesOrderLineNumber(node.getTextContent());
				} else if ("SALES_ORDER_NUMBER".equals(node.getNodeName())) {
					otcBean.setSalesOrderNumber(node.getTextContent());
				} else if ("LINE_STATUS".equals(node.getNodeName())) {
					otcBean.setLineStatus(node.getTextContent().toLowerCase());
				} else if ("ITEM_NUMBER".equals(node.getNodeName())) {
					otcBean.setItemNumber(node.getTextContent());
				}
			}

			setOTCBeanValue(otcBean, contractLinePrice, startDate, endDate, corpIDAndContractNumber,
					ConstantUtils.OTC_LINE_QTY, ConstantUtils.OTC_ACTION_CREATE);

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	public void placeInvalidProductNumberOTCXMLIntoActiveMQPath() {
		copyFile(sourcePathNotServiceProduct, destinationPath);
	}

	public void generateOTCXMLInActiveProductNumber() {
		String filepath = "./src/test/resources/xml/InvalidProduct.xml";
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			String corpIDAndContractNumber = RandomDataUtil.getRandomNumber(8);

			Node corpInfo = doc.getElementsByTagName("CORP_INFO").item(0);
			NodeList corpList = corpInfo.getChildNodes();
			for (int i = 0; i < corpList.getLength(); i++) {
				Node node = corpList.item(i);
				if ("CORP_ID_VALUE".equals(node.getNodeName())) {
					node.setTextContent(corpIDAndContractNumber);
				}
			}

			Node contractInfo = doc.getElementsByTagName("CONTRACT_V2_INFO").item(0);

			String contractLinePrice = null;
			String startDate = null;
			String endDate = null;

			OTCXMLBean otcBean = new OTCXMLBean();

			NodeList list = contractInfo.getChildNodes();
			for (int i = 0; i < list.getLength(); i++) {
				Node node = list.item(i);
				if ("CONTRACT_NUMBER".equals(node.getNodeName())) {
					node.setTextContent(corpIDAndContractNumber);
				} else if ("START_DATE".equals(node.getNodeName())) {
					startDate = getDate("0");
					node.setTextContent(startDate);
				} else if ("END_DATE".equals(node.getNodeName())) {
					endDate = getDate("2");
					node.setTextContent(endDate);
				} else if ("LINE_QUANTITY".equals(node.getNodeName())) {
					node.setTextContent(ConstantUtils.OTC_LINE_QTY);
				} else if ("TOTAL_LINE_PRICE".equals(node.getNodeName())) {
					contractLinePrice = RandomDataUtil.getRandomNumber(3);
					node.setTextContent(contractLinePrice);
				} else if ("ACTION".equals(node.getNodeName())) {
					node.setTextContent(ConstantUtils.OTC_ACTION_CREATE);
				} else if ("INSTALL_TO_UCN".equals(node.getNodeName())) {
					otcBean.setInstallToUCN(node.getTextContent());
				} else if ("ISBN".equals(node.getNodeName())) {
					otcBean.setISBN(node.getTextContent());
				} else if ("ITEM_DESCRIPTION".equals(node.getNodeName())) {
					otcBean.setTitle(node.getTextContent());
				} else if ("SALES_ORDER_LINE_NUMBER".equals(node.getNodeName())) {
					otcBean.setSalesOrderLineNumber(node.getTextContent());
				} else if ("SALES_ORDER_NUMBER".equals(node.getNodeName())) {
					otcBean.setSalesOrderNumber(node.getTextContent());
				} else if ("LINE_STATUS".equals(node.getNodeName())) {
					otcBean.setLineStatus(node.getTextContent().toLowerCase());
				} else if ("ITEM_NUMBER".equals(node.getNodeName())) {
					otcBean.setItemNumber(node.getTextContent());
				}
			}

			setOTCBeanValue(otcBean, contractLinePrice, startDate, endDate, corpIDAndContractNumber,
					ConstantUtils.OTC_LINE_QTY, ConstantUtils.OTC_ACTION_CREATE);

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	public void placeInactiveProductNumberOTCXMLIntoActiveMQPath() {
		copyFile(sourcePathInvalidProduct, destinationPath);
	}

	public void updateContractNumberInOTCXML(String contractStatus) {
		if (contractStatus.equals(ConstantUtils.CONTRACT_EXPIRED)) {
			updateXMLFileAndSave(contractStatus);
		} else if (contractStatus.equals(ConstantUtils.CONTRACT_TERMINATED)) {
			updateXMLFileAndSave(contractStatus);
		} else {
			Assert.fail("Wrong param " + contractStatus + " passed.");
		}
	}

	public void updateXMLFileAndSave(String contractStatus) {
		String filepath = null;
		if (contractStatus.equals(ConstantUtils.CONTRACT_EXPIRED)) {
			filepath = "./src/test/resources/xml/ContractExpired.xml";
		} else if (contractStatus.equals(ConstantUtils.CONTRACT_TERMINATED)) {
			filepath = "./src/test/resources/xml/ContractTerminated.xml";
		}
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			String corpIDAndContractNumber = (String) TestBaseProvider.getTestBase().getContext()
					.getProperty("CONTRACT_NUMBER_OTC_UPDATE");

			System.out.println("corpId and Contract Number :: " + corpIDAndContractNumber);

			Node corpInfo = doc.getElementsByTagName("CORP_INFO").item(0);
			NodeList corpList = corpInfo.getChildNodes();
			for (int i = 0; i < corpList.getLength(); i++) {
				Node node = corpList.item(i);
				if ("CORP_ID_VALUE".equals(node.getNodeName())) {
					node.setTextContent(corpIDAndContractNumber);
				}
			}

			Node contractInfo = doc.getElementsByTagName("CONTRACT_V2_INFO").item(0);

			String contractLinePrice = null;
			String startDate = null;
			String endDate = null;

			OTCXMLBean otcBean = new OTCXMLBean();

			NodeList list = contractInfo.getChildNodes();
			for (int i = 0; i < list.getLength(); i++) {
				Node node = list.item(i);
				if ("CONTRACT_NUMBER".equals(node.getNodeName())) {
					node.setTextContent(corpIDAndContractNumber);
				} else if ("START_DATE".equals(node.getNodeName())) {
					startDate = getDate("0");
					node.setTextContent(startDate);
				} else if ("END_DATE".equals(node.getNodeName())) {
					endDate = getDate("2");
					node.setTextContent(endDate);
				} else if ("LINE_QUANTITY".equals(node.getNodeName())) {
					node.setTextContent(ConstantUtils.OTC_LINE_QTY);
				} else if ("TOTAL_LINE_PRICE".equals(node.getNodeName())) {
					node.setTextContent(contractLinePrice);
				} else if ("ACTION".equals(node.getNodeName())) {
					node.setTextContent(ConstantUtils.OTC_ACTION_UPDATE);
				} else if ("INSTALL_TO_UCN".equals(node.getNodeName())) {
					otcBean.setInstallToUCN(node.getTextContent());
				} else if ("ISBN".equals(node.getNodeName())) {
					otcBean.setISBN(node.getTextContent());
				} else if ("ITEM_DESCRIPTION".equals(node.getNodeName())) {
					otcBean.setTitle(node.getTextContent());
				} else if ("SALES_ORDER_LINE_NUMBER".equals(node.getNodeName())) {
					otcBean.setSalesOrderLineNumber(node.getTextContent());
				} else if ("SALES_ORDER_NUMBER".equals(node.getNodeName())) {
					otcBean.setSalesOrderNumber(node.getTextContent());
				} else if ("LINE_STATUS".equals(node.getNodeName())) {
					otcBean.setLineStatus(node.getTextContent().toLowerCase());
				} else if ("ITEM_NUMBER".equals(node.getNodeName())) {
					otcBean.setItemNumber(node.getTextContent());
				}
			}

			setOTCBeanValue(otcBean, contractLinePrice, startDate, endDate, corpIDAndContractNumber,
					ConstantUtils.OTC_LINE_QTY, ConstantUtils.OTC_ACTION_UPDATE);

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	public void placeContractStatusOTCXMLIntoActiveMQPath(String contractStatus) {
		if (contractStatus.equals(ConstantUtils.CONTRACT_EXPIRED)) {
			copyFile(sourcePathContractExpired, destinationPath);
		} else if (contractStatus.equals(ConstantUtils.CONTRACT_TERMINATED)) {
			copyFile(sourcePathContractTerminated, destinationPath);
		} else {
			Assert.fail("Wrong param " + contractStatus + " passed.");
		}
	}

	public void updateTheLineQuantityInOTCXML(String updatedValue) {
		String filepath = "./src/test/resources/xml/ContractLineQuantity.xml";
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			String corpIDAndContractNumber = (String) TestBaseProvider.getTestBase().getContext()
					.getProperty("CONTRACT_NUMBER_OTC_UPDATE");

			System.out.println("corpId and Contract Number :: " + corpIDAndContractNumber);

			Node corpInfo = doc.getElementsByTagName("CORP_INFO").item(0);
			NodeList corpList = corpInfo.getChildNodes();
			for (int i = 0; i < corpList.getLength(); i++) {
				Node node = corpList.item(i);
				if ("CORP_ID_VALUE".equals(node.getNodeName())) {
					node.setTextContent(corpIDAndContractNumber);
				}
			}

			Node contractInfo = doc.getElementsByTagName("CONTRACT_V2_INFO").item(0);

			String contractLinePrice = null;
			String startDate = null;
			String endDate = null;

			OTCXMLBean otcBean = new OTCXMLBean();

			NodeList list = contractInfo.getChildNodes();
			for (int i = 0; i < list.getLength(); i++) {
				Node node = list.item(i);
				if ("CONTRACT_NUMBER".equals(node.getNodeName())) {
					node.setTextContent(corpIDAndContractNumber);
				} else if ("START_DATE".equals(node.getNodeName())) {
					startDate = getDate("0");
					node.setTextContent(startDate);
				} else if ("END_DATE".equals(node.getNodeName())) {
					endDate = getDate("2");
					node.setTextContent(endDate);
				} else if ("LINE_QUANTITY".equals(node.getNodeName())) {
					node.setTextContent(updatedValue);
				} else if ("TOTAL_LINE_PRICE".equals(node.getNodeName())) {
					node.setTextContent(contractLinePrice);
				} else if ("ACTION".equals(node.getNodeName())) {
					node.setTextContent(ConstantUtils.OTC_ACTION_UPDATE);
				} else if ("INSTALL_TO_UCN".equals(node.getNodeName())) {
					otcBean.setInstallToUCN(node.getTextContent());
				} else if ("ISBN".equals(node.getNodeName())) {
					otcBean.setISBN(node.getTextContent());
				} else if ("ITEM_DESCRIPTION".equals(node.getNodeName())) {
					otcBean.setTitle(node.getTextContent());
				} else if ("SALES_ORDER_LINE_NUMBER".equals(node.getNodeName())) {
					otcBean.setSalesOrderLineNumber(node.getTextContent());
				} else if ("SALES_ORDER_NUMBER".equals(node.getNodeName())) {
					otcBean.setSalesOrderNumber(node.getTextContent());
				} else if ("LINE_STATUS".equals(node.getNodeName())) {
					otcBean.setLineStatus(node.getTextContent().toLowerCase());
				} else if ("ITEM_NUMBER".equals(node.getNodeName())) {
					otcBean.setItemNumber(node.getTextContent());
				}
			}

			setOTCBeanValue(otcBean, contractLinePrice, startDate, endDate, corpIDAndContractNumber, updatedValue,
					ConstantUtils.OTC_ACTION_UPDATE);

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	public void placeUpdatedLineQuantityOTCXMLIntoActiveMQPath() {
		copyFile(sourcePathContractLineQuantity, destinationPath);
	}
}
