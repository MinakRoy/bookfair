package com.scholastic.salesforce.automation.support;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.opencsv.CSVReader;
import com.scholastic.torque.common.TestBaseProvider;

public class DataToS3 {

	private static final String ACCESS_KEY = "AKIAIRTX3PEPWFAVKZYQ";
	private static final String SECRET_KEY = "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";
	private static final String DEV_EXPORT_BUCKET_NAME = "schl-sdc-dev-export";
	private static final String ACCOUNT_FILES_LOCATION = "project/sdc-salesforce-data-integration/account_details/staging/";
	private static final String DISTRICT_FILES_LOCATION = "project/sdc-salesforce-data-integration/district_structure/staging/";
	private static final String USER_FILES_LOCATION = "project/sdc-salesforce-data-integration/user_structure/staging/";
	private static final String TERRITORY_FILES_LOCATION = "project/sdc-salesforce-data-integration/territory_structure/staging/";

	public static AmazonS3 getS3Client() {
		AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
		AmazonS3 s3client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1).build();
		return s3client;
	}

	public void the_files_should_saved_in_the(String type, String location) {
		ObjectListing objectListing = getS3Client()
				.listObjects(new ListObjectsRequest().withBucketName(DEV_EXPORT_BUCKET_NAME).withPrefix(location));
		int size = objectListing.getObjectSummaries().size();
		System.out.println("List of Files are stored :: " + size);
		if (size <= 0) {
			Assert.fail("No File Present/Save to " + location);
		} else {
			Assert.assertTrue(size > 0);
			List<S3ObjectSummary> os = objectListing.getObjectSummaries();
			System.out.println("Last Key " + os.get(size - 1).getKey());
		}
		// int i = 0;
		// for (S3ObjectSummary os : objectListing.getObjectSummaries()) {
		// System.out.println(i++ + "." + os.getKey());
		// }
	}

	public void the_files_saved_should_be_CSV_files() {
		ObjectListing objectListing = getS3Client().listObjects(
				new ListObjectsRequest().withBucketName(DEV_EXPORT_BUCKET_NAME).withPrefix(ACCOUNT_FILES_LOCATION));
		int size = objectListing.getObjectSummaries().size();
		System.out.println("List of Files are stored :: " + size);
		if (size <= 0) {
			Assert.fail("No File Present/Save to " + ACCOUNT_FILES_LOCATION);
		} else {
			Assert.assertTrue(size > 0);
			List<S3ObjectSummary> os = objectListing.getObjectSummaries();
			Assert.assertTrue(os.get(size - 1).getKey().contains(".csv"), "Last File not Present/Saved with CSV.");
		}
	}

	public void the_files_should_have_a_naming_convention(String type, String format) {
		ObjectListing objectListing = null;
		if (type.equalsIgnoreCase("Account")) {
			objectListing = getS3Client().listObjects(
					new ListObjectsRequest().withBucketName(DEV_EXPORT_BUCKET_NAME).withPrefix(ACCOUNT_FILES_LOCATION));
			verifyFilePrefix(objectListing, type);
		} else if (type.equalsIgnoreCase("District")) {
			objectListing = getS3Client().listObjects(new ListObjectsRequest().withBucketName(DEV_EXPORT_BUCKET_NAME)
					.withPrefix(DISTRICT_FILES_LOCATION));
			verifyFilePrefix(objectListing, type);
		} else if (type.equalsIgnoreCase("User")) {
			objectListing = getS3Client().listObjects(
					new ListObjectsRequest().withBucketName(DEV_EXPORT_BUCKET_NAME).withPrefix(USER_FILES_LOCATION));
			verifyFilePrefix(objectListing, type);
		} else if (type.equalsIgnoreCase("Territory")) {
			objectListing = getS3Client().listObjects(new ListObjectsRequest().withBucketName(DEV_EXPORT_BUCKET_NAME)
					.withPrefix(TERRITORY_FILES_LOCATION));
			verifyFilePrefix(objectListing, type);
		} else {
			Assert.fail("Wrong type " + type + " Passed.");
		}
		String dateAndTimeFromKey = (String) TestBaseProvider.getTestBase().getString("DATE_AND_TIME_FROM_KEY");
		Assert.assertTrue(dateAndTimeFromKey.length() == 14, "Time Format is matched.");
	}

	private void verifyFilePrefix(ObjectListing objectListing, String type) {
		int size = objectListing.getObjectSummaries().size();
		if (size <= 0) {
			Assert.fail("No File Present/Save in " + type + " folder.");
		} else {
			Assert.assertTrue(size > 0);
			List<S3ObjectSummary> os = objectListing.getObjectSummaries();
			String key = os.get(size - 1).getKey();
			System.out.println("Key.." + key + " Type ::" + type);
			String dateAndTime = key.substring(key.lastIndexOf("-") + 1, key.indexOf("."));
			TestBaseProvider.getTestBase().getContext().setProperty("DATE_AND_TIME_FROM_KEY", dateAndTime);
			Assert.assertTrue(key.toUpperCase().contains(type.toUpperCase() + "-"),
					"S3 Filename format is Not Matching.");
			Assert.assertTrue(key.contains(".csv"), "Last File not Present/Saved with CSV.");
		}
	}

	public void download_latest_file_from_S_Server(String type) {
		String key = null;
		String fileName = null;
		ObjectListing objectListing = null;
		if (type.equalsIgnoreCase("Account data")) {
			objectListing = getS3Client().listObjects(
					new ListObjectsRequest().withBucketName(DEV_EXPORT_BUCKET_NAME).withPrefix(ACCOUNT_FILES_LOCATION));
		} else if (type.equalsIgnoreCase("District data")) {
			objectListing = getS3Client().listObjects(new ListObjectsRequest().withBucketName(DEV_EXPORT_BUCKET_NAME)
					.withPrefix(DISTRICT_FILES_LOCATION));
		} else if (type.equalsIgnoreCase("User data")) {
			objectListing = getS3Client().listObjects(
					new ListObjectsRequest().withBucketName(DEV_EXPORT_BUCKET_NAME).withPrefix(USER_FILES_LOCATION));
		} else if (type.equalsIgnoreCase("Territory data")) {
			objectListing = getS3Client().listObjects(new ListObjectsRequest().withBucketName(DEV_EXPORT_BUCKET_NAME)
					.withPrefix(TERRITORY_FILES_LOCATION));
		} else {
			Assert.fail("Wrong type " + type + " Passed.");
		}

		int size = objectListing.getObjectSummaries().size();
		System.out.println("list of Files " + size);
		if (size <= 0) {
			Assert.fail("No File Present/Save to " + type);
		} else {
			List<S3ObjectSummary> os = objectListing.getObjectSummaries();
			System.out.println("Last Key " + os.get(size - 1).getKey());
			key = os.get(size - 1).getKey();
			System.out.println("key :: " + key);
			fileName = key.substring(key.lastIndexOf("/") + 1);
			System.out.println("file to Download  + " + fileName);
			TestBaseProvider.getTestBase().getContext().setProperty("FILENAME", fileName);
		}

		S3Object s3object = getS3Client().getObject(DEV_EXPORT_BUCKET_NAME, key);
		S3ObjectInputStream inputStream = s3object.getObjectContent();
		try {
			String projectPath = System.getProperty("user.dir");
			String resourceS3Path = projectPath + "/src/test/resources/s3/";
			String fullPathWithFileName = resourceS3Path + fileName;
			File file = new File(fullPathWithFileName);
			if (!file.exists()) {
				System.out.println("Latest File not present in directory.");
				FileUtils.copyInputStreamToFile(inputStream, new File(fullPathWithFileName));
				System.out.println("File Download Completed.");
			} else {
				System.out.println("Latest File is Already Present in Directory.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void the_files_should_contain_all_fields_for_that_type(String type) {
		String fileToRead = "./src/test/resources/s3/" + TestBaseProvider.getTestBase().getString("FILENAME");
		File file = new File(fileToRead);
		CSVReader reader = null;
		int i = 0;
		try {
			reader = new CSVReader(new FileReader(file));
			String[] nextLine = null;
			while ((nextLine = reader.readNext()) != null) {
				if (i == 0) {
					for (int j = 0; j < nextLine.length; j++) {
						if (type.equalsIgnoreCase("Account data")) {
							Assert.assertEquals(nextLine[j], ConstantUtils.ACCOUNT_FILE_FIELD[j]);
						} else if (type.equalsIgnoreCase("District data")) {
							Assert.assertEquals(nextLine[j], ConstantUtils.DISTRICT_FILE_FIELD[j]);
						} else if (type.equalsIgnoreCase("User data")) {
							Assert.assertEquals(nextLine[j], ConstantUtils.USER_FILE_FIELD[j]);
						} else if (type.equalsIgnoreCase("Territory data")) {
							Assert.assertEquals(nextLine[j], ConstantUtils.TERRITORY_FILE_FIELD[j]);
						} else {
							Assert.fail("Wrong type " + type + " Passed.");
						}
					}
				}
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
