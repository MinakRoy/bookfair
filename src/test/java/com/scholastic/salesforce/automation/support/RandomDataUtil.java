package com.scholastic.salesforce.automation.support;

import org.apache.commons.lang.RandomStringUtils;

public class RandomDataUtil {

	public static String getRandomNumber(int length) {
		return RandomStringUtils.randomNumeric(length);
	}

	public static int getRandomFieldServiceContract(int Max) {
		int Min = 0;
		int randomNum = (int) (Math.random() * (Max - Min));
		System.out.println("Random FSC :: " + randomNum);
		return randomNum;
	}

	public static String getRandomProductName() {
		return "Testing " + RandomStringUtils.randomAlphabetic(8).toLowerCase() + " Product";
	}

	public static String getRandomAccountsName() {
		return "Testing " + RandomStringUtils.randomAlphabetic(8).toLowerCase() + " Accounts";
	}

	public static String getRandomInstituteName() {
		return "Test " + RandomStringUtils.randomAlphabetic(8).toLowerCase() + " Institute";
	}

	public static String getRandomOpportunityName() {
		return "Test " + RandomStringUtils.randomAlphabetic(8).toLowerCase() + " Opportunity";
	}

	public static String getRandomTask() {
		return "Task " + RandomStringUtils.randomAlphabetic(8).toLowerCase();
	}

	public static String getRandomFirstAndLastName() {
		return RandomStringUtils.randomAlphabetic(5).toLowerCase();
	}

	public static String getRandomEvent() {
		return "Event " + RandomStringUtils.randomAlphabetic(8).toLowerCase();
	}

	public static String getRandomNoteTitle() {
		return "Note Title " + RandomStringUtils.randomAlphabetic(5).toLowerCase();
	}

	public static String getRandomNoteDiscription() {
		return "Note Disctiption " + RandomStringUtils.randomAlphabetic(5).toLowerCase();
	}

	public static String getRandomMDRID() {
		return RandomStringUtils.randomNumeric(8);
	}

	public static int getRandomSuggestedConsultant(int Max) {
		int Min = 0;
		int randomNum = (int) (Math.random() * (Max - Min));
		System.out.println("Random Consultant Number :: " + randomNum);
		return randomNum;
	}
}
